package com.josenaves.capstone.client;

import java.util.Collection;

import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;

import com.josenaves.capstone.model.CheckIn;
import com.josenaves.capstone.model.Patient;

/**
 * This interface defines an API for a CheckinService. The
 * interface is used to provide a contract for client/server
 * interactions. 
 * 
 * The interface is annotated with Retrofit annotations so 
 * that clients can automatically convert the object
 * 
 * @author josenaves
 */
public interface CheckinServiceApi {
	
	// The path where we expect the CheckinService to live
	public static final String CHECKIN_PATH = "/checkin";
	
	public static final String TOKEN_PATH = "/oauth/token";

	@GET(CHECKIN_PATH)
	public Collection<CheckIn> getCheckinList();
	
	@GET(CHECKIN_PATH + "/{id}")
	public Patient getCheckin(@Path("id") long id);
	
	@GET(CHECKIN_PATH + "/{id}/medication")
	public Patient getCheckinMedication(@Path("id") long id);
		
	@POST(CHECKIN_PATH)
	public Void addCheckin(@Body CheckIn v);
	
	@DELETE(CHECKIN_PATH + "/{id}")
	public Void deleteCheckin(@Path("id") long id);
	
}
