package com.josenaves.capstone.client;

import java.util.Collection;

import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;
import retrofit.http.Query;

import com.josenaves.capstone.model.Patient;

/**
 * This interface defines an API for a PatientService. The
 * interface is used to provide a contract for client/server
 * interactions. 
 * 
 * The interface is annotated with Retrofit annotations so 
 * that clients can automatically convert the object
 * 
 * @author josenaves
 */
public interface PatientServiceApi {
	
	// The path where we expect the PatientService to live
	public static final String PATIENT_PATH = "/patient";
	
	public static final String TOKEN_PATH = "/oauth/token";

	// The path to search patient by firstName
	public static final String PATIENT_FIRST_NAME_SEARCH_PATH = PATIENT_PATH + "/search/findByFirstName";
	
	public static final String FIRST_NAME_PARAMETER = "firstName";

	@GET(PATIENT_PATH)
	public Collection<Patient> getPatientList();
	
	@GET(PATIENT_PATH + "/{id}")
	public Patient getPatient(@Path("id") long id);
		
	@POST(PATIENT_PATH)
	public Void addPatient(@Body Patient v);
	
	@DELETE(PATIENT_PATH + "/{id}")
	public Void deletePatient(@Path("id") long id);
	
	@GET(PATIENT_FIRST_NAME_SEARCH_PATH)
	public Collection<Patient> findByFirstName(@Query(FIRST_NAME_PARAMETER) String firstName);
}
