package com.josenaves.capstone.client;

import retrofit.http.Body;
import retrofit.http.POST;

import com.josenaves.capstone.model.Login;

public interface LoginServiceApi {
	// The path where we expect the LoginService to live
	public static final String LOGIN_PATH = "/login";
	
	public static final String TOKEN_PATH = "/oauth/token";
	
	@POST(LOGIN_PATH)
	public Login authenticate(@Body Login login);
}
