package com.josenaves.capstone.client;

import java.util.Collection;

import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;

import com.josenaves.capstone.model.Medication;

/**
 * THIS IS RETROFIT !!!
 * 
 * This interface defines an API for a Medication.  The interface is used to provide a 
 * contract for client/server interactions. 
 * 
 * The interface is annotated with Retrofit annotations so that clients can automatically 
 * convert the object.
 * 
 * @author josenaves
 */
public interface MedicationServiceApi {
	
	// The path where we expect the MedicationService to live
	public static final String MEDICATION_PATH = "/medication";
	
	public static final String TOKEN_PATH = "/oauth/token";

	@GET(MEDICATION_PATH)
	public Collection<Medication> getMedicationList();
	
	@GET(MEDICATION_PATH + "/{id}")
	public Medication getMedication(@Path("id") long id);
	
	@POST(MEDICATION_PATH)
	public Void addMedication(@Body Medication medication);
	
	@DELETE(MEDICATION_PATH + "/{id}")
	public Void deleteMedication(@Path("id") long id);
}
