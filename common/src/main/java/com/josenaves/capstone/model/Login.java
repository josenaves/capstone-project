package com.josenaves.capstone.model;

import java.io.Serializable;

import com.google.common.base.Objects;

public final class Login implements Serializable {
	
	private static final long serialVersionUID = -1921934173388923836L;

	private long id = -1;
	private String username;
	private String password;
	private int role;
	
	public static final int ROLE_PATIENT = 1;
	public static final int ROLE_DOCTOR = 2;
	
	public Login() {
	}

	public Login(String user, String pass, int role) {
		this(-1, user, pass, role);
	}

	public Login(long id, String user, String pass, int role) {
		this.id = id;
		this.username = user;
		this.password = pass;
		this.role = role;
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public int getRole() {
		return role;
	}

	public void setRole(int role) {
		this.role = role;
	}

	/**
	 * Two medications will generate the same hashcode if they have exactly the
	 * same values for their names.
	 */
	@Override
	public int hashCode() {
		// Google Guava provides great utilities for hashing
		return Objects.hashCode(id, username, password, role);
	}

	/**
	 * Two medications are considered equal if they have exactly the same values for
	 * their fields.
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Login) {
			Login other = (Login) obj;
			// Google Guava provides great utilities for equals too!
			return Objects.equal(id, other.getId()) && 
					Objects.equal(username, other.getUsername()) && 
					Objects.equal(password, other.getPassword()) &&
					Objects.equal(role, other.getRole());
		} 
		else {
			return false;
		}
	}

	@Override
	public String toString() {
		return "Login [id=" + id + ", username=" + username + ", password="
				+ password + ", role=" + role + "]";
	}
	
}
