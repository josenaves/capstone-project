package com.josenaves.capstone.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.base.Objects;

/**
 * A simple object to represent a patient.
 * @author josenaves
 */
@Entity
public class Patient {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id = -1;

	private String firstName;
	private String lastName;
	private String birthDate;
	
	private long medicalRecordNumber;
	
	private String userName;
	private String password;
	
	private String lastCheckinDate;
	
	@ManyToOne
	@JsonIgnore
	private Doctor doctor;
	
	@OneToMany(mappedBy="patient")
	@JsonIgnore
	private Set<CheckIn> checkin = new HashSet<CheckIn>();

	@ManyToMany
	@JsonIgnore
	private List<Medication> medication = new ArrayList<Medication>();
	
	public Patient() {
	}

	public Patient(String firstName, String lastName, String birthDate, long medicalRecordNumber, String username, String password) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.birthDate = birthDate;
		this.medicalRecordNumber = medicalRecordNumber;
		this.password = password;
		this.userName = username;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}

	public long getMedicalRecordNumber() {
		return medicalRecordNumber;
	}

	public void setMedicalRecordNumber(long medicalRecordNumber) {
		this.medicalRecordNumber = medicalRecordNumber;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Doctor getDoctor() {
		return doctor;
	}

	public void setDoctor(Doctor doctor) {
		this.doctor = doctor;
	}

	public String getLastCheckinDate() {
		return lastCheckinDate;
	}

	public void setLastCheckinDate(String lastCheckinDate) {
		this.lastCheckinDate = lastCheckinDate;
	}

	public List<Medication> getMedication() {
		return medication;
	}

	public void setMedication(List<Medication> medication) {
		this.medication = medication;
	}
	
	public Set<CheckIn> getCheckin() {
		return checkin;
	}

	public void setCheckin(Set<CheckIn> checkin) {
		this.checkin = checkin;
	}

	/**
	 * Two Patients will generate the same hashcode if they have exactly the same values for their name.
	 */
	@Override
	public int hashCode() {
		// Google Guava provides great utilities for hashing
		return Objects.hashCode(firstName, lastName, birthDate, medicalRecordNumber);
	}

	/**
	 * Two Patients are considered equal if they have exactly the same values for
	 * their firstName, lastName, birthDate and medicalRecordNumber.
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Patient) {
			Patient other = (Patient) obj;
			// Google Guava provides great utilities for equals too!
			return Objects.equal(firstName, other.firstName) 
					&& Objects.equal(lastName, other.lastName)
					&& Objects.equal(birthDate, other.birthDate)
					&& Objects.equal(medicalRecordNumber, other.medicalRecordNumber);
		} 
		else {
			return false;
		}
	}

	@Override
	public String toString() {
		return "Patient [id=" + id + ", firstName=" + firstName + ", lastName="
				+ lastName + ", birthDate=" + birthDate
				+ ", medicalRecordNumber=" + medicalRecordNumber
				+ ", userName=" + userName + ", password=" + password
				+ ", lastCheckinDate=" + lastCheckinDate + ", doctor=" + doctor
				+ ", checkin=" + checkin + ", medication=" + medication + "]";
	}
}
