package com.josenaves.capstone.utils;

import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Random;

/**
 * Created by jules on 10/6/14.
 */
public class TestUtils {

    private static final String[] POSSIBLE_LOCALHOSTS = {
            "10.0.2.2", // Android Emulator
            "192.168.56.1" // Genymotion
    };

    public static String findTheRealLocalhostAddress() {
        String realLocalHost = null;

        for(String localhost : POSSIBLE_LOCALHOSTS) {
            try {
                URL url = new URL("http://"+localhost+":8080");
                URLConnection con = url.openConnection();
                con.setConnectTimeout(500);
                con.setReadTimeout(500);
                InputStream in = con.getInputStream();
                if (in != null){
                    realLocalHost = localhost;
                    in.close();
                    break;
                }
            } catch (Exception e) {}
        }

        return realLocalHost;
    }

    public static String randomPersonName() {
        final String alphabet = "0123456789ABCDEFGHIJKLMNOPRSTUVWXYZ";
        final int N = alphabet.length();
        
        String person = "";
        
        Random r = new Random();
        for (int i = 0; i < 10; i++) {
        	person += (alphabet.charAt(r.nextInt(N)));
        }
        return person;
    }
    

}

