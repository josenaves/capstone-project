insert into medication (name) values ('Lortab');
insert into medication (name) values ('OxyContin');
insert into medication (name) values ('Kadian');
insert into medication (name) values ('Onsolis');
insert into medication (name) values ('UltraJect');

insert into doctor (doctor_license_id , first_name, last_name, user_name, password) values (1, 'José', 'Henrique', 'drrique', '123456');
insert into doctor (doctor_license_id , first_name, last_name, user_name, password) values (2, 'Palmirinha', 'Borges', 'drpalmirinha', '123456');

insert into patient (first_name, last_name, birth_date, medical_record_number, user_name, password, doctor_id, last_checkin_date) values ('Jose', 'Naves', '19721006', 1, 'josenaves', '123456', 1, '2014-11-24 12:10:00');
insert into patient (first_name, last_name, birth_date, medical_record_number, user_name, password, doctor_id, last_checkin_date) values ('Eduardo', 'José', '19970218', 2, 'ejpn', '123456', 1, '2014-11-27 11:12:00');
insert into patient (first_name, last_name, birth_date, medical_record_number, user_name, password, doctor_id, last_checkin_date) values ('Juliana', 'Helena', '19770527', 3, 'juliana', '123456', 1, '2014-11-30 04:00:00');

insert into patient (first_name, last_name, birth_date, medical_record_number, user_name, password, doctor_id, last_checkin_date) values ('Ilma', 'Melo', '19480610', 4, 'ilma', '123456', 2, '2014-11-20 05:10:00');
insert into patient (first_name, last_name, birth_date, medical_record_number, user_name, password, doctor_id, last_checkin_date) values ('Luciana', 'Naves', '19750428', 5, 'luciana', '123456', 2, '2014-11-25 11:10:00');

insert into patient_medication (patient_id, medication_id) values (1,1);
insert into patient_medication (patient_id, medication_id) values (1,2);

insert into patient_medication (patient_id, medication_id) values (2,1);
insert into patient_medication (patient_id, medication_id) values (2,2);

insert into patient_medication (patient_id, medication_id) values (3,1);
insert into patient_medication (patient_id, medication_id) values (4,1);
insert into patient_medication (patient_id, medication_id) values (5,1);

insert into check_in (mouth_pain, stop_eating, patient_id, medication_id, taken, datetime) values (1, 1, 1, 1, true,  '2014-11-24 12:10:00');

insert into check_in (mouth_pain, stop_eating, patient_id, medication_id, taken, datetime) values (0, 1, 2, 1, false, '2014-11-27 11:12:00');
insert into check_in (mouth_pain, stop_eating, patient_id, medication_id, taken, datetime) values (1, 2, 2, 2, true,  '2014-11-25 11:10:00');
insert into check_in (mouth_pain, stop_eating, patient_id, medication_id, taken, datetime) values (2, 0, 2, 1, false, '2014-11-21 01:00:00');
insert into check_in (mouth_pain, stop_eating, patient_id, medication_id, taken, datetime) values (0, 2, 2, 2, true,  '2014-11-22 04:00:00');

insert into check_in (mouth_pain, stop_eating, patient_id, medication_id, taken, datetime) values (0, 2, 3, 1, true,  '2014-11-30 04:00:00');

insert into check_in (mouth_pain, stop_eating, patient_id, medication_id, taken, datetime) values (0, 2, 4, 1, true,  '2014-11-20 05:10:00');

insert into check_in (mouth_pain, stop_eating, patient_id, medication_id, taken, datetime) values (0, 2, 5, 1, true,  '2014-11-25 11:10:00');