package com.josenaves.capstone.patient.repository.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.josenaves.capstone.model.Medication;

public class MedicationTest {

	private final Medication medication1 = new Medication("a");
	private final Medication medication2 = new Medication("a");
	
	@Test
	public void testEquals() {
		assertEquals(medication1, medication2);
	}
	
	@Test
	public void testHashcode() {
		assertEquals(medication1.hashCode(), medication2.hashCode());
	}
}
