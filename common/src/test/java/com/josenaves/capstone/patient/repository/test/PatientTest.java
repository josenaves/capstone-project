package com.josenaves.capstone.patient.repository.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.josenaves.capstone.model.Patient;

public class PatientTest {

	private final Patient patient_1 = new Patient("a", "b", "19721006", 21, "a", "a!!!");
	private final Patient patient_2 = new Patient("a", "b", "19721006", 21, "a", "a!!!");
	
	@Test
	public void testEquals() {
		assertEquals(patient_1,patient_2);
	}
	
	@Test
	public void testHashcode() {
		assertEquals(patient_1.hashCode(),patient_2.hashCode());
	}
	
}
