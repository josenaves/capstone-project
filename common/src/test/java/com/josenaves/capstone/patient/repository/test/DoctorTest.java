package com.josenaves.capstone.patient.repository.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.josenaves.capstone.model.Doctor;

public class DoctorTest {

	private final Doctor doctor1 = new Doctor("a", "b", 1111, "a", "a!!!");
	private final Doctor doctor2 = new Doctor("a", "b", 1111, "a", "a!!!");
	
	@Test
	public void testEquals() {
		assertEquals(doctor1,doctor2);
	}
	
	@Test
	public void testHashcode() {
		assertEquals(doctor1.hashCode(),doctor2.hashCode());
	}
}
