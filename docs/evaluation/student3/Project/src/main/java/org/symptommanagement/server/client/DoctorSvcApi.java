package org.symptommanagement.server.client;

import java.util.Collection;

import org.symptommanagement.server.repository.Doctor;
import org.symptommanagement.server.repository.Patient;

import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;
import retrofit.http.Query;

public interface DoctorSvcApi {

	public static final String LAST_NAME_PARAMETER = "LASTNAME";
	
	public static final String PATIENT_DOCTOR_ID_PARAMETER = "PATIENTDOCTORID";

	public static final String TOKEN_PATH = "/oauth/token";

	// The path where we expect the DoctorSvc to live
	public static final String DOCTOR_SVC_PATH = "/doctor";

	// The path to search Doctors by title
	public static final String DOCTOR_LAST_NAME_SEARCH_PATH = DOCTOR_SVC_PATH + "/search/findByLastName";

	@GET(DOCTOR_SVC_PATH)
	public Collection<Doctor> getDoctorList();
	
	@GET(DOCTOR_SVC_PATH + "/{id}")
	public Doctor getDoctorById(@Path("id") long id);
	
	@POST(DOCTOR_SVC_PATH)
	public Doctor addDoctor(@Body Doctor v);
	
	@GET(DOCTOR_LAST_NAME_SEARCH_PATH)
	public Collection<Doctor> findByLastName(@Query(LAST_NAME_PARAMETER) String lastName);
	
	@GET(DOCTOR_LAST_NAME_SEARCH_PATH)
	public Collection<Doctor> findByPatientDoctorId(@Query(PATIENT_DOCTOR_ID_PARAMETER) long patientDoctorId);
}
