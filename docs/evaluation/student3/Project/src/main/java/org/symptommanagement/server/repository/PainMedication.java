package org.symptommanagement.server.repository;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import com.google.common.base.Objects;

@Entity
public class PainMedication {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	private long patientId;
	private String name;
	private boolean taken;
	private Date timeTaken;

	public PainMedication() {
	}

	public PainMedication(long patientId, String name, boolean taken)
	{
		super();
		this.patientId = patientId;
		this.name = name;
		this.taken = taken;
	}
	
	public PainMedication(long patientId, String name, boolean taken, Date timeTaken)
	{
		super();
		this.patientId = patientId;
		this.name = name;
		this.taken = taken;
		this.timeTaken = timeTaken;
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getPatientId() {
		return patientId;
	}
	
	public void setPatientId(long patientId)
	{
		this.patientId = patientId;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public boolean getTaken()
	{
		return this.taken;
	}
	
	public void setTaken(boolean taken) {
		this.taken = taken;
	}

	public Date getTimeTaken() {
		return timeTaken;
	}

	public void setTimeTaken(Date timeTaken) {
		this.timeTaken = timeTaken;
	}

	/**
	 * Two Videos will generate the same hashcode if they have exactly the same
	 * values for their name, url, and duration.
	 * 
	 */
	@Override
	public int hashCode() {
		// Google Guava provides great utilities for hashing
		return Objects.hashCode(id, patientId, name);
	}

	/**
	 * Two Videos are considered equal if they have exactly the same values for
	 * their name, url, and duration.
	 * 
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof PainMedication) {
			PainMedication other = (PainMedication) obj;
			// Google Guava provides great utilities for equals too!
			return id == other.id
					&& patientId == other.patientId
					&& Objects.equal(name, other.name);
		} else {
			return false;
		}
	}

}
