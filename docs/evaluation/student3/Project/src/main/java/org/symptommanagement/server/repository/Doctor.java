package org.symptommanagement.server.repository;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import com.google.common.base.Objects;

@Entity
public class Doctor {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	private String firstName;
	private String lastName;
	private long patientDoctorId;

	public Doctor() {
	}

	public Doctor(String firstName, String lastName, long patientDoctorId)
	{
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.patientDoctorId = patientDoctorId;
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public long getPatientDoctorId() {
		return patientDoctorId;
	}

	public void setPatientDoctorId(long patientDoctorId) {
		this.patientDoctorId = patientDoctorId;
	}

	/**
	 * Two Videos will generate the same hashcode if they have exactly the same
	 * values for their name, url, and duration.
	 * 
	 */
	@Override
	public int hashCode() {
		// Google Guava provides great utilities for hashing
		return Objects.hashCode(id, patientDoctorId, lastName);
	}

	/**
	 * Two Videos are considered equal if they have exactly the same values for
	 * their name, url, and duration.
	 * 
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Doctor) {
			Doctor other = (Doctor) obj;
			// Google Guava provides great utilities for equals too!
			return id == other.id
					&& patientDoctorId == other.patientDoctorId
					&& Objects.equal(lastName, other.lastName);
		} else {
			return false;
		}
	}

}
