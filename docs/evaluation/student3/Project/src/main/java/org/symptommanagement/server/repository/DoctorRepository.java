package org.symptommanagement.server.repository;

import java.util.Collection;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DoctorRepository extends CrudRepository<Doctor, Long> {

	public Collection<Doctor> findByLastName(String lastName);
	public Collection<Doctor> findByPatiendDoctorId(long patientDoctorId);
}
