package org.symptommanagement.server.repository;

public enum MouthPainSoreThroatAnswer
{
	WELL_CONTROLLED(1), MODERATE(2), SEVERE(3);
	private int value;
	
	private MouthPainSoreThroatAnswer(int value)
	{
		this.value = value;
	}
};
