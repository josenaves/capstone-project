package org.symptommanagement.server.repository;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import com.google.common.base.Objects;

@Entity
public class Reminder {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	private long patientId;
	private Collection<Date> dateTimeToGive;
	private String message;

	public Reminder() {
	}

	public Reminder(long patientId, Collection<Date> dateTimeToGive, String message)
	{
		super();
		this.patientId = patientId;
		this.dateTimeToGive = dateTimeToGive;
		this.message = message;
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getPatientId() {
		return this.patientId;
	}

	public void setPatientId(long patientId) {
		this.patientId = patientId;
	}
	
	public Collection<Date> getDateTimeToGive() {
		return dateTimeToGive;
	}

	public void setDateTimeToGive(Collection<Date> dateTimeToGive) {
		this.dateTimeToGive = dateTimeToGive;
	}
	
	public String getMessage()
	{
		return message;
	}
	
	public void setMessage(String message)
	{
		this.message = message;
	}

	/**
	 * Two Videos will generate the same hashcode if they have exactly the same
	 * values for their name, url, and duration.
	 * 
	 */
	@Override
	public int hashCode() {
		// Google Guava provides great utilities for hashing
		return Objects.hashCode(id, patientId, message);
	}

	/**
	 * Two Videos are considered equal if they have exactly the same values for
	 * their name, url, and duration.
	 * 
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Reminder) {
			Reminder other = (Reminder) obj;
			// Google Guava provides great utilities for equals too!
			return id == other.id
					&& patientId == other.patientId
					&& Objects.equal(message, other.message);
		} else {
			return false;
		}
	}

}
