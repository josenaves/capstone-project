package org.symptommanagement.server.repository;

public enum StopEatingDrinking
{
	NO(1), SOME(2), CANT_EAT(3);
	int value;
	
	private StopEatingDrinking(int value)
	{
		this.value = value;
	}
};
