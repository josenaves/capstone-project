package org.symptommanagement.server;

import java.security.Principal;
import java.util.Collection;
import java.util.Set;
import java.util.HashSet;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.symptommanagement.server.client.PatientSvcApi;
import org.symptommanagement.server.repository.Doctor;
import org.symptommanagement.server.repository.DoctorRepository;
import org.symptommanagement.server.repository.Patient;
import org.symptommanagement.server.repository.PatientRepository;

import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;

import com.google.common.collect.Lists;

@Controller
public class MyController {
public static final String MRN_PARAMETER = "MRN";
	
	public static final String DOB_PARAMETER = "DOB";

	public static final String TOKEN_PATH = "/oauth/token";

	// The path where we expect the PatientSvc to live
	public static final String PATIENT_SVC_PATH = "/patient";

	// The path to search Patients by title
	public static final String PATIENT_ID_SEARCH_PATH = PATIENT_SVC_PATH + "/search/findById";
	
	// The path to search Patients by title
	public static final String PATIENT_DOB_SEARCH_PATH = PATIENT_SVC_PATH + "/search/findByDOB";

	@Autowired
	private PatientRepository patients;
	private DoctorRepository doctors;

	@RequestMapping(value = PATIENT_SVC_PATH, method = RequestMethod.GET)
	public @ResponseBody Collection<Patient> getPatientList() {
		return Lists.newArrayList(patients.findAll());
	}

	@RequestMapping(value = PATIENT_SVC_PATH + "/{id}", method = RequestMethod.GET)
	public @ResponseBody Patient getPatientByMRN(@PathVariable("id") long id,
			HttpServletResponse response) {
		Patient patient = null;
		if (patients.findOne(id) != null)
			patient = patients.findOne(id);
		else {
			response.setStatus(HttpStatus.NOT_FOUND.value());
			return null;
		}
		return patient;
	}

	@RequestMapping(value = PATIENT_SVC_PATH, method = RequestMethod.POST)
	public @ResponseBody Patient addPatient(@RequestBody Patient v) {
		Patient patient = new Patient(v.getDob(), v.getFirstName(), v.getLastName(), v.getReminderId(), v.getCheckId(), v.getDoctorId());
		return patients.save(patient);
	}

	@RequestMapping(value = PatientSvcApi.PATIENT_DOB_SEARCH_PATH, method = RequestMethod.GET)
	// Tell Spring to use the "title" parameter in the HTTP request's query
	// string as the value for the title method parameter
	public @ResponseBody Collection<Patient> findByDOB(
			@RequestParam(DOB_PARAMETER) String dob) {
		return patients.findByDOB(dob);
	}

	@RequestMapping(value = PATIENT_SVC_PATH + "/{id}/doctor", method = RequestMethod.GET)
	public @ResponseBody Doctor getDoctorAssignedToPatient(
			@PathVariable("id") long id, HttpServletResponse response) {
		Patient v = null;
		if (patients.findOne(id) != null)
			v = patients.findOne(id);
		else {
			response.setStatus(HttpStatus.NOT_FOUND.value());
			return null;
		}
		long doctorId = v.getDoctorId();
		Doctor d = null;
		if(doctors.findOne(doctorId) != null)
			d = doctors.findOne(doctorId);
		return d;
	}
}
