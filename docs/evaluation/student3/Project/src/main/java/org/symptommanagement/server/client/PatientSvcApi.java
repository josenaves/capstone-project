package org.symptommanagement.server.client;

import java.util.Collection;

import org.symptommanagement.server.repository.Doctor;
import org.symptommanagement.server.repository.Patient;

import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;
import retrofit.http.Query;

public interface PatientSvcApi {
	
	public static final String DOB_PARAMETER = "DOB";

	public static final String TOKEN_PATH = "/oauth/token";

	// The path where we expect the PatientSvc to live
	public static final String PATIENT_SVC_PATH = "/patient";
	
	// The path to search Patients by title
	public static final String PATIENT_DOB_SEARCH_PATH = PATIENT_SVC_PATH + "/search/findByDOB";

	@GET(PATIENT_SVC_PATH)
	public Collection<Patient> getPatientList();
	
	@GET(PATIENT_SVC_PATH + "/{id}")
	public Patient getPatientById(@Path("id") long id);
	
	@POST(PATIENT_SVC_PATH)
	public Patient addPatient(@Body Patient v);
	
	@GET(PATIENT_DOB_SEARCH_PATH)
	public Collection<Patient> findByDOB(@Query(DOB_PARAMETER) String dob);
	
	@GET(PATIENT_SVC_PATH + "/{id}/doctor")
	public Doctor getDoctorAssignedToPatient(@Path("id") long id);
}
