package org.symptommanagement.server.repository;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import com.google.common.base.Objects;

@Entity
public class Patient {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	private String dob;
	private String firstName;
	private String lastName;
	private long reminderId;
	private long checkId;
	private long doctorId;
	private long painMedicationId;

	public Patient() {
	}

	public Patient(String dob, String firstName,
			String lastName, long reminderId, long checkId, long doctorId, long painMedicationId)
	{
		super();
		this.dob = dob;
		this.firstName = firstName;
		this.lastName = lastName;
		this.reminderId = reminderId;
		this.checkId = checkId;
		this.doctorId = doctorId;
		this.painMedicationId = painMedicationId;
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public long getReminderId() {
		return reminderId;
	}

	public void setReminderId(long reminderId) {
		this.reminderId = reminderId;
	}

	public long getCheckId() {
		return checkId;
	}

	public void setCheckId(long checkId) {
		this.checkId = checkId;
	}

	public long getDoctorId() {
		return doctorId;
	}

	public void setDoctorId(long doctorId) {
		this.doctorId = doctorId;
	}
	
	public long getPainMedicationId()
	{
		return this.painMedicationId;
	}
	
	public void setPainMedicationId(long painMedicationId)
	{
		this.painMedicationId = painMedicationId;
	}
	
	/**
	 * Two Videos will generate the same hashcode if they have exactly the same
	 * values for their name, url, and duration.
	 * 
	 */
	@Override
	public int hashCode() {
		// Google Guava provides great utilities for hashing
		return Objects.hashCode(id, dob, lastName);
	}

	/**
	 * Two Videos are considered equal if they have exactly the same values for
	 * their name, url, and duration.
	 * 
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Patient) {
			Patient other = (Patient) obj;
			// Google Guava provides great utilities for equals too!
			return id == other.id
					&& Objects.equal(dob, other.dob)
					&& Objects.equal(lastName, other.lastName);
		} else {
			return false;
		}
	}

}
