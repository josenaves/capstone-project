package org.symptommanagement.server.repository;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

import com.google.common.base.Objects;

@Entity
public class CheckIn {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	private long patientId;
	private Date date;
	private MouthPainSoreThroatAnswer mouthPainSoreThroatAnswer;
	private StopEatingDrinking stopEatingDrinkingAnswer;
	private boolean[] painMedicationAnswers;

	public CheckIn() {
	}

	public CheckIn(long patientId, Date date)
	{
		super();
		this.patientId = patientId;
		this.date = date;
	}
	
	public CheckIn(long patientId, Date date, MouthPainSoreThroatAnswer mouthPainSoreThroatAnswer,StopEatingDrinking stopEatingDrinkingAnswer, boolean painMedicationAnswers[])
	{
		super();
		this.patientId = patientId;
		this.date = date;
		this.mouthPainSoreThroatAnswer = mouthPainSoreThroatAnswer;
		this.stopEatingDrinkingAnswer = stopEatingDrinkingAnswer;
		this.painMedicationAnswers = painMedicationAnswers;
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public MouthPainSoreThroatAnswer getMouthPainSoreThroatAnswer()
	{
		return mouthPainSoreThroatAnswer;
	}
	
	public void setMouthPainSoreThroatAnswer(MouthPainSoreThroatAnswer mouthPainSoreThroatAnswer)
	{
		this.mouthPainSoreThroatAnswer = mouthPainSoreThroatAnswer;
	}
	
	public StopEatingDrinking getStopEatingDrinking()
	{
		return stopEatingDrinkingAnswer;
	}
	
	public void setStopEatingDrinking(StopEatingDrinking stopEatingDrinkingAnswer)
	{
		this.stopEatingDrinkingAnswer = stopEatingDrinkingAnswer;
	}
	
	public boolean[] getPainMedicationAnswers()
	{
		return this.painMedicationAnswers;
	}
	
	public void setPainMedicationAnswers(boolean[] painMedicationAnswers)
	{
		this.painMedicationAnswers = painMedicationAnswers;
	}
	
	public boolean getPainMedicationAnswer()
	{
		boolean answer = true;
		for(boolean a : painMedicationAnswers)
		{
			answer = answer && a;
		}
		return answer;
	}

	/**
	 * Two Videos will generate the same hashcode if they have exactly the same
	 * values for their name, url, and duration.
	 * 
	 */
	@Override
	public int hashCode() {
		// Google Guava provides great utilities for hashing
		return Objects.hashCode(id, patientId, date);
	}

	/**
	 * Two Videos are considered equal if they have exactly the same values for
	 * their name, url, and duration.
	 * 
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof CheckIn) {
			CheckIn other = (CheckIn) obj;
			// Google Guava provides great utilities for equals too!
			return id == other.id
					&& patientId == other.patientId
					&& Objects.equal(date, other.date);
		} else {
			return false;
		}
	}

}
