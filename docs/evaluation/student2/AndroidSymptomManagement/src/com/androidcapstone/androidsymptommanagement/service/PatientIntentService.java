package com.androidcapstone.androidsymptommanagement.service;

import retrofit.client.ApacheClient;
import retrofit.client.Client;

import com.androidcapstone.androidsymptommanagement.SymptomManagementApplication;
import com.androidcapstone.androidsymptommanagement.client.Config;
import com.androidcapstone.androidsymptommanagement.client.EasyHttpClient;
import com.androidcapstone.androidsymptommanagement.client.OAuthHandler;
import com.androidcapstone.androidsymptommanagement.client.SecuredRestBuilder;
import com.androidcapstone.symptommanagement.api.PatientService;
import com.androidcapstone.symptommanagement.model.Patient;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;

public class PatientIntentService extends IntentService {
	
	public static final int STATUS_RUNNING = 0x01;
	
	public static final int STATUS_FINISHED = 0x02;
	
	public static final int STATUS_ERROR = 0x03;

	public static final String PATIENT_REQUEST_EXTRA = "PatientIntentService.Request_Patient";
	
	public static final String PATIENT_SERVICE_RESULT = "PatientIntentService.Result";
	
	public static final int GET_PATIENT_REQUEST = 0x01;
	
	public PatientIntentService() {
		super("PatientIntentService");
	}
	


	@Override
	protected void onHandleIntent(Intent intent) {
		// TODO Auto-generated method stub
		int request = intent.getIntExtra(PATIENT_REQUEST_EXTRA, GET_PATIENT_REQUEST);
		
		final ResultReceiver receiver = intent.getParcelableExtra(Config.RESULT_RECEIVER);
		
		Bundle bundle = new Bundle();
		
		receiver.send(STATUS_RUNNING, Bundle.EMPTY);
		
		// TODO Error on AccessToken
		SymptomManagementApplication app = SymptomManagementApplication.getInstance();
		String accessToken = app.getAccessToken();
		String user = app.getUserName();
		String pass = app.getPassword();
		Log.i(Config.TAG, "User " + user + " pass" + pass + " accessToken " + accessToken);
		PatientService patients = newService(user, pass,accessToken);
		
		if(request == GET_PATIENT_REQUEST) {
			Patient p = patients.getPatient();
			Log.i(Config.TAG, "PatientIntentService.getPatient got " + p);
			bundle.putSerializable(PATIENT_SERVICE_RESULT, p);
			receiver.send(STATUS_FINISHED, bundle);
		} else {
			bundle.putString(Intent.EXTRA_TEXT, "Unidentified Operation");
			receiver.send(STATUS_ERROR, bundle);
		}

	}
	
	PatientService newService(String user, String pass, String accessToken) {
		Client c = new ApacheClient(new EasyHttpClient());
		OAuthHandler handler = new OAuthHandler(c, Config.LOGIN_ENDPOINT, user, pass, Config.CLIENT_ID, "", accessToken);
		PatientService patients = new SecuredRestBuilder()
		.setClient(c)
		.setUsername(user).setPassword(pass)
		.setOAuthHandler(handler).build().create(PatientService.class);
		return patients;
	}

}
