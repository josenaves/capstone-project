package com.androidcapstone.androidsymptommanagement;

import java.util.ArrayList;
import java.util.List;

import com.androidcapstone.androidsymptommanagement.doctor.DoctorApplication;
import com.androidcapstone.symptommanagement.model.CheckIn;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

public class CheckInListActivity extends ListActivity {

	public static final String CHECKIN_LIST_EXTRA = "CHECKIN_LIST_EXTRA";
	
	private CheckInListAdapter mAdapter;
	
	private List mListItems;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_check_in_list);
		Intent intent = getIntent();
		Bundle extras = intent.getExtras();
		List<CheckIn> checkIns = (ArrayList<CheckIn>) extras.getSerializable(CHECKIN_LIST_EXTRA);
		
		if(null == checkIns) {
			checkIns = DoctorApplication.getInstance().getCheckIns();
		}
		
		mListItems = new ArrayList();
		for (CheckIn checkIn : checkIns) {
			mListItems.add(new CheckInListItem(checkIn));
		}
		
		mAdapter = new CheckInListAdapter(this, mListItems, null);
		
		setListAdapter(mAdapter);
	}
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.check_in_list, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
