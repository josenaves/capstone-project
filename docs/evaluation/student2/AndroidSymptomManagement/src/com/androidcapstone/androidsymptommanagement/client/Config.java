package com.androidcapstone.androidsymptommanagement.client;

import com.androidcapstone.symptommanagement.api.API;

public class Config {
	
	public static final String RESULT_RECEIVER = "ResultReceiver";

	public static final String BASE_URL = "https://10.230.230.182:8443";
	
	public static final String CLIENT_ID = "mobile";
	
	public static final String LOGIN_ENDPOINT = BASE_URL + API.TOKEN_PATH;
	
	public static final String ACCESS_TOKEN = "accessToken";

	public static final int DOCTOR_UPDATE_NOTIFICATION = 0x02;
	
	public static final String TAG = "AndroidCapstone";

	public static final String USER = "USER";

	public static final String PASS = "PASS";
}
