package com.androidcapstone.androidsymptommanagement.doctor;

import java.util.ArrayList;
import java.util.List;

import com.androidcapstone.androidsymptommanagement.R;
import com.androidcapstone.androidsymptommanagement.R.id;
import com.androidcapstone.androidsymptommanagement.R.layout;
import com.androidcapstone.androidsymptommanagement.R.menu;
import com.androidcapstone.symptommanagement.model.Alert;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListAdapter;
import android.widget.ListView;

public class AlertListActivity extends ListActivity {

	
	public static final String ALERT_LIST_EXTRA = "ALERT_LIST_EXTRA";
	
	public static final String SEE_ALL = "ALERT_LIST_SEE_ALL";

	private AlertListAdapter mAdapter;
	
	private List mListItems;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_alert_list);
		Intent intent = getIntent();
		Bundle extras = intent.getExtras();
		String action = intent.getAction();
		List<Alert> alerts;
		if(null != action && action.equalsIgnoreCase(SEE_ALL)) {
			alerts = DoctorApplication.getInstance().getAlerts();
		} else {
			alerts = (ArrayList<Alert>) extras.getSerializable(ALERT_LIST_EXTRA);
		}

		mListItems = new ArrayList();
		for (Alert alert : alerts) {
			mListItems.add(new AlertListItem(alert));
		}
		mAdapter = new AlertListAdapter(this, mListItems, null);
		setListAdapter(mAdapter);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.alert_list, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	

}
