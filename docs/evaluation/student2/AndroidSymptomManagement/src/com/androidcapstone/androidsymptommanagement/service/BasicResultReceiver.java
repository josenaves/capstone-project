package com.androidcapstone.androidsymptommanagement.service;

import com.androidcapstone.androidsymptommanagement.client.Config;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.util.Log;

public class BasicResultReceiver extends ResultReceiver {

	private Receiver mReceiver;
	
	public BasicResultReceiver(Handler handler) {
		super(handler);
		// TODO Auto-generated constructor stub
	}

	
	public interface Receiver {
		public void onReceiveResult(int resultCode, Bundle resultData);
	}
	
	 public void setReceiver(Receiver receiver) {
	        mReceiver = receiver;
	 }
	 
	 protected void onReceiveResult(int resultCode, Bundle resultData) {
		 if (mReceiver != null) {
			 mReceiver.onReceiveResult(resultCode, resultData);
		 } else {
			 Log.i(Config.TAG, "BasicResultReceiver mReceiver is NULL");
		 }
	 }
}
