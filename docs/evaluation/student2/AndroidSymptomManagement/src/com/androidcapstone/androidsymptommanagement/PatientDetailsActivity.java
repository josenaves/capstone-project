package com.androidcapstone.androidsymptommanagement;

import java.util.ArrayList;
import java.util.List;

import com.androidcapstone.androidsymptommanagement.client.Config;
import com.androidcapstone.androidsymptommanagement.doctor.AlertListActivity;
import com.androidcapstone.androidsymptommanagement.doctor.DoctorApplication;
import com.androidcapstone.androidsymptommanagement.doctor.DoctorApplication.PatientData;
import com.androidcapstone.symptommanagement.model.Alert;
import com.androidcapstone.symptommanagement.model.CheckIn;
import com.androidcapstone.symptommanagement.model.Medicine;
import com.androidcapstone.symptommanagement.model.Patient;

import butterknife.ButterKnife;
import butterknife.InjectView;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class PatientDetailsActivity extends Activity implements OnClickListener {
	
	public static final String PATIENT_ARG = "PatientDetailsActivity.PATIENT";

	public static final int UPDATE_MEDICINE_REQUEST = 100;

	private Patient mPatient;

	@InjectView(R.id.textView1)
	TextView nameTextView;
	
	@InjectView(R.id.textView2)
	TextView dobTextView;
	
	@InjectView(R.id.textView3)
	TextView painLevel;
	
	@InjectView(R.id.textView4)
	TextView eatingProblems;
	
	@InjectView(R.id.button1)
	Button checkinsButton;
	
	@InjectView(R.id.textView5)
	TextView alertStatus;
	
	@InjectView(R.id.textView6)
	TextView alertHours;
	
	@InjectView(R.id.button2)
	Button alertsButton;
	
	@InjectView(R.id.medListView)
	ListView medicineListView;
	
	@InjectView(R.id.button3)
	Button updateMedicinesButton;
	
	ListAdapter mAdapter;
	
	List medicineList;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_patient_details);
		ButterKnife.inject(this);
		Intent intent = getIntent();
		Bundle extras = intent.getExtras();
		mPatient = (Patient) extras.getSerializable(PATIENT_ARG);
		
		nameTextView.setText(nameTextView.getText().toString() + ": " + mPatient.getFirstName() + " " + mPatient.getLastName());
		
		dobTextView.setText(dobTextView.getText().toString() + ": " + DateFormat.format("yyyy-MM-dd", mPatient.getDateOfBirth()));
		
		DoctorApplication drapp = DoctorApplication.getInstance();
		CheckIn lastCheckin = drapp.getLastCheckInFor(mPatient);
		
		if (null != lastCheckin) {
			painLevel.setText(painLevel.getText().toString() + ": "
					+ lastCheckin.getPainLevel());
			eatingProblems.setText(eatingProblems.getText().toString() + ": "
					+ lastCheckin.getEatingInterferenceLevel());
		} else {
			painLevel.setText(painLevel.getText().toString() + ": "
					+ "N/A");
			eatingProblems.setText(eatingProblems.getText().toString() + ": "
					+ "N/A");
		}
		checkinsButton.setOnClickListener(this);
		
		
		Alert alert = drapp.getLastAlertFor(mPatient);
		if(null != alert) {
			alertStatus.setText(alertStatus.getText() + ": " + alert.getAlertType());
			alertHours.setText(alertHours.getText() + ": " + drapp.hoursSince(alert.getCreated()));
		} else {
			alertStatus.setText(alertStatus.getText() + ": N/A" );
			alertHours.setText(alertHours.getText() + ": N/A");
		}
		
		alertsButton.setOnClickListener(this);
		
		setListAdapter();
		

		
		updateMedicinesButton.setOnClickListener(this);
		
	}

	private void setListAdapter() {
		// TODO Auto-generated method stub
		List<Medicine> meds = mPatient.getMedicines();
		medicineList = new ArrayList(meds.size());
		for (Medicine medicine : meds) {
			medicineList.add(new MedicineListItem(medicine.getName()));
		}
		
		mAdapter = new MedicineListAdapter(this, medicineList);
		medicineListView.setAdapter(mAdapter);		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.patient_details, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onClick(View v) {
		if(v == checkinsButton) {
			PatientData pd = DoctorApplication.getInstance().getData(mPatient);
			Intent intent = new Intent(this, CheckInListActivity.class);
			Bundle extras = new Bundle();
			extras.putSerializable(CheckInListActivity.CHECKIN_LIST_EXTRA, (ArrayList<CheckIn>)pd.checkIns);
			intent.putExtras(extras);
			startActivity(intent);
		} else if (v == alertsButton) {
			PatientData pd = DoctorApplication.getInstance().getData(mPatient);
			Intent intent = new Intent(this, AlertListActivity.class);
			Bundle extras = new Bundle();
			extras.putSerializable(AlertListActivity.ALERT_LIST_EXTRA, (ArrayList<Alert>)pd.alerts);
			intent.putExtras(extras);
			startActivity(intent);
		} else if (v == updateMedicinesButton) {
			Intent intent = new Intent(this, UpdateMedicineActivity.class);
			Bundle extras = new Bundle();
			extras.putSerializable(UpdateMedicineActivity.PATIENT_ARG, mPatient);
			intent.putExtras(extras);
			startActivityForResult(intent, UPDATE_MEDICINE_REQUEST);
		} 
		
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(requestCode == UPDATE_MEDICINE_REQUEST) {
			if(resultCode == RESULT_OK) {
				Bundle extra = data.getExtras();
				Patient p  = (Patient) extra.getSerializable(UpdateMedicineActivity.PATIENT_RESULT);
				Log.i(Config.TAG,"Got new Patient from UpdateMedicineActivity " + p);
				if(null != p) {
					mPatient = p;
				}
				setListAdapter();
			}
		}
	}
}
