package com.androidcapstone.androidsymptommanagement;

import java.util.Calendar;
import java.util.List;

import com.androidcapstone.androidsymptommanagement.client.Config;
import com.androidcapstone.androidsymptommanagement.doctor.DoctorApplication;
import com.androidcapstone.androidsymptommanagement.service.ReminderIntentService;
import com.androidcapstone.androidsymptommanagement.service.ReminderIntentServiceResultReceiver;
import com.androidcapstone.symptommanagement.model.Reminder;

import butterknife.ButterKnife;
import butterknife.InjectView;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AnalogClock;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TimePicker;
import android.widget.Toast;

public class EditReminderActivity extends Activity implements OnClickListener, ReminderIntentServiceResultReceiver.Receiver{

	public static final String REMINDER_ARG = "EditReminderActivity.Reminder";
	
	@InjectView(R.id.reminderPicker)
	TimePicker reminderTimePicker;
	
	@InjectView(R.id.reminderSubmit)
	Button submitButton;
	
	@InjectView(R.id.reminderDeleteButton)
	Button deleteButton;
	
	@InjectView(R.id.reminderCancelButton)
	Button cancelButton;
	

	
	private Reminder mReminder = null;
	
	private ReminderIntentServiceResultReceiver mReceiver;
	
	private SymptomManagementApplication app = SymptomManagementApplication.getInstance();
	
	private ReminderIntentService.Operation mOperation = ReminderIntentService.Operation.None;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_edit_reminder);
		ButterKnife.inject(this);
		submitButton.setOnClickListener(this);
		deleteButton.setOnClickListener(this);
		cancelButton.setOnClickListener(this);
		mReceiver = new ReminderIntentServiceResultReceiver(new Handler ());
		mReceiver.setReceiver(this);
		Intent intent = getIntent();
		
		mReminder = (Reminder) intent.getSerializableExtra(REMINDER_ARG);
		
		if(mReminder != null) {
			String[] tmp = mReminder.getTime().split(":");
			Log.i(Config.TAG, mReminder.getTime());
			Log.i(Config.TAG, "size after split " + tmp.length);
			for (int i = 0; i < tmp.length; i++) {
				Log.i(Config.TAG, "tmp " + i + " " + tmp[i]);
			}
			Integer hour = Integer.parseInt(tmp[0]);
			if(null != hour) {
				reminderTimePicker.setCurrentHour(Integer.getInteger(tmp[0]));
			} else {
				reminderTimePicker.setCurrentHour(0);
			}
			Integer minute = Integer.parseInt(tmp[1]);
			if(null != minute) {
				
				reminderTimePicker.setCurrentMinute(minute);
			} else {
				reminderTimePicker.setCurrentMinute(0);
			}
		} else {
			mReminder = new Reminder();
			mReminder.setPatient(app.getPatient());
		}
		
		
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if(v == submitButton) {
			submitButton.setEnabled(false);
			int hour = reminderTimePicker.getCurrentHour();
			int minute = reminderTimePicker.getCurrentMinute();
			String time = String.format("%02d:%02d:00", hour, minute);
			mReminder.setTime(time);
			launchService(ReminderIntentService.Operation.Save, mReminder);

		} else if (v == cancelButton) {
			finish();
		} else if (v == deleteButton) {
			launchService(ReminderIntentService.Operation.Delete, mReminder);
		}
	}
	
	private void launchService(ReminderIntentService.Operation operation, Reminder reminder) {
		mOperation = operation;
		Intent intent = new Intent(this, ReminderIntentService.class);
		Bundle extras = new Bundle();
		extras.putInt(ReminderIntentService.REMINDER_OPERATION, operation.ordinal());
		extras.putParcelable(Config.RESULT_RECEIVER, mReceiver);
		extras.putSerializable(ReminderIntentService.REMINDER_EXTRA_ARG, reminder);
		intent.putExtras(extras);
		startService(intent);
	}

	@Override
	public void onReceiveResult(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub
		ReminderIntentService.Status status = ReminderIntentService.Status.values()[resultCode];
		Log.i(Config.TAG, "EditReminderActivity.onReceiveResult result " + status);
		switch(status) {
		case Error:
			displayError(resultData);
			//stopAnimation();
			break;
		case Finished:
			handleResults(resultData);
			//stopAnimation();
			break;
		case Running:
			//startAnimation();
			break;
		default:
			break;
			
		}
	}

	private void handleResults(Bundle resultData) {

		switch(mOperation) {
		case Delete:
			Log.i(Config.TAG, "Removing reminder " + mReminder.getTime());
			app.removeReminder(mReminder);
			//removeAlarm(mReminder);
			break;
		case Save:
			Reminder reminder = (Reminder) resultData.getSerializable(ReminderIntentService.REMINDER_RESULT);
			app.addReminder(reminder);
			Log.i(Config.TAG, "EditReminderActivity loading reminder " + reminder.getId());
			addAlarm(reminder);
			break;
		default:
			break;
		}
		Log.i(Config.TAG, "Calling finish on EditReminderActivity");
		finish();
	}
	
	private void addAlarm(Reminder reminder) {
		SymptomManagementApplication app = SymptomManagementApplication.getInstance();
		AlarmReceiver.launchAlarm(this, reminder, app.getUserName(), app.getPassword());
	}

	private void displayError(Bundle resultData) {
		Log.e(Config.TAG, "ReminderFragment ERROR");
		String text = resultData.getString(Intent.EXTRA_TEXT);
		Log.e(Config.TAG, "ReminderFragment ERROR " + text);
		Toast.makeText(getApplicationContext(), text, Toast.LENGTH_LONG).show();;
		finish();
	}
}
