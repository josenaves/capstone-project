package com.androidcapstone.androidsymptommanagement.doctor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import com.androidcapstone.symptommanagement.model.Alert;
import com.androidcapstone.symptommanagement.model.CheckIn;
import com.androidcapstone.symptommanagement.model.Medicine;
import com.androidcapstone.symptommanagement.model.Patient;

public class DoctorApplication {

	private static DoctorApplication _INSTANCE = null;
	
	private List<PatientData> patientDataList = new ArrayList<DoctorApplication.PatientData>();
	
	private List<Medicine> medicines = new ArrayList<Medicine>();
	
	private String authToken;
	
	private String userName;
	
	private String password;
	
	
	private DoctorApplication() {
		// TODO Auto-generated constructor stub
	}
	
	public static synchronized DoctorApplication getInstance() {
		if(null == _INSTANCE) {
			_INSTANCE = new DoctorApplication();
		}
		return _INSTANCE;
	}
	
	public PatientData getData(Patient p) {
		PatientData tmp = new PatientData();
		tmp.p = p;
		if(!patientDataList.contains(tmp)) {
			patientDataList.add(tmp);
		} else {
			int index = patientDataList.indexOf(tmp);
			tmp = patientDataList.get(index);
			tmp.p = p;
		}
		return tmp;
	}
	
	public void addPatient(Patient p) {
		PatientData tmp = getData(p);
		tmp.p = p;
	}
	
	public void addAlerts(Patient p, Collection<Alert> as) {
		PatientData tmp = getData(p);
		tmp.p = p;
		tmp.alerts = new ArrayList<Alert>(as);
	}
	
	public void addCheckins(Patient p, Collection<CheckIn> cs) {
		PatientData tmp = getData(p);
		tmp.p = p;	
		tmp.checkIns = new ArrayList<CheckIn>(cs);
	}
	
	
	static public class PatientData {
		public Patient p;
		public List<Alert> alerts;
		public List<CheckIn> checkIns;
		
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((p == null) ? 0 : p.hashCode());
			return result;
		}
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (!(obj instanceof PatientData))
				return false;
			PatientData other = (PatientData) obj;

			if (p == null) {
				if (other.p != null)
					return false;
			} else if (!p.equals(other.p))
				return false;
			return true;
		}
		
		
	}


	public List<PatientData> getPatientDataList() {
		return patientDataList;
	}

	public void setPatientDataList(List<PatientData> patientDataList) {
		this.patientDataList = patientDataList;
	}

	public List<Medicine> getMedicines() {
		return medicines;
	}

	public void setMedicines(List<Medicine> medicines) {
		this.medicines = medicines;
	}

	public String getAuthToken() {
		return authToken;
	}

	public void setAuthToken(String authToken) {
		this.authToken = authToken;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public int getAlertCount() {
		
		int count = 0;
		
		for (PatientData patientData : patientDataList) {
			count += patientData.alerts.size();
		}
		return count;
	}
	
	public int getPatientCount() {
		return patientDataList.size();
	}

	public static long hoursSince(Date created) {
		long now = new Date().getTime();
		long since = created.getTime();
		long HOUR = 60*60*1000L;
		return (now - since) / HOUR;
	}

	public List<Alert> getAlerts() {
		// TODO Auto-generated method stub
		List<Alert> alerts = new LinkedList<Alert>();
		for (PatientData patientData : patientDataList) {
			if(null != patientData.alerts) {
				alerts.addAll(patientData.alerts);
			}
		}
		return alerts;
	}
	
	public List<Patient> getPatients() {
		List<Patient> ps = new ArrayList<Patient>(patientDataList.size());
		for (PatientData patientData : patientDataList) {
			ps.add(patientData.p);
		}
		return ps;
	}

	public CheckIn getLastCheckInFor(Patient p) {
		PatientData d = getData(p);
		if(null == d.checkIns || d.checkIns.isEmpty()) {
			return null;
		}
		
		CheckIn ptr = d.checkIns.get(0);
		for (CheckIn c : d.checkIns) {
			if(c.getCheckInTimestamp().after(ptr.getCheckInTimestamp())) {
				ptr = c;
			}
			
		}
		return ptr;
	}

	public Alert getLastAlertFor(Patient p) {
		PatientData d = getData(p);
		
		if(null == d.alerts || d.alerts.isEmpty()) {
			return null;
		}
		
		Alert ptr = d.alerts.get(0);
		
		for(Alert a : d.alerts) {
			if(a.getCreated().after(ptr.getCreated())){
				ptr = a;
			}
		}
		
		return ptr;
	}

	public Patient getPatientByName(String first, String last) {

		for (PatientData patientData : patientDataList) {
			if(patientData.p.getLastName().equalsIgnoreCase(last)
					&& patientData.p.getFirstName().equalsIgnoreCase(first)) {
				return patientData.p;
			}
		}
		return null;
	}

	public List<CheckIn> getCheckIns() {
		List<CheckIn> checkins = new LinkedList<CheckIn>();
		for (PatientData p : patientDataList) {
			if(null != p.checkIns ) {
				checkins.addAll(p.checkIns);
			}
		}
		return checkins;
	}

	public boolean updatePatient(Patient patient) {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean updateAlerts(Patient patient, Collection<Alert> as) {
		PatientData data = getData(patient);
		boolean isNew = false;
		if(null == data.alerts) {
			if (null != as && !as.isEmpty()) {
				isNew = true;
				data.alerts = new ArrayList<Alert>(as);
			}
		} else {
			isNew = as.size() > data.alerts.size();
			if(isNew) {
				data.alerts = new ArrayList<Alert>(as);
			}
			
		}
		return isNew;
	}

	public boolean updateCheckins(Patient patient, Collection<CheckIn> cs) {
		PatientData data = getData(patient);
		boolean isNew = false;
		if(null == data.checkIns) {
			if (null != cs && !cs.isEmpty()) {
				isNew = true;
				data.checkIns = new ArrayList<CheckIn>(cs);
			}
		} else {
			isNew = cs.size() > data.alerts.size();
			if(isNew) {
				data.checkIns = new ArrayList<CheckIn>(cs);
			}
			
		}
		return isNew;
	}

	public void clear() {
		if (null != patientDataList) {
			// TODO Auto-generated method stub
			patientDataList.clear();
		}
		if (null != medicines) {
			medicines.clear();
		}
		authToken = null;
		userName = null;
		password = null;
	}
}
