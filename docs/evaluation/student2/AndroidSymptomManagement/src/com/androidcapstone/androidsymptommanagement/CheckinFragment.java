package com.androidcapstone.androidsymptommanagement;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import com.androidcapstone.androidsymptommanagement.client.Config;
import com.androidcapstone.androidsymptommanagement.service.BasicResultReceiver;
import com.androidcapstone.androidsymptommanagement.service.CheckInIntentService;
import com.androidcapstone.androidsymptommanagement.service.ReminderIntentServiceResultReceiver;
import com.androidcapstone.symptommanagement.model.CheckIn;
import com.androidcapstone.symptommanagement.model.EatingInterferenceType;
import com.androidcapstone.symptommanagement.model.MedicationCheckIn;
import com.androidcapstone.symptommanagement.model.Medicine;
import com.androidcapstone.symptommanagement.model.PainLevelType;
import com.androidcapstone.symptommanagement.model.Patient;
import com.androidcapstone.symptommanagement.model.ResponseType;

import butterknife.ButterKnife;
import butterknife.InjectView;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Spinner;

/**
 * A simple {@link Fragment} subclass. Activities that contain this fragment
 * must implement the {@link CheckinFragment.OnFragmentInteractionListener}
 * interface to handle interaction events. Use the
 * {@link CheckinFragment#newInstance} factory method to create an instance of
 * this fragment.
 *
 */
public class CheckinFragment extends Fragment implements OnClickListener, BasicResultReceiver.Receiver {


	private OnFragmentInteractionListener mListener;
	
	@InjectView(R.id.painSpinner)
	Spinner painSpinner;
	
	@InjectView(R.id.eatSpinner)
	Spinner eatSpinner;
	
	@InjectView(R.id.checkInMedicines)
	ListView mListView;
	
	@InjectView(R.id.checkInSubmit)
	Button submitButton;
	


	SymptomManagementApplication app = SymptomManagementApplication.getInstance();
	
	private List mMedicineCheckinList;
	
	private ListAdapter mAdapter;
	
	private BasicResultReceiver mReceiver;
	
	/**
	 * Use this factory method to create a new instance of this fragment using
	 * the provided parameters.
	 *
	 * @param param1
	 *            Parameter 1.
	 * @param param2
	 *            Parameter 2.
	 * @return A new instance of fragment CheckinFragment.
	 */
	// TODO: Rename and change types and number of parameters
	public static CheckinFragment newInstance(String param1, String param2) {
		CheckinFragment fragment = new CheckinFragment();
		Bundle args = new Bundle();
		fragment.setArguments(args);
		return fragment;
	}

	public CheckinFragment() {
		// Required empty public constructor
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (getArguments() != null) {

		}
		mReceiver = new BasicResultReceiver(new Handler ());
		mReceiver.setReceiver(this);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		View view = inflater.inflate(R.layout.fragment_checkin, container, false);
		ButterKnife.inject(this, view);
		setListAdapter();
		submitButton.setOnClickListener(this);
		
		return view;
	}

	private void setListAdapter() {
		
		if (null == mMedicineCheckinList) {
			mMedicineCheckinList = new ArrayList();
			Patient p = app.getPatient();
			Collection<Medicine> meds = p.getMedicines();
			for (Medicine medicine : meds) {
				mMedicineCheckinList.add(new MedicineCheckinListItem(medicine
						.getName()));
			}
		}
		mAdapter = new MedicineCheckinListAdapter(getActivity(), mMedicineCheckinList, this);
		mListView.setAdapter(mAdapter);
	}

	// TODO: Rename method, update argument and hook method into UI event
	public void onButtonPressed(Uri uri) {
		if (mListener != null) {
			mListener.onFragmentInteraction(uri);
		}

	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			mListener = (OnFragmentInteractionListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement OnFragmentInteractionListener");
		}
	}

	@Override
	public void onDetach() {
		super.onDetach();
		mListener = null;
	}

	/**
	 * This interface must be implemented by activities that contain this
	 * fragment to allow an interaction in this fragment to be communicated to
	 * the activity and potentially other fragments contained in that activity.
	 * <p>
	 * See the Android Training lesson <a href=
	 * "http://developer.android.com/training/basics/fragments/communicating.html"
	 * >Communicating with Other Fragments</a> for more information.
	 */
	public interface OnFragmentInteractionListener {
		// TODO: Update argument type and name
		public void onFragmentInteraction(Uri uri);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (v instanceof CheckBox) {
			CheckBox box = (CheckBox) v;
			if(box.isChecked()) {
				Intent intent = new Intent(getActivity(), MedicineCheckinDateActivity.class);
				intent.putExtra(MedicineCheckinDateActivity.MEDICINE_NAME, box.getText().toString());
				startActivityForResult(intent, 1);
			} else {
				
			}
			
			
		} else if (v == submitButton) {
			CheckIn checkIn = new CheckIn();
			checkIn.setCheckInTimestamp(new Date());
			checkIn.setPatient(app.getPatient());
			String painLevel = painSpinner.getSelectedItem().toString();
			String eating = eatSpinner.getSelectedItem().toString();
			PainLevelType pain = PainLevelType.valueOf(painLevel);
			checkIn.setPainLevel(pain);
			EatingInterferenceType eatingInt = EatingInterferenceType.valueOf(eating);
			checkIn.setEatingInterferenceLevel(eatingInt);
			List<MedicationCheckIn> mchecks = new ArrayList<MedicationCheckIn>();
			for (Object object : mMedicineCheckinList) {
				if (object instanceof MedicineCheckinListItem) {
					MedicineCheckinListItem item = (MedicineCheckinListItem) object;
					if(item.isChecked()) {
						MedicationCheckIn mc = new MedicationCheckIn();
						mc.setCheckIn(checkIn);
						Calendar cal = Calendar.getInstance();
						cal.set(Calendar.HOUR_OF_DAY, item.getHour());
						cal.set(Calendar.MINUTE, item.getMinute());
						mc.setMedicineTaken(cal.getTime());
						mc.setResponse(ResponseType.Yes);
						mc.setMedicine(app.getMedicineByName(item.getMedicineName()));
						mchecks.add(mc);
					}
					
				}
			}
			checkIn.setResponse(mchecks);
			
			CheckInIntentService.startActionSave(getActivity(), checkIn, mReceiver);
		} 
		
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// no request codes yet
		Log.i(Config.TAG, "CheckinFragement onActivityResult called");
		if(resultCode == Activity.RESULT_OK) {
			String medicineName = data.getStringExtra(MedicineCheckinDateActivity.MEDICINE_NAME);
			int currentHour = data.getIntExtra(MedicineCheckinDateActivity.MEDICINE_HOUR, 0);
			int currentMinute = data.getIntExtra(MedicineCheckinDateActivity.MEDICINE_MINUTE, 0);
			MedicineCheckinListItem item = findItemByName(medicineName);
			String time = String.format("%02d:%02d:00", currentHour, currentMinute);
			Log.i(Config.TAG, "Medicine " + medicineName + " Hour " + currentHour + " minute " + currentMinute);
			item.setTimestamp(time);
			item.setHour(currentHour);
			item.setMinute(currentMinute);
			item.setChecked(true);
			setListAdapter();
		}
	}

	private MedicineCheckinListItem findItemByName(String medicineName) {
		for (Object object : mMedicineCheckinList) {
			MedicineCheckinListItem item = (MedicineCheckinListItem) object;
			if(item.getMedicineName().equalsIgnoreCase(medicineName)) {
				return item;
			}
		}
		return null;
	}

	@Override
	public void onReceiveResult(int resultCode, Bundle resultData) {

		Log.i(Config.TAG, "CheckInFragment received resultCode " + resultCode);
		if(resultCode == Activity.RESULT_OK) {
			CheckIn result = (CheckIn) resultData.getSerializable(CheckInIntentService.CHECKIN_RESULT);
			app.addCheckIn(result);
			Log.i(Config.TAG,"Result CheckIn: " + result);
			PatientActivity pa = (PatientActivity) getActivity();
			pa.selectView(PatientActivity.PATIENT_SUMMARY);
		}
		
	}

}
