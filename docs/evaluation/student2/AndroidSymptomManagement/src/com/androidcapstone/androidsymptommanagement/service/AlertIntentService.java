package com.androidcapstone.androidsymptommanagement.service;

import java.util.ArrayList;
import java.util.Collection;

import retrofit.client.ApacheClient;
import retrofit.client.Client;
import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;

import com.androidcapstone.androidsymptommanagement.SymptomManagementApplication;
import com.androidcapstone.androidsymptommanagement.client.Config;
import com.androidcapstone.androidsymptommanagement.client.EasyHttpClient;
import com.androidcapstone.androidsymptommanagement.client.OAuthHandler;
import com.androidcapstone.androidsymptommanagement.client.SecuredRestBuilder;
import com.androidcapstone.symptommanagement.api.AlertService;
import com.androidcapstone.symptommanagement.model.Alert;

public class AlertIntentService extends IntentService {
	

	public enum Status {
		Running, Finished, Error
	}
	
	public enum Operation {
		None, Get, Save, GetByPatient, Acknowledge
	}

	public static final String ALERT_OPERATION = "AlertIntentService.Operation";
	
	public static final String ALERT_RESULT = "AlertIntentService.Result";

	public static final String ALERT_EXTRA_ARG = "AlertIntentService.AlertArg";
	

	
	public AlertIntentService() {
		super("AlertIntentService");
	}
	

	public static void startActionSave(Context ctx, Alert alert, ResultReceiver receiver) {
		Intent intent = new Intent(ctx, AlertIntentService.class);
		Bundle extras = new Bundle();
		extras.putInt(ALERT_OPERATION, Operation.Save.ordinal());
		extras.putParcelable(Config.RESULT_RECEIVER, receiver);
		extras.putSerializable(ALERT_EXTRA_ARG, alert);
		intent.putExtras(extras);
		ctx.startService(intent);
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		// TODO Auto-generated method stub
		Bundle extras = intent.getExtras();
		int request = extras.getInt(ALERT_OPERATION, Operation.None.ordinal());

		final ResultReceiver receiver = extras.getParcelable(Config.RESULT_RECEIVER);
		
		Bundle bundle = new Bundle();
		
		receiver.send(Status.Running.ordinal(), Bundle.EMPTY);
		
		// TODO Error on AccessToken

		AlertService service = newService();
		
		Operation op = Operation.values()[request];
		
		switch(op) {
		case Get:
			doGet(service, receiver);
			break;
		case Save:
			doSave(service, receiver, intent);
			break;
		case None:
			default:
			error(receiver, "Invalid Operation Request");
			Log.i(Config.TAG, "AlertIntentService No Such Operation");
		}
		
		

	}
	



	private void error(ResultReceiver receiver, String string) {
		// TODO Auto-generated method stub
		Bundle resultBundle = new Bundle();
		resultBundle.putString(Intent.EXTRA_TEXT, string);
		receiver.send(Status.Error.ordinal(), resultBundle);
		Log.i(Config.TAG, "Error in AlertService " + string);
	}



	private void doSave(AlertService service, ResultReceiver receiver, Intent intent) {
		try {
			Bundle extras = intent.getExtras();
			Alert r = (Alert) extras.getSerializable(ALERT_EXTRA_ARG);
			Alert saved = service.save(r);
			Bundle resultData = new Bundle ();
			resultData.putSerializable(ALERT_RESULT, saved);
			receiver.send(Status.Finished.ordinal(), resultData);
		} catch (Exception e) {
			error(receiver, "Error saving alert" + e.getMessage());
		}
	}



	private void doGet(AlertService service, ResultReceiver receiver) {

		try {
			Log.i(Config.TAG, "AlertIntentService doGet called");
			Collection<Alert> tmp = service.list();
			ArrayList<Alert> alerts = new ArrayList(tmp);
			Bundle resultData = new Bundle();			
			resultData.putSerializable(ALERT_RESULT, alerts);
			Log.i(Config.TAG, "AlertIntentService returning alerts " + alerts.size());
			receiver.send(Status.Finished.ordinal(), resultData);
		} catch (Exception e) {
			error(receiver, "Error getting reminders " + e.getMessage());
		}
		
	}



	AlertService newService() {
		SymptomManagementApplication app = SymptomManagementApplication.getInstance();
		String accessToken = app.getAccessToken();
		String user = app.getUserName();
		String pass = app.getPassword();
		Log.i(Config.TAG, "User " + user + " pass" + pass + " accessToken " + accessToken);
		Client c = new ApacheClient(new EasyHttpClient());
		OAuthHandler handler = new OAuthHandler(c, Config.LOGIN_ENDPOINT, user, pass, Config.CLIENT_ID, "", accessToken);
		AlertService alerts = new SecuredRestBuilder()
		.setClient(c)
		.setUsername(user).setPassword(pass)
		.setOAuthHandler(handler).build().create(AlertService.class);
		return alerts;
	}

}
