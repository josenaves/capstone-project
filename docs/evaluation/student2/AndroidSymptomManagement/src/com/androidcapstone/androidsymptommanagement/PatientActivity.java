package com.androidcapstone.androidsymptommanagement;


import com.androidcapstone.androidsymptommanagement.client.Config;
import com.androidcapstone.androidsymptommanagement.service.AlertIntentService;
import com.androidcapstone.androidsymptommanagement.service.BasicResultReceiver;
import com.androidcapstone.symptommanagement.model.Alert;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.Toast;

public class PatientActivity extends FragmentActivity implements PatientSummaryFragment.OnFragmentInteractionListener, CheckinFragment.OnFragmentInteractionListener , BasicResultReceiver.Receiver{
	
	public static final String FRAGMENT_TAG = "FRAGMENT_TAG";
	public static final String CHECKIN_FRAGMENT = "CheckinFragment";
	private FragmentNavigationDrawer dlDrawer;
	
	public static final int PATIENT_SUMMARY = 0;
	public static final int MEDICINES = 1;
	public static final int REMINDERS = 2;
	public static final int CHECKIN = 3;
	public static final String INTENT_REASON = "INTENT_REASON";
	public static final String CHECKIN_ALARM = "CHECKIN_ALARM";
	public static final String CHECKIN_REMINDER = "com.androidcapstone.symptommanagement.CHECKIN_REMINDER";
	
	BasicResultReceiver mReceiver;
	
	private boolean isAlarm = false;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_patient);
		Intent intent = getIntent();
		String action = intent.getAction();
		if(null != action && action.equals(CHECKIN_REMINDER)) {
			Log.i(Config.TAG, "PatientActivity received checkinalarm");
			isAlarm = true;
		}
		dlDrawer = (FragmentNavigationDrawer) findViewById(R.id.drawer_layout);
        // Setup drawer view
        dlDrawer.setupDrawerConfiguration((ListView) findViewById(R.id.lvDrawer), 
                     R.layout.drawer_nav_item, R.id.flContent);
        // Add nav items
        
        dlDrawer.addNavItem("Summary", R.drawable.ic_action_summary ,"Summary", PatientSummaryFragment.class, null);
        dlDrawer.addNavItem("Medicines", R.drawable.ic_action_two, "Medicines", MedicineFragment.class, null);
        dlDrawer.addNavItem("Reminders", R.drawable.ic_action_three, "Reminders", ReminderFragment.class, null);
        Bundle a = new Bundle();
        a.putString(FRAGMENT_TAG, CHECKIN_FRAGMENT);
        dlDrawer.addNavItem("CheckIn", -1, "CheckIn", CheckinFragment.class, a);
        // Select default
        if (savedInstanceState == null) {
        	if(isAlarm) {
        		dlDrawer.selectDrawerItem(CHECKIN);
        	} else {
        		dlDrawer.selectDrawerItem(0);
        	}
        }
        
	}
	
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		if (dlDrawer.isDrawerOpen()) {
            // Uncomment to hide menu items                        
            // menu.findItem(R.id.mi_test).setVisible(false);
        }
		return super.onPrepareOptionsMenu(menu);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.patient, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.

		if(item.getItemId() == R.id.action_logout) {
			SymptomManagementApplication.getInstance().clear();
			this.finish();
			Intent intent = new Intent(Intent.ACTION_MAIN);
			intent.addCategory(Intent.CATEGORY_HOME);
			startActivity(intent);
			return true;
		}
      if (dlDrawer.getDrawerToggle().onOptionsItemSelected(item)) {
    	  return true;	
      }
	
	return super.onOptionsItemSelected(item);
	}
	
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        dlDrawer.getDrawerToggle().syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggles
        dlDrawer.getDrawerToggle().onConfigurationChanged(newConfig);
    }

	@Override
	public void onFragmentInteraction(Uri uri) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	protected void onActivityResult(int arg0, int arg1, Intent arg2) {
		super.onActivityResult(arg0, arg1, arg2);
	}
	
	public void selectView(int which) {
		SymptomManagementApplication app = SymptomManagementApplication.getInstance();
		Alert a = app.getAlert();
		if (null != a) {
			 mReceiver = new BasicResultReceiver(new Handler());
			 mReceiver.setReceiver(this);
			 Log.i(Config.TAG,"Launching AlertIntentService to save alert");
			 AlertIntentService.startActionSave(this, a, mReceiver);
			
		} else {
			Log.i(Config.TAG, "Alert is null. Not saving");
		}
		dlDrawer.selectDrawerItem(which);
	}

	@Override
	public void onReceiveResult(int resultCode, Bundle resultData) {
		if(resultCode == RESULT_OK) {
			Alert alert = (Alert) resultData.getSerializable(AlertIntentService.ALERT_RESULT);
			SymptomManagementApplication app = SymptomManagementApplication.getInstance();
			app.setAlert(alert);
			Toast.makeText(getApplicationContext(), "The Doctor has been Alerted to your condition", Toast.LENGTH_LONG).show();
		} else if (resultCode == AlertIntentService.Status.Finished.ordinal()) {
			Alert alert = (Alert) resultData.getSerializable(AlertIntentService.ALERT_RESULT);
			SymptomManagementApplication app = SymptomManagementApplication.getInstance();
			app.setAlert(alert);
			Toast.makeText(getApplicationContext(), "The Doctor has been Alerted to your condition", Toast.LENGTH_LONG).show();
		} else if (resultCode == AlertIntentService.Status.Error.ordinal()) {
			String error = resultData.getString(Intent.EXTRA_TEXT);
			Log.i(Config.TAG, "Error receivng result from AlertIntentService " + error);
			Toast.makeText(getApplicationContext(), "An Error occurred Alerting the Doctor", Toast.LENGTH_LONG).show();
		}
		
	}
}
