package com.androidcapstone.androidsymptommanagement;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;



public class MedicineCheckinListAdapter extends ArrayAdapter {

	
	private Context context;
	private boolean useList = true;
	private OnClickListener listener;
	
	public MedicineCheckinListAdapter(Context context, List items, OnClickListener listener) {
		super(context, android.R.layout.simple_list_item_1, items);
		this.context = context;
		this.listener = listener;
	}
	
	/** * Holder for the list items. */ 
	private class ViewHolder{ CheckBox checkBox; TextView dateView;}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		MedicineCheckinListItem item = (MedicineCheckinListItem) getItem(position);
		View viewToUse = null;
		
		LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
		
		if (convertView == null) 
		{ 
			if(useList){ 
				viewToUse = mInflater.inflate(R.layout.checkin_list_item, null); 
			} else { 
				viewToUse = mInflater.inflate(R.layout.checkin_list_item, null); 
			} 
			holder = new ViewHolder(); 
			holder.checkBox = (CheckBox)viewToUse.findViewById(R.id.medicineTakenCheckBox); 
			holder.checkBox.setText(item.getMedicineName());
			holder.checkBox.setOnClickListener(listener);
			holder.dateView = (TextView)viewToUse.findViewById(R.id.medicineDateText);
			
			viewToUse.setTag(holder); 
		} else {
			viewToUse = convertView; holder = (ViewHolder) viewToUse.getTag(); 
		} 
		holder.dateView.setText(item.getTimestamp()); 
		holder.checkBox.setChecked(item.isChecked());
		return viewToUse;
		
	}
	
}
