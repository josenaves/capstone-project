package com.androidcapstone.androidsymptommanagement;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.ButterKnife;
import butterknife.InjectView;

import com.androidcapstone.androidsymptommanagement.client.Config;
import com.androidcapstone.androidsymptommanagement.dummy.DummyContent;
import com.androidcapstone.androidsymptommanagement.service.ReminderIntentService;
import com.androidcapstone.androidsymptommanagement.service.ReminderIntentServiceResultReceiver;
import com.androidcapstone.symptommanagement.model.Reminder;

/**
 * A fragment representing a list of Items.
 * <p />
 * Large screen devices (such as tablets) are supported by replacing the
 * ListView with a GridView.
 * <p />
 * Activities containing this fragment MUST implement the {@link Callbacks}
 * interface.
 */
public class ReminderFragment extends Fragment implements
		ListView.OnItemClickListener, ReminderIntentServiceResultReceiver.Receiver, OnClickListener {

	

	private OnFragmentInteractionListener mListener;

	/**
	 * The fragment's ListView/GridView.
	 */
	private ListView mListView;

	/**
	 * The Adapter which will be used to populate the ListView/GridView with
	 * Views.
	 */
	private ListAdapter mAdapter;
	
	private SymptomManagementApplication app = SymptomManagementApplication.getInstance();
	
	private AlphaAnimation inAnimation;
	private AlphaAnimation outAnimation;
	
	private ReminderIntentServiceResultReceiver mReceiver = null;
	@InjectView(R.id.reminderProgressBarHolder) FrameLayout progressBarHolder;
	
	Button mAddButton;

	private List mReminderList;

	// TODO: Rename and change types of parameters
	public static ReminderFragment newInstance(String param1, String param2) {
		ReminderFragment fragment = new ReminderFragment();
		Bundle args = new Bundle();

		fragment.setArguments(args);
		return fragment;
	}

	/**
	 * Mandatory empty constructor for the fragment manager to instantiate the
	 * fragment (e.g. upon screen orientation changes).
	 */
	public ReminderFragment() {
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if(app.getReminders().isEmpty()) {
			Log.i(Config.TAG, "Calling ReminderIntentService");
			mReceiver = new ReminderIntentServiceResultReceiver(new Handler ());
			mReceiver.setReceiver(this);
			Intent intent = new Intent(getActivity(), ReminderIntentService.class);
			Bundle extras = new Bundle();
			extras.putInt(ReminderIntentService.REMINDER_OPERATION, ReminderIntentService.Operation.Get.ordinal());
			extras.putParcelable(Config.RESULT_RECEIVER, mReceiver);
			intent.putExtras(extras);
			getActivity().startService(intent);
		}
		// TODO: Change Adapter to display your content

	}

	private void setReminderListAdapter(Collection<Reminder> reminders) {
		mReminderList = new ArrayList();
		for (Reminder reminder : reminders) {
			mReminderList.add(new ReminderListItem(reminder.getTime()));			
		}
		
		mAdapter = new ReminderListAdapter(getActivity(), mReminderList);
		mListView.setAdapter(mAdapter);
		
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_reminder, container,
				false);
		ButterKnife.inject(this, view);
		// Set the adapter
		mListView = (ListView) view.findViewById(android.R.id.list);
		if(!app.getReminders().isEmpty()) {
			Log.i(Config.TAG, "Already have reminders");
			setReminderListAdapter(app.getReminders());
		}

		// Set OnItemClickListener so we can be notified on item clicks
		mListView.setOnItemClickListener(this);
		
		mAddButton = new Button(getActivity());
		mAddButton.setText("Add Reminder");
		mListView.setFooterDividersEnabled(true);
		mListView.addFooterView(mAddButton, null, true);
		mAddButton.setOnClickListener(this);
		return view;
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
//		try {
//			mListener = (OnFragmentInteractionListener) activity;
//		} catch (ClassCastException e) {
//			throw new ClassCastException(activity.toString()
//					+ " must implement OnFragmentInteractionListener");
//		}
	}

	@Override
	public void onDetach() {
		super.onDetach();
		mListener = null;
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		if (null != mListener) {
			// Notify the active callbacks interface (the activity, if the
			// fragment is attached to one) that an item has been selected.
			mListener
					.onFragmentInteraction(DummyContent.ITEMS.get(position).id);
		}
		
		ReminderListItem item = (ReminderListItem) mReminderList.get(position);
		Reminder r = app.getReminderByTime(item.getReminderTime());
		Log.i(Config.TAG, "Clicked on Reminder " + r);
		launchEditReminderActivity(r);
	}

	/**
	 * The default content for this Fragment has a TextView that is shown when
	 * the list is empty. If you would like to change the text, call this method
	 * to supply the text it should use.
	 */
	public void setEmptyText(CharSequence emptyText) {
		View emptyView = mListView.getEmptyView();

		if (emptyText instanceof TextView) {
			((TextView) emptyView).setText(emptyText);
		}
	}

	/**
	 * This interface must be implemented by activities that contain this
	 * fragment to allow an interaction in this fragment to be communicated to
	 * the activity and potentially other fragments contained in that activity.
	 * <p>
	 * See the Android Training lesson <a href=
	 * "http://developer.android.com/training/basics/fragments/communicating.html"
	 * >Communicating with Other Fragments</a> for more information.
	 */
	public interface OnFragmentInteractionListener {
		// TODO: Update argument type and name
		public void onFragmentInteraction(String id);
	}

	@Override
	public void onReceiveResult(int resultCode, Bundle resultData) {

		ReminderIntentService.Status status = ReminderIntentService.Status.values()[resultCode];
		
		switch(status) {
		case Error:
			displayError(resultData);
			stopAnimation();
			break;
		case Finished:
			loadResults(resultData);
			stopAnimation();
			break;
		case Running:
			startAnimation();
			break;
		default:
			break;
			
		}
	}

	private void stopAnimation() {
		// TODO Auto-generated method stub
		Log.i(Config.TAG, "ReminderFragment stoppingAnimation");
		outAnimation = new AlphaAnimation(1f, 0f);
		outAnimation.setDuration(200);
		progressBarHolder.setAnimation(outAnimation);
		progressBarHolder.setVisibility(View.GONE);
	}

	private void startAnimation() {
		Log.i(Config.TAG, "ReminderFragment startingAnimation");
		// TODO Auto-generated method stub
		inAnimation = new AlphaAnimation(0f, 1f);
        inAnimation.setDuration(200);
        progressBarHolder.setAnimation(inAnimation);
        progressBarHolder.setVisibility(View.VISIBLE);
	}

	private void displayError(Bundle resultData) {
		Log.e(Config.TAG, "ReminderFragment ERROR");
		String text = resultData.getString(Intent.EXTRA_TEXT);
		Log.e(Config.TAG, "ReminderFragment ERROR " + text);
		Toast.makeText(getActivity(), text, Toast.LENGTH_LONG).show();;
		
	}

	private void loadResults(Bundle resultData) {
		List<Reminder> reminders = (List<Reminder>) resultData.getSerializable(ReminderIntentService.REMINDER_RESULT);
		app.setReminders(reminders);
		Log.i(Config.TAG, "ReminderFragment loadingResults " + reminders.size());
		setReminderListAdapter(reminders);
		
	}

	@Override
	public void onClick(View v) {
		if(v == mAddButton) {
			launchEditReminderActivity(null);
		}
		
	}
	
	private void launchEditReminderActivity(Reminder r) {
		Intent startEditReminderActivityIntent = new Intent(getActivity(), EditReminderActivity.class);
		if(null != r) {
			Bundle extras = new Bundle();
			extras.putSerializable(EditReminderActivity.REMINDER_ARG, r);
			startEditReminderActivityIntent.putExtras(extras);
		}
		Log.i(Config.TAG, "starting EditReminderActivity");
		getActivity().startActivity(startEditReminderActivityIntent);
	}
	
	@Override
	public void onResume() {
		super.onResume();
		if(!app.getReminders().isEmpty()) {
			setReminderListAdapter(app.getReminders());
		}
	}

}
