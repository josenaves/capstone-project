package com.androidcapstone.androidsymptommanagement.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import retrofit.client.ApacheClient;
import retrofit.client.Client;

import com.androidcapstone.androidsymptommanagement.AlarmReceiver;
import com.androidcapstone.androidsymptommanagement.DoctorUpdateReceiver;
import com.androidcapstone.androidsymptommanagement.PatientActivity;
import com.androidcapstone.androidsymptommanagement.client.Config;
import com.androidcapstone.androidsymptommanagement.client.EasyHttpClient;
import com.androidcapstone.androidsymptommanagement.client.OAuthHandler;
import com.androidcapstone.androidsymptommanagement.client.SecuredRestBuilder;
import com.androidcapstone.androidsymptommanagement.doctor.DoctorActivity;
import com.androidcapstone.androidsymptommanagement.doctor.DoctorApplication;
import com.androidcapstone.symptommanagement.api.AlertService;
import com.androidcapstone.symptommanagement.api.CheckInService;
import com.androidcapstone.symptommanagement.api.MedicineService;
import com.androidcapstone.symptommanagement.api.PatientService;
import com.androidcapstone.symptommanagement.api.ReminderService;
import com.androidcapstone.symptommanagement.model.Alert;
import com.androidcapstone.symptommanagement.model.CheckIn;
import com.androidcapstone.symptommanagement.model.Patient;
import com.androidcapstone.symptommanagement.model.Reminder;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;

public class NotificationService extends IntentService {

	public static final int NOTIFICATION_ID = 0x01;

	private static final String DOCTOR_UPDATE = "com.androidcapstone.symptommanagement.DOCTOR_UPDATE";

	private static final String CHECKIN_REMINDER = "com.androidcapstone.symptommanagement.CHECKIN_REMINDER";

	private static final int UPDATE_DOCTOR_NOTIFICATION = 0x02;

	private NotificationManager mManager;
	
	public NotificationService() {
		super("NotificationService");
	}


	public static void startActionDoctorUpdate(Context context, String user, String password) {
		Intent intent = new Intent(context, NotificationService.class);
		intent.setAction(DOCTOR_UPDATE);
		Bundle extras = new Bundle ();
		extras.putString(Config.USER, user);
		extras.putString(Config.PASS, password);
		intent.putExtras(extras);
		context.startService(intent);
		
	}
	
	public static void startActionReminderNotification(Context context, String user, String password) {
		Intent intent = new Intent(context, NotificationService.class);
		intent.setAction(CHECKIN_REMINDER);
		Bundle extras = new Bundle ();
		extras.putString(Config.USER, user);
		extras.putString(Config.PASS, password);
		intent.putExtras(extras);
		context.startService(intent);
	}
	

	@Override
	protected void onHandleIntent(Intent intent) {
		
		String action = intent.getAction();
		if(DOCTOR_UPDATE.equalsIgnoreCase(action)) {
			handleDoctorUpdate(intent);
		} else if (CHECKIN_REMINDER.equalsIgnoreCase(action)) {
			handleReminder(intent);
		}
		

	}

	private void handleReminder(Intent intent) {

		Bundle extras = intent.getExtras();
		String user = extras.getString(Config.USER);
		String pass = extras.getString(Config.PASS);
		int hour = extras.getInt(AlarmReceiver.HOUR);
		int minute = extras.getInt(AlarmReceiver.MINUTE);
		DoctorApplication dapp = DoctorApplication.getInstance();
		Client c = new ApacheClient(new EasyHttpClient());
		OAuthHandler handler = new OAuthHandler(c, Config.LOGIN_ENDPOINT, user, pass, Config.CLIENT_ID, "", null);
		
		ReminderService reminders = new SecuredRestBuilder()
		.setClient(c)
		.setUsername(user).setPassword(pass)
		.setOAuthHandler(handler).build().create(ReminderService.class);
		
		List<Reminder> r = new ArrayList<Reminder>(reminders.getReminders());
		Collections.sort(r, new Comparator<Reminder>() {

			@Override
			public int compare(Reminder lhs, Reminder rhs) {
				return lhs.getTime().compareTo(rhs.getTime());
			}
		});
		
		Reminder next = null;
		for (Reminder reminder : r) {
			String[] times = reminder.getTime().split(":");
			int h = Integer.parseInt(times[0]);
			int m = Integer.parseInt(times[1]);
			
			if(h >= hour && m > minute ) {
				next = reminder;
			}
		}
		
		if(next == null) {
			next = r.get(0);
		}
		
		AlarmReceiver.launchAlarm(this, next, user, pass);
		String contentTitle = "CheckIn Notification";
		String contentText = "Please click here to CheckIn";
		sendNotification(this, contentTitle, contentText, PatientActivity.class, NOTIFICATION_ID, PatientActivity.CHECKIN_REMINDER);
				
	}

	private void handleDoctorUpdate(Intent intent) {
		// TODO Auto-generated method stub

		Bundle extras = intent.getExtras();
		String user = extras.getString(Config.USER);
		String pass = extras.getString(Config.PASS);
		DoctorApplication dapp = DoctorApplication.getInstance();
		Client c = new ApacheClient(new EasyHttpClient());
		OAuthHandler handler = new OAuthHandler(c, Config.LOGIN_ENDPOINT, user, pass, Config.CLIENT_ID, "", null);
		AlertService alerts = new SecuredRestBuilder()
		.setClient(c)
		.setUsername(user).setPassword(pass)
		.setOAuthHandler(handler).build().create(AlertService.class);

		PatientService patients = new SecuredRestBuilder()
		.setClient(c)
		.setUsername(user).setPassword(pass)
		.setOAuthHandler(handler).build().create(PatientService.class);
		CheckInService checkins = new SecuredRestBuilder()
		.setClient(c)
		.setUsername(user)
		.setPassword(pass)
		.setOAuthHandler(handler).build()
		.create(CheckInService.class);

		boolean isNew = false;
		DoctorApplication app = DoctorApplication.getInstance();
		Collection<Patient> ps = patients.getMyPatients();
		for (Patient patient : ps) {
			if(app.updatePatient(patient)) {
				isNew = true;
			}

			Collection<Alert> as = alerts.listByPatient(patient.getId());
			if(app.updateAlerts(patient, as)) {
				isNew = true;
			}

			Collection<CheckIn> cs = checkins.getCheckinsByPatientLastName(patient.getLastName());
			if(app.updateCheckins(patient, cs)) {
				isNew = true;
			}

		}

		if(isNew) {
			sendNotification(this, "Patient Update", "New Checkins and Alerts", DoctorActivity.class, UPDATE_DOCTOR_NOTIFICATION,"UPDATE_DOCTOR");
		}

		DoctorUpdateReceiver.launchNewUpdateAlarm(this, user, pass);
	}
	
	private void sendNotification(Context context, String title, String text, Class clazz, int notificationId, String action) {
		// TODO Auto-generated method stub
		NotificationCompat.Builder mBuilder =
		        new NotificationCompat.Builder(context)
		        .setSmallIcon(android.R.drawable.stat_sys_warning)
		        .setContentTitle(title)
		        .setContentText(text)
		        .setAutoCancel(true);
		// Creates an explicit intent for an Activity in your app
		Intent resultIntent = new Intent(context, clazz);
		resultIntent.setAction(action);
		// The stack builder object will contain an artificial back stack for the
		// started Activity.
		// This ensures that navigating backward from the Activity leads out of
		// your application to the Home screen.
		TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
		// Adds the back stack for the Intent (but not the Intent itself)
		stackBuilder.addParentStack(DoctorActivity.class);
		// Adds the Intent that starts the Activity to the top of the stack
		stackBuilder.addNextIntent(resultIntent);
		PendingIntent resultPendingIntent =
		        stackBuilder.getPendingIntent(
		            0,
		            PendingIntent.FLAG_UPDATE_CURRENT
		        );
		mBuilder.setContentIntent(resultPendingIntent);
		NotificationManager mNotificationManager =
		    (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		// mId allows you to update the notification later on.
		mNotificationManager.notify(notificationId, mBuilder.build());
	}


}
