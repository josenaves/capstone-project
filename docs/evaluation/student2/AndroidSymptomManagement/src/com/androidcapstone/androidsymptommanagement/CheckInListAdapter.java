package com.androidcapstone.androidsymptommanagement;

import java.util.ArrayList;
import java.util.List;

import com.androidcapstone.androidsymptommanagement.R;
import com.androidcapstone.androidsymptommanagement.client.Config;
import com.androidcapstone.androidsymptommanagement.doctor.AlertListItem;
import com.androidcapstone.androidsymptommanagement.doctor.DoctorApplication;
import com.androidcapstone.symptommanagement.model.Alert;
import com.androidcapstone.symptommanagement.model.CheckIn;
import com.androidcapstone.symptommanagement.model.MedicationCheckIn;

import android.app.Activity;
import android.content.Context;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;



public class CheckInListAdapter extends ArrayAdapter {

	
	private Context context;
	private boolean useList = true;
	private OnClickListener listener;
	
	public CheckInListAdapter(Context context, List items, OnClickListener listener) {
		super(context, android.R.layout.simple_list_item_1, items);
		this.context = context;
		this.listener = listener;
	}
	
	/** * Holder for the list items. */ 
	private class ViewHolder{ 
		
		TextView name;
		TextView date;
		TextView painLevel;
		TextView problemsEating;
		TextView medications;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		CheckInListItem item = (CheckInListItem) getItem(position);
		View viewToUse = null;
		
		LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
		
		if (convertView == null) 
		{ 
			if(useList){ 
				viewToUse = mInflater.inflate(R.layout.checkin_item, null); 
			} else { 
				viewToUse = mInflater.inflate(R.layout.checkin_item, null); 
			} 
			holder = new ViewHolder();
			holder.name = (TextView) viewToUse.findViewById(R.id.textView1);
			holder.date = (TextView) viewToUse.findViewById(R.id.textView2);
			holder.painLevel = (TextView) viewToUse.findViewById(R.id.textView3);
			holder.problemsEating = (TextView) viewToUse.findViewById(R.id.textView4);	
			holder.medications = (TextView) viewToUse.findViewById(R.id.textView5);
			if(null == holder.medications) {
				Log.i(Config.TAG, "Medications ListView is NULL");
			}
			viewToUse.setTag(holder); 
		} else {
			viewToUse = convertView; holder = (ViewHolder) viewToUse.getTag(); 
		} 
		CheckIn checkIn = item.getCheckIn();
		holder.name.setText("Name: " + checkIn.getPatient().getFirstName() + " " + checkIn.getPatient().getLastName());
		holder.date.setText("Date: " + DateFormat.format("yyyy-MM-dd kk:mm", checkIn.getCheckInTimestamp()));
		holder.painLevel.setText("PainLevel: " + checkIn.getPainLevel());
		String eatingProbs = (checkIn.getEatingInterferenceLevel() == null? "No" : checkIn.getEatingInterferenceLevel().toString());
		holder.problemsEating.setText("Problems Eating? " + eatingProbs);
		configureMedicines(holder.medications, checkIn);
		return viewToUse;
		
	}

	private void configureMedicines(TextView medications, CheckIn checkIn) {
		List<MedicationCheckIn> meds = checkIn.getResponse();
		StringBuilder builder = new StringBuilder("Medications:\n");

		
		for (MedicationCheckIn medicationCheckIn : meds) {
			builder.append(medicationCheckIn.getMedicine().getName() + " ");
			builder.append(DateFormat.format("kk:mm", medicationCheckIn.getMedicineTaken()));
			builder.append("\n");
		}
		
		medications.setText(builder.toString());
		
	}
	
}
