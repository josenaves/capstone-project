package com.androidcapstone.androidsymptommanagement.doctor;

import com.androidcapstone.symptommanagement.model.Medicine;

public class MedicineUpdateListItem {

	private String medicineName;
	

	
	private boolean checked = false;
	

	
	private Medicine medicine;

	public String getMedicineName() {
		return medicineName;
	}

	public void setMedicineName(String medicineName) {
		this.medicineName = medicineName;
	}

	public MedicineUpdateListItem(String medicineName) {
		super();
		this.medicineName = medicineName;
	}

	public MedicineUpdateListItem(Medicine medicine) {
		super();
		this.medicine = medicine;
		this.medicineName = medicine.getName();
	}


	public boolean isChecked() {
		return checked;
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}


	public Medicine getMedicine() {
		return medicine;
	}

	public void setMedicine(Medicine medicine) {
		this.medicine = medicine;
	}
	
	
	
}
