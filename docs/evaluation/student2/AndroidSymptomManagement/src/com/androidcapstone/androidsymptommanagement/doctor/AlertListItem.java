package com.androidcapstone.androidsymptommanagement.doctor;

import com.androidcapstone.symptommanagement.model.Alert;

public class AlertListItem {

	private Alert alert;

	public AlertListItem(Alert alert) {
		super();
		this.alert = alert;
	}

	public Alert getAlert() {
		return alert;
	}

	public void setAlert(Alert alert) {
		this.alert = alert;
	}


	
}
