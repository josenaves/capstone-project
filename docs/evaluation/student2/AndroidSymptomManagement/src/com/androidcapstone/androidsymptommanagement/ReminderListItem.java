package com.androidcapstone.androidsymptommanagement;

public class ReminderListItem {

	private String reminderTime;

	public ReminderListItem(String reminderTime) {
		super();
		this.reminderTime = reminderTime;
	}

	public String getReminderTime() {
		return reminderTime;
	}

	public void setReminderTime(String reminderTime) {
		this.reminderTime = reminderTime;
	}


	
	
}
