package com.androidcapstone.androidsymptommanagement;

import com.androidcapstone.symptommanagement.model.Medicine;

public class MedicineCheckinListItem {

	private String medicineName;
	
	private String timestamp;
	
	private boolean checked = false;
	
	private int minute;
	
	private int hour;
	
	private Medicine medicine;

	public String getMedicineName() {
		return medicineName;
	}

	public void setMedicineName(String medicineName) {
		this.medicineName = medicineName;
	}

	public MedicineCheckinListItem(String medicineName) {
		super();
		this.medicineName = medicineName;
	}

	public MedicineCheckinListItem(Medicine medicine) {
		super();
		this.medicine = medicine;
		this.medicineName = medicine.getName();
	}

	public String getTimestamp() {
		// TODO Auto-generated method stub
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public boolean isChecked() {
		return checked;
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}

	public void setMinute(int currentMinute) {
		this.minute = currentMinute;
		
	}

	public void setHour(int currentHour) {
		this.hour = currentHour;
	}

	public int getMinute() {
		return minute;
	}

	public int getHour() {
		return hour;
	}

	public Medicine getMedicine() {
		return medicine;
	}

	public void setMedicine(Medicine medicine) {
		this.medicine = medicine;
	}
	
	
	
}
