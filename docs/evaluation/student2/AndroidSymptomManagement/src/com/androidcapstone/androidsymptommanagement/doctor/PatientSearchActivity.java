package com.androidcapstone.androidsymptommanagement.doctor;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

import com.androidcapstone.androidsymptommanagement.PatientDetailsActivity;
import com.androidcapstone.androidsymptommanagement.R;
import com.androidcapstone.androidsymptommanagement.R.id;
import com.androidcapstone.androidsymptommanagement.R.layout;
import com.androidcapstone.androidsymptommanagement.R.menu;
import com.androidcapstone.androidsymptommanagement.client.Config;
import com.androidcapstone.symptommanagement.model.Patient;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;

public class PatientSearchActivity extends Activity implements OnClickListener {

	
	@InjectView(R.id.autoCompleteTextView1)
	AutoCompleteTextView autoCompleteTextView;
	
	@InjectView(R.id.patientSearchButton)
	Button patientSearchButton;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_patient_search);
		ButterKnife.inject(this);
		addNamesToAutoComplete(getPatientLastName());
		patientSearchButton.setOnClickListener(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.patient_search, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onClick(View v) {
		if(v == patientSearchButton) {
			String text = autoCompleteTextView.getText().toString();
			String[] names = text.split(", ");
			Patient p = DoctorApplication.getInstance().getPatientByName(names[1], names[0]);
			if(null != p) {
				Intent intent = new Intent(PatientSearchActivity.this, PatientDetailsActivity.class);
				Bundle extras = new Bundle();
				extras.putSerializable(PatientDetailsActivity.PATIENT_ARG, p);
				intent.putExtras(extras);
				Log.i(Config.TAG, "Starting PatientDetailsActivity");
				startActivity(intent);
			} else {
				Log.i(Config.TAG, "Did not find patient from autocomplete " + text);
			}
		}
		
	}
	
	private List<String> getPatientLastName() {
		List<Patient> patients = DoctorApplication.getInstance().getPatients();
		List<String> nameList = new ArrayList<String> (patients.size());
		for (Patient patient : patients) {
			nameList.add(patient.getLastName() + ", " + patient.getFirstName());
		}
		return nameList;
	}
	
	private void addNamesToAutoComplete(List<String> names) {
		ArrayAdapter<String> adapter = 
				new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, names);
		autoCompleteTextView.setAdapter(adapter);
	}
}
