package com.androidcapstone.androidsymptommanagement.doctor;

import com.androidcapstone.androidsymptommanagement.PatientSummaryListActivity.PatientSummary;

public class PatientSummaryListItem {

	private PatientSummary summary;

	public PatientSummaryListItem(PatientSummary summary) {
		super();
		this.summary = summary;
	}

	public PatientSummary getSummary() {
		return summary;
	}

	

	
}
