package com.androidcapstone.androidsymptommanagement.doctor;

import java.util.ArrayList;
import java.util.Collection;

import retrofit.client.ApacheClient;
import retrofit.client.Client;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ProgressBar;
import butterknife.ButterKnife;
import butterknife.InjectView;

import com.androidcapstone.androidsymptommanagement.CheckInListActivity;
import com.androidcapstone.androidsymptommanagement.DoctorUpdateReceiver;
import com.androidcapstone.androidsymptommanagement.PatientSummaryListActivity;
import com.androidcapstone.androidsymptommanagement.R;
import com.androidcapstone.androidsymptommanagement.client.Config;
import com.androidcapstone.androidsymptommanagement.client.EasyHttpClient;
import com.androidcapstone.androidsymptommanagement.client.OAuthHandler;
import com.androidcapstone.androidsymptommanagement.client.SecuredRestBuilder;
import com.androidcapstone.symptommanagement.api.AlertService;
import com.androidcapstone.symptommanagement.api.CheckInService;
import com.androidcapstone.symptommanagement.api.MedicineService;
import com.androidcapstone.symptommanagement.api.PatientService;
import com.androidcapstone.symptommanagement.model.Alert;
import com.androidcapstone.symptommanagement.model.CheckIn;
import com.androidcapstone.symptommanagement.model.Medicine;
import com.androidcapstone.symptommanagement.model.Patient;

public class DoctorActivity extends Activity implements OnClickListener {
	
	@InjectView(R.id.patientSearchButton)
	Button mAlertsButton;
	
	@InjectView(R.id.button2)
	Button mAllPatientButton;
	
	@InjectView(R.id.button3)
	Button mSearchPatientsButton;

	@InjectView(R.id.doctor_progress)
	View progress;
	
	@InjectView(R.id.doctor_form)
	View doctorForm;
	
	LoadDoctorTask mLoadTask = null;
	
	private boolean mLoading = false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_doctor);
		ButterKnife.inject(this);
		
		mAlertsButton.setOnClickListener(this);
		mAllPatientButton.setOnClickListener(this);
		mSearchPatientsButton.setOnClickListener(this);
		
		mLoading = true;
		mLoadTask = new LoadDoctorTask();
		mLoadTask.execute((Void)null);
		
	}
	
	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		showProgress(mLoading);

	}

	private void showProgress(final boolean show) {
		// TODO Auto-generated method stub
		int shortAnimTime = getResources().getInteger(
				android.R.integer.config_shortAnimTime);

		doctorForm.setVisibility(show ? View.GONE : View.VISIBLE);
		doctorForm.animate().setDuration(shortAnimTime)
				.alpha(show ? 0 : 1)
				.setListener(new AnimatorListenerAdapter() {
					@Override
					public void onAnimationEnd(Animator animation) {
						doctorForm.setVisibility(show ? View.GONE
								: View.VISIBLE);
					}
				});

		progress.setVisibility(show ? View.VISIBLE : View.GONE);
		progress.animate().setDuration(shortAnimTime)
				.alpha(show ? 1 : 0)
				.setListener(new AnimatorListenerAdapter() {
					@Override
					public void onAnimationEnd(Animator animation) {
						progress.setVisibility(show ? View.VISIBLE
								: View.GONE);
					}
				});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.doctor, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		} else if (id == R.id.doctor_logout) {
			DoctorApplication.getInstance().clear();
			this.finish();
			Intent intent = new Intent(Intent.ACTION_MAIN);
			intent.addCategory(Intent.CATEGORY_HOME);
			startActivity(intent);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public class LoadDoctorTask extends AsyncTask<Void, Void, Boolean> {

		AlertService alerts;
		PatientService patients;
		CheckInService checkins;
		MedicineService medicines;
		
		@Override
		protected void onPreExecute() {
			DoctorApplication dapp = DoctorApplication.getInstance();
			Client c = new ApacheClient(new EasyHttpClient());
			String accessToken = dapp.getAuthToken();
			String user = dapp.getUserName();
			String pass = dapp.getPassword();
			OAuthHandler handler = new OAuthHandler(c, Config.LOGIN_ENDPOINT, user, pass, Config.CLIENT_ID, "", accessToken);
			
			alerts = new SecuredRestBuilder()
			.setClient(c)
			.setUsername(user).setPassword(pass)
			.setOAuthHandler(handler).build().create(AlertService.class);

			patients = new SecuredRestBuilder()
			.setClient(c)
			.setUsername(user).setPassword(pass)
			.setOAuthHandler(handler).build().create(PatientService.class);
			
			checkins = new SecuredRestBuilder()
			.setClient(c)
			.setUsername(user)
			.setPassword(pass)
			.setOAuthHandler(handler).build()
			.create(CheckInService.class);
			
			medicines = new SecuredRestBuilder()
			.setClient(c)
			.setUsername(user)
			.setPassword(pass)
			.setOAuthHandler(handler).build()
			.create(MedicineService.class);
			
			
		}
		
		@Override
		protected Boolean doInBackground(Void... params) {
			DoctorApplication app = DoctorApplication.getInstance();
			Collection<Patient> ps = patients.getMyPatients();
			for (Patient patient : ps) {
				app.addPatient(patient);
				
				Collection<Alert> as = alerts.listByPatient(patient.getId());
				app.addAlerts(patient, as);
				
				Collection<CheckIn> cs = checkins.getCheckinsByPatientLastName(patient.getLastName());
				app.addCheckins(patient, cs);
				
				Collection<Medicine> ms = medicines.getMedicines();
				app.setMedicines(new ArrayList<Medicine>(ms));
			}
			
			return true;
		}
		
		@Override
		protected void onPostExecute(Boolean result) {
			mLoading = false;
			DoctorApplication app = DoctorApplication.getInstance();
			String alertButtonText = mAlertsButton.getText().toString() +  " (" +app.getAlertCount() + ")";
			mAlertsButton.setText(alertButtonText);
			
			String patientsButtonText = mAllPatientButton.getText().toString() + " (" + app.getPatientCount() + ")";
			mAllPatientButton.setText(patientsButtonText);
			showProgress(mLoading);
			DoctorApplication dr = DoctorApplication.getInstance();
			DoctorUpdateReceiver.launchNewUpdateAlarm(DoctorActivity.this, dr.getUserName(), dr.getPassword());
		}
		
		@Override
		protected void onCancelled() {
			mLoadTask = null;
			mLoading = false;
			showProgress(mLoading);
		}
		
	}

	@Override
	public void onClick(View v) {
		if(v == mAlertsButton) {
			Intent intent = new Intent(this, AlertListActivity.class);
			intent.setAction(AlertListActivity.SEE_ALL);
			startActivity(intent);
		} else if (v == mAllPatientButton) {
			Intent intent = new Intent(this, PatientSummaryListActivity.class);
			startActivity(intent);
		} else if (v == mSearchPatientsButton) {
			Intent intent = new Intent(this, PatientSearchActivity.class);
			startActivity(intent);
		}
		
	}
}
