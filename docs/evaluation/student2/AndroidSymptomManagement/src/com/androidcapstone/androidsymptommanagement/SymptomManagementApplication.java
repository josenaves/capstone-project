package com.androidcapstone.androidsymptommanagement;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import android.util.Log;

import com.androidcapstone.androidsymptommanagement.client.Config;
import com.androidcapstone.symptommanagement.model.Alert;
import com.androidcapstone.symptommanagement.model.AlertType;
import com.androidcapstone.symptommanagement.model.CheckIn;
import com.androidcapstone.symptommanagement.model.EatingInterferenceType;
import com.androidcapstone.symptommanagement.model.Medicine;
import com.androidcapstone.symptommanagement.model.PainLevelType;
import com.androidcapstone.symptommanagement.model.Patient;
import com.androidcapstone.symptommanagement.model.Reminder;
import com.androidcapstone.symptommanagement.model.Role;

public class SymptomManagementApplication {
	
	private static SymptomManagementApplication _instance = null;
	
	private static final long TWELVE_HOURS = 12 * 60 * 60 * 1000L;
	
	private static final long SIXTEEN_HOURS = 16 * 60 * 60 * 1000L;
	
	
	
	private SymptomManagementApplication() {
	
	}
	
	public static synchronized SymptomManagementApplication getInstance () {
		if (null == _instance) {
			_instance = new SymptomManagementApplication();
		}
		return _instance;
	}
	
	
	private Patient patient;
	
	private Collection<Reminder> reminders;
	
	private String accessToken;
	
	private String userName;
	
	private String password;
	
	private List<CheckIn> checkins = new ArrayList<CheckIn>();
	
	private Alert alert;
	
	private Date moderateOrSevereStart;
	
	private Date severeStart = new Date(new Date().getTime() - TWELVE_HOURS + (60 * 60 * 1000L));
	
	private Date noEatStart;
	

	public Patient getPatient() {
		return patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}

	public Collection<Reminder> getReminders() {
		if(null == reminders) {
			reminders = Collections.emptyList();
		}
		return reminders;
	}

	public void setReminders(Collection<Reminder> reminders) {
		this.reminders = reminders;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void addReminder(Reminder reminder) {
		// TODO Auto-generated method stub
		Collection<Reminder> reminders = getReminders();
		if (reminders.isEmpty()) {
			reminders = new ArrayList<Reminder>();
			reminders.add(reminder);
		} else {
			if(!reminders.contains(reminder)) {
				reminders.add(reminder);
			} else {
				reminders.remove(reminder);
				reminders.add(reminder);
				List<Reminder> l = (List<Reminder>) reminders;
				Collections.sort(l, new Comparator<Reminder>() {

					@Override
					public int compare(Reminder lhs, Reminder rhs) {
						
						return lhs.getTime().compareTo(rhs.getTime());
					}
				});
				setReminders(l);
			}
		}
		
		
	}

	public Reminder getReminderByTime(String reminderTime) {
		Collection<Reminder> reminders = getReminders();
		for (Reminder reminder : reminders) {
			if(reminder.getTime().equalsIgnoreCase(reminderTime)) {
				return reminder;
			}
		}
		return null;
	}

	public void removeReminder(Reminder reminder) {
		getReminders().remove(reminder);
	}

	public Medicine getMedicineByName(String medicineName) {
		List<Medicine> meds = patient.getMedicines();
		for (Medicine medicine : meds) {
			if(medicine.getName().equalsIgnoreCase(medicineName)) {
				return medicine;
			}
		}
		return null;
	}

	public void addCheckIn(CheckIn result) {
		//checkins.add(result);
		
		processPainLevel(result);
		processNoEat(result);
		
	}



	private void processNoEat(CheckIn result) {
		// TODO Auto-generated method stub
		switch(result.getEatingInterferenceLevel()) {
		case CanNotEat:
			if(null == noEatStart) {
				noEatStart = result.getCheckInTimestamp();
			}
			if(isNoEatAlert(result.getCheckInTimestamp())){
				makeOrIncrementNoEatAlert(result);
			}
			break;
		case No:
			noEatStart = null;
		case Some:
			noEatStart = null;
		default:
			if(null != alert && alert.getAlertType() == AlertType.TwelveHoursNoEat) {
				alert = null;
			}
			break	;
		
		}
	}

	private void makeOrIncrementNoEatAlert(CheckIn result) {
		if(null == alert) {
			alert = makeAlert(result, AlertType.TwelveHoursNoEat);
		} else if (alert.getAlertType() == AlertType.TwelveHoursNoEat) {
			alert.setAlertCount(alert.getAlertCount() + 1);
			alert.setUpdated(result.getCheckInTimestamp());
		} else {
			alert = makeAlert(result, AlertType.TwelveHoursNoEat);
		}
		
	}

	private boolean isNoEatAlert(Date checkInTimestamp) {
		Date plus12Hours = new Date(noEatStart.getTime() + TWELVE_HOURS);
		return !checkInTimestamp.before(plus12Hours);

	}

	private void processPainLevel(CheckIn result) {
		Log.i(Config.TAG, "painLevel is " + result.getPainLevel());
		switch(result.getPainLevel()) {
		case Moderate:
			severeStart = null;
			if(null != alert && alert.getAlertType() == AlertType.TwelveHoursSeverePain) {
				alert = null;
			}
			if(null != moderateOrSevereStart) {
				// check if more than 16
				// if so create an alert
				if(isModerateSevereAlert(result.getCheckInTimestamp())) {
					makeOrincrementModerateSevereAlert(result);
				}
			} else {
				moderateOrSevereStart = result.getCheckInTimestamp();
			}
			break;
		case Severe:
			// if null is severStart, set it
			// // if severe is 12hours, make alert
			// else if moderate is 16hours set it 
			Log.i(Config.TAG, "In Severe Case");
			if(null == severeStart) {
				severeStart = result.getCheckInTimestamp();
			}
			if(null == moderateOrSevereStart) {
				moderateOrSevereStart = result.getCheckInTimestamp();
			}
			if(isSevereAlert(result.getCheckInTimestamp())) {
				Log.i(Config.TAG, "Is Sever Alert");
				makeOrIncrementSevereAlert(result);
			} else if (isModerateSevereAlert(result.getCheckInTimestamp())) {
				Log.i(Config.TAG, "Is ModerateSevere");
				makeOrincrementModerateSevereAlert(result);
			} else {
				Log.i(Config.TAG,"Is Not An Alert");
			}
			break;
		case WellControlled:
			alert = null;
			moderateOrSevereStart = null;
			severeStart = null;
			break;
		default:
			break;
		
		}
		
	}

	private void makeOrIncrementSevereAlert(CheckIn result) {
		if(alert == null) {
			alert = makeAlert(result, AlertType.TwelveHoursSeverePain);
			
		} else if(alert.getAlertType() == AlertType.TwelveHoursNoEat) {
			alert.setAlertCount(alert.getAlertCount() + 1);
			alert.setUpdated(result.getCheckInTimestamp());
		} else if(alert.getAlertType() == AlertType.SixteenHoursModerateToSeverePain) {
			alert = makeAlert(result, AlertType.TwelveHoursSeverePain);
		}
		Log.i(Config.TAG, "Made a SevereAlert");	
	}
	
	private Alert makeAlert(CheckIn result, AlertType alertType) {
		Alert alert = new Alert();
		alert.setPatient(patient);
		alert.setDoctor(patient.getDoctor());
		alert.setAlertType(alertType);
		alert.setCreated(result.getCheckInTimestamp());
		alert.setAckCount(0);
		alert.setAlertCount(1);
		return alert;
	}

	private boolean isSevereAlert(Date checkInTimestamp) {
//		if(null == severeStart) return false;
//		Date plus12Hours = new Date(severeStart.getTime() + TWELVE_HOURS);
//		return !checkInTimestamp.before(plus12Hours);
		return true;
	}

	private void makeOrincrementModerateSevereAlert(CheckIn result) {
		// TODO Auto-generated method stub
		if(null == alert) {
			alert = makeAlert(result, AlertType.SixteenHoursModerateToSeverePain);
		} else if (alert.getAlertType() == AlertType.TwelveHoursSeverePain) {
			alert = makeAlert(result, AlertType.SixteenHoursModerateToSeverePain);
		} else if (alert.getAlertType() == AlertType.TwelveHoursSeverePain) {
			alert.setAlertCount(alert.getAlertCount() + 1);
			alert.setUpdated(result.getCheckInTimestamp());
		}
		Log.i(Config.TAG, "Made a ModerateToSevereAlert");
	}

	private boolean isModerateSevereAlert(Date checkInTimestamp) {
		// TODO Auto-generated method stub
		Date plus16Hours = new Date(moderateOrSevereStart.getTime() + SIXTEEN_HOURS);
		return !checkInTimestamp.before(plus16Hours);
	}

	public Alert getAlert() {
		return alert;
	}

	public void setAlert(Alert alert) {
		this.alert = alert;
	}
	
	
	public static boolean isPatient(Collection<Role> roles) {
		for (Role role : roles) {
			if(role.getName().equalsIgnoreCase("Patient")) {
				return true;
			}
		}
		return false;
	}
	
	public static boolean isDoctor(Collection<Role> roles) {
		for (Role role : roles) {
			if(role.getName().equalsIgnoreCase("Doctor")) {
				return true;
			}
		}
		return false;
	}

	public void clear() {
		this.patient = null;
		if (null != reminders) {
			this.reminders.clear();
		}
		this.accessToken = null;
		this.userName = null;
		this.password = null;
		if (null != this.checkins) {
			this.checkins.clear();
		}
		this.alert = null;
		this.moderateOrSevereStart = null;
		this.severeStart = null;
		this.noEatStart = null;
		
		
	}

}
