package com.androidcapstone.androidsymptommanagement;

import java.text.SimpleDateFormat;

import butterknife.ButterKnife;
import butterknife.InjectView;

import com.androidcapstone.androidsymptommanagement.client.Config;
import com.androidcapstone.androidsymptommanagement.service.GetPatientResultReceiver;
import com.androidcapstone.androidsymptommanagement.service.PatientIntentService;
import com.androidcapstone.symptommanagement.model.Patient;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

/**
 * A simple {@link Fragment} subclass. Activities that contain this fragment
 * must implement the
 * {@link PatientSummaryFragment.OnFragmentInteractionListener} interface to
 * handle interaction events. Use the {@link PatientSummaryFragment#newInstance}
 * factory method to create an instance of this fragment.
 *
 */
public class PatientSummaryFragment extends Fragment implements GetPatientResultReceiver.Receiver {
	// TODO: Rename parameter arguments, choose names that match
	// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER

	
	@InjectView(R.id.patientName) TextView patientName;
	@InjectView(R.id.patientEmail) TextView patientEmail;
	@InjectView(R.id.patientDoB) TextView patientDoB;
	@InjectView(R.id.doctor) TextView doctor;
	@InjectView(R.id.progressBarHolder) FrameLayout progressBarHolder;
	
	private SymptomManagementApplication app = SymptomManagementApplication.getInstance();
	
	private AlphaAnimation inAnimation;
	private AlphaAnimation outAnimation;
	
	private GetPatientResultReceiver mReceiver = null;
	private Patient p;


	private OnFragmentInteractionListener mListener;

	/**
	 * Use this factory method to create a new instance of this fragment using
	 * the provided parameters.
	 *
	 * @param param1
	 *            Parameter 1.
	 * @param param2
	 *            Parameter 2.
	 * @return A new instance of fragment PatientSummaryFragment.
	 */
	// TODO: Rename and change types and number of parameters
	public static PatientSummaryFragment newInstance(String param1,
			String param2) {
		PatientSummaryFragment fragment = new PatientSummaryFragment();
		Bundle args = new Bundle();
		fragment.setArguments(args);
		return fragment;
	}

	public PatientSummaryFragment() {
		// Required empty public constructor
	}
	
	

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (getArguments() != null) {
		}
		
		if(mReceiver == null) {
			mReceiver = new GetPatientResultReceiver(new Handler());
			mReceiver.setReceiver(this);
			Intent intent = new Intent(getActivity(), PatientIntentService.class);
			intent.putExtra(PatientIntentService.PATIENT_REQUEST_EXTRA, PatientIntentService.GET_PATIENT_REQUEST);
			intent.putExtra(Config.RESULT_RECEIVER, mReceiver);
			getActivity().startService(intent);
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		Log.i(Config.TAG, "PatientSummaryFragment onCreateView called");
		View view = inflater.inflate(R.layout.fragment_patient_summary, container,
				false);
		ButterKnife.inject(this, view);
		return view;
	}

	// TODO: Rename method, update argument and hook method into UI event
	public void onButtonPressed(Uri uri) {
		if (mListener != null) {
			mListener.onFragmentInteraction(uri);
		}
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			mListener = (OnFragmentInteractionListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement OnFragmentInteractionListener");
		}
	}

	@Override
	public void onDetach() {
		super.onDetach();
		mListener = null;
	}

	/**
	 * This interface must be implemented by activities that contain this
	 * fragment to allow an interaction in this fragment to be communicated to
	 * the activity and potentially other fragments contained in that activity.
	 * <p>
	 * See the Android Training lesson <a href=
	 * "http://developer.android.com/training/basics/fragments/communicating.html"
	 * >Communicating with Other Fragments</a> for more information.
	 */
	public interface OnFragmentInteractionListener {
		// TODO: Update argument type and name
		public void onFragmentInteraction(Uri uri);
	}

	@Override
	public void onReceiveResult(int resultCode, Bundle resultData) {
		// TODO Auto-generated method stub
		Log.i(Config.TAG, "OnReceiveResultCalled with status " + resultCode);
		if(resultCode == PatientIntentService.STATUS_FINISHED) {
			Patient p = (Patient) resultData.getSerializable(PatientIntentService.PATIENT_SERVICE_RESULT);
			app.setPatient(p);
			Log.i(Config.TAG, "PatientSummaryFragment got " + p);
			this.patientName.setText(p.getFirstName() + " " + p.getLastName());
			this.patientEmail.setText(p.getUserName());
			this.patientDoB.setText(DateFormat.format("yyyy-MM-dd", p.getDateOfBirth()));
			this.doctor.setText(p.getDoctor().getFirstName() + " " + p.getDoctor().getLastName());
			outAnimation = new AlphaAnimation(1f, 0f);
			outAnimation.setDuration(200);
			progressBarHolder.setAnimation(outAnimation);
			progressBarHolder.setVisibility(View.GONE);
			
		} else if (resultCode == PatientIntentService.STATUS_ERROR) { 
			outAnimation = new AlphaAnimation(1f, 0f);
			outAnimation.setDuration(200);
			progressBarHolder.setAnimation(outAnimation);
			progressBarHolder.setVisibility(View.GONE);
            String errorMessage = resultData.getString(Intent.EXTRA_TEXT);
            Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_LONG).show();;
		} else {
			inAnimation = new AlphaAnimation(0f, 1f);
            inAnimation.setDuration(200);
            progressBarHolder.setAnimation(inAnimation);
            progressBarHolder.setVisibility(View.VISIBLE);			
		}
		
	}

}
