package com.androidcapstone.androidsymptommanagement.service;

import retrofit.client.ApacheClient;
import retrofit.client.Client;

import com.androidcapstone.androidsymptommanagement.SymptomManagementApplication;
import com.androidcapstone.androidsymptommanagement.client.Config;
import com.androidcapstone.androidsymptommanagement.client.EasyHttpClient;
import com.androidcapstone.androidsymptommanagement.client.OAuthHandler;
import com.androidcapstone.androidsymptommanagement.client.SecuredRestBuilder;
import com.androidcapstone.androidsymptommanagement.service.ReminderIntentService.Status;
import com.androidcapstone.symptommanagement.api.CheckInService;
import com.androidcapstone.symptommanagement.model.CheckIn;

import android.app.Activity;
import android.app.IntentService;
import android.content.Intent;
import android.content.Context;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class CheckInIntentService extends IntentService {
	// TODO: Rename actions, choose action names that describe tasks that this
	// IntentService can perform, e.g. ACTION_FETCH_NEW_ITEMS
	private static final String ACTION_CHECKIN_SAVE = "com.androidcapstone.androidsymptommanagement.action.CheckInSave";
	private static final String ACTION_BAZ = "com.androidcapstone.androidsymptommanagement.action.BAZ";

	// TODO: Rename parameters
	private static final String CHECKIN_SAVE_PARAM1 = "com.androidcapstone.androidsymptommanagement.extra.CheckInSaveParam";
	private static final String EXTRA_PARAM2 = "com.androidcapstone.androidsymptommanagement.extra.PARAM2";
	public static final String CHECKIN_RESULT = "com.androidcapstone.androidsymptommanagement.extra.CheckInResult";
	public static final int ERROR_CODE = 0x03;
	

	/**
	 * Starts this service to perform action Foo with the given parameters. If
	 * the service is already performing a task this action will be queued.
	 *
	 * @see IntentService
	 */
	
	
	public static void startActionSave(Context context, CheckIn checkIn, ResultReceiver receiver) {
		Intent intent = new Intent(context, CheckInIntentService.class);
		intent.setAction(ACTION_CHECKIN_SAVE);
		Bundle extras = new Bundle();
		extras.putParcelable(Config.RESULT_RECEIVER, receiver);
		extras.putSerializable(CHECKIN_SAVE_PARAM1, checkIn);
		intent.putExtras(extras);
		context.startService(intent);
	}

	/**
	 * Starts this service to perform action Baz with the given parameters. If
	 * the service is already performing a task this action will be queued.
	 *
	 * @see IntentService
	 */
	// TODO: Customize helper method
	public static void startActionBaz(Context context, String param1,
			String param2) {
		Intent intent = new Intent(context, CheckInIntentService.class);
		intent.setAction(ACTION_BAZ);
		intent.putExtra(CHECKIN_SAVE_PARAM1, param1);
		intent.putExtra(EXTRA_PARAM2, param2);
		context.startService(intent);
	}

	public CheckInIntentService() {
		super("CheckInIntentService");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		if (intent != null) {
			final String action = intent.getAction();
			if (ACTION_CHECKIN_SAVE.equals(action)) {
				Bundle extras = intent.getExtras();
				ResultReceiver receiver = extras.getParcelable(Config.RESULT_RECEIVER);
				CheckIn checkIn = (CheckIn) extras.getSerializable(CHECKIN_SAVE_PARAM1);
				handleActionSave(receiver, checkIn);
			} else if (ACTION_BAZ.equals(action)) {
				final String param1 = intent.getStringExtra(CHECKIN_SAVE_PARAM1);
				final String param2 = intent.getStringExtra(EXTRA_PARAM2);
				handleActionBaz(param1, param2);
			}
		}
	}

	/**
	 * Handle action Foo in the provided background thread with the provided
	 * parameters.
	 */
	private void handleActionSave(ResultReceiver receiver, CheckIn checkIn) {

		try {
			CheckInService service = newService();
			CheckIn resp = service.save(checkIn);
			Bundle resultData = new Bundle();
			resultData.putSerializable(CHECKIN_RESULT, resp);
			receiver.send(Activity.RESULT_OK, resultData);
		} catch (Exception e) {
			error(receiver, "Error Saving Checkin " + e.getMessage());
		}

	}

	private void error(ResultReceiver receiver, String string) {
		// TODO Auto-generated method stub
		Bundle resultBundle = new Bundle();
		resultBundle.putString(Intent.EXTRA_TEXT, string);
		receiver.send(ERROR_CODE, resultBundle);
		Log.i(Config.TAG, "Error in CheckInIntentService " + string);
	}

	private CheckInService newService() {
		SymptomManagementApplication app = SymptomManagementApplication.getInstance();
		Client c = new ApacheClient(new EasyHttpClient());
		OAuthHandler handler = new OAuthHandler(c, Config.LOGIN_ENDPOINT, app.getUserName(), app.getPassword(), Config.CLIENT_ID, "", app.getAccessToken());
		CheckInService checkins = new SecuredRestBuilder()
		.setClient(c)
		.setUsername(app.getUserName())
		.setPassword(app.getPassword())
		.setOAuthHandler(handler).build()
		.create(CheckInService.class);
		return checkins;
	}

	/**
	 * Handle action Baz in the provided background thread with the provided
	 * parameters.
	 */
	private void handleActionBaz(String param1, String param2) {
		// TODO: Handle action Baz
		throw new UnsupportedOperationException("Not yet implemented");
	}
}
