package com.androidcapstone.androidsymptommanagement.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import retrofit.client.ApacheClient;
import retrofit.client.Client;
import retrofit.client.Response;

import com.androidcapstone.androidsymptommanagement.SymptomManagementApplication;
import com.androidcapstone.androidsymptommanagement.client.Config;
import com.androidcapstone.androidsymptommanagement.client.EasyHttpClient;
import com.androidcapstone.androidsymptommanagement.client.OAuthHandler;
import com.androidcapstone.androidsymptommanagement.client.SecuredRestBuilder;
import com.androidcapstone.symptommanagement.api.PatientService;
import com.androidcapstone.symptommanagement.api.ReminderService;
import com.androidcapstone.symptommanagement.model.Patient;
import com.androidcapstone.symptommanagement.model.Reminder;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;

public class ReminderIntentService extends IntentService {
	

	public enum Status {
		Running, Finished, Error
	}
	
	public enum Operation {
		None, Get, Save, Delete
	}

	public static final String REMINDER_OPERATION = "ReminderIntentService.Operation";
	
	public static final String REMINDER_RESULT = "ReminderIntentService.Result";

	public static final String REMINDER_EXTRA_ARG = "ReminderIntentService.ReminderArg";
	

	
	public ReminderIntentService() {
		super("ReminderIntentService");
	}
	


	@Override
	protected void onHandleIntent(Intent intent) {
		// TODO Auto-generated method stub
		Bundle extras = intent.getExtras();
		int request = extras.getInt(REMINDER_OPERATION, Operation.None.ordinal());

		final ResultReceiver receiver = extras.getParcelable(Config.RESULT_RECEIVER);
		
		Bundle bundle = new Bundle();
		
		receiver.send(Status.Running.ordinal(), Bundle.EMPTY);
		
		// TODO Error on AccessToken
		SymptomManagementApplication app = SymptomManagementApplication.getInstance();
		String accessToken = app.getAccessToken();
		String user = app.getUserName();
		String pass = app.getPassword();
		Log.i(Config.TAG, "User " + user + " pass" + pass + " accessToken " + accessToken);
		ReminderService service = newService(user, pass,accessToken);
		
		Operation op = Operation.values()[request];
		
		switch(op) {
		case Get:
			doGet(service, receiver);
			break;
		case Save:
			doSave(service, receiver, intent);
			break;
		case Delete:
			doDelete(service, receiver, intent);
			break;
		case None:
			default:
			error(receiver, "Invalid Operation Request");
			Log.i(Config.TAG, "ReminderIntentService No Such Operation");
		}
		
		

	}
	
	private void doDelete(ReminderService service, ResultReceiver receiver,
			Intent intent) {
		try {
			Bundle extras = intent.getExtras();
			Reminder r = (Reminder) extras.getSerializable(REMINDER_EXTRA_ARG);
			Log.i(Config.TAG, "About to call delete");
			Response resp = service.delete(r);
			Log.i(Config.TAG, "Delete returned status " + resp.getStatus());
			receiver.send(Status.Finished.ordinal(), Bundle.EMPTY);
		} catch (Exception e) {
			error(receiver, "Error saving Reminder " + e.getMessage());
		}
		
	}



	private void error(ResultReceiver receiver, String string) {
		// TODO Auto-generated method stub
		Bundle resultBundle = new Bundle();
		resultBundle.putString(Intent.EXTRA_TEXT, string);
		receiver.send(Status.Error.ordinal(), resultBundle);
		Log.i(Config.TAG, "Error in ReminderService " + string);
	}



	private void doSave(ReminderService service, ResultReceiver receiver, Intent intent) {
		try {
			Bundle extras = intent.getExtras();
			Reminder r = (Reminder) extras.getSerializable(REMINDER_EXTRA_ARG);
			Reminder saved = service.save(r);
			Bundle resultData = new Bundle ();
			resultData.putSerializable(REMINDER_RESULT, saved);
			receiver.send(Status.Finished.ordinal(), resultData);
		} catch (Exception e) {
			error(receiver, "Error saving Reminder " + e.getMessage());
		}
	}



	private void doGet(ReminderService service, ResultReceiver receiver) {

		try {
			Log.i(Config.TAG, "ReminderIntentService doGet called");
			Collection<Reminder> tmp = service.getReminders();
			ArrayList<Reminder> reminders = new ArrayList(tmp);
			Bundle resultData = new Bundle();			
			resultData.putSerializable(REMINDER_RESULT, reminders);
			Log.i(Config.TAG, "ReminderIntentService returning reminders " + reminders.size());
			receiver.send(Status.Finished.ordinal(), resultData);
		} catch (Exception e) {
			error(receiver, "Error getting reminders " + e.getMessage());
		}
		
	}



	ReminderService newService(String user, String pass, String accessToken) {
		Client c = new ApacheClient(new EasyHttpClient());
		OAuthHandler handler = new OAuthHandler(c, Config.LOGIN_ENDPOINT, user, pass, Config.CLIENT_ID, "", accessToken);
		ReminderService reminders = new SecuredRestBuilder()
		.setClient(c)
		.setUsername(user).setPassword(pass)
		.setOAuthHandler(handler).build().create(ReminderService.class);
		return reminders;
	}

}
