package com.androidcapstone.androidsymptommanagement;

import com.androidcapstone.androidsymptommanagement.client.Config;

import butterknife.ButterKnife;
import butterknife.InjectView;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.TimePicker;

public class MedicineCheckinDateActivity extends Activity implements OnClickListener{

	public static final String MEDICINE_NAME = "MedicineCheckinDateActivity.MedicineName";
	public static final String MEDICINE_HOUR = "MedicineCheckinDateActivity.MedicineHour";
	public static final String MEDICINE_MINUTE = "MedicineCheckinDateActivity.MedicineMinute";
	
	@InjectView(R.id.whatMed)
	TextView whatMed;
	
	@InjectView(R.id.medicineTimePicker)
	TimePicker medicineTimePicker;
	
	@InjectView(R.id.medicineDateSubmit)
	Button submit;
	
	@InjectView(R.id.medicineDateCancel)
	Button cancel;
	
	private String mMedicineName;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_medicine_checkin_date);
		ButterKnife.inject(this);
		
		Intent intent = getIntent();
		mMedicineName = intent.getStringExtra(MEDICINE_NAME);
		String whatMedText = whatMed.getText().toString();
		whatMedText = whatMedText + " " + mMedicineName + "?";
		whatMed.setText(whatMedText);
		submit.setOnClickListener(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.medicine_checkin_date, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onClick(View v) {
		if(v == cancel) {
			Intent returnIntent = new Intent();
			returnIntent.putExtra(MEDICINE_NAME, mMedicineName);
			setResult(RESULT_CANCELED, returnIntent);
			Log.i(Config.TAG, "Med Checkin Canceled");
			finish();
		} else if (v == submit) {
			int currentHour = medicineTimePicker.getCurrentHour();
			int currentMinute = medicineTimePicker.getCurrentMinute();
			Intent returnIntent = new Intent();
			returnIntent.putExtra(MEDICINE_NAME, mMedicineName);
			returnIntent.putExtra(MEDICINE_HOUR, currentHour);
			returnIntent.putExtra(MEDICINE_MINUTE, currentMinute);
			Log.i(Config.TAG, "Med Checkin complete med " + mMedicineName + " hour " + currentHour + " min " + currentMinute);
			setResult(RESULT_OK, returnIntent);
			finish();
		}
		
	}
}
