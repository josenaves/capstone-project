package com.androidcapstone.androidsymptommanagement.doctor;

import java.util.List;

import com.androidcapstone.androidsymptommanagement.R;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;



public class MedicineUpdateListAdapter extends ArrayAdapter {

	
	private Context context;
	private boolean useList = true;
	private OnClickListener listener;
	
	public MedicineUpdateListAdapter(Context context, List items, OnClickListener listener) {
		super(context, android.R.layout.simple_list_item_1, items);
		this.context = context;
		this.listener = listener;
	}
	
	/** * Holder for the list items. */ 
	private class ViewHolder{ CheckBox checkBox; }
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		MedicineUpdateListItem item = (MedicineUpdateListItem) getItem(position);
		View viewToUse = null;
		
		LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
		

		if (null == convertView) {
			viewToUse = mInflater.inflate(R.layout.update_medicine_checkbox, null);
			holder = new ViewHolder();
			holder.checkBox = (CheckBox) viewToUse.findViewById(R.id.updateMedicineCheckBox);
			viewToUse.setTag(holder);
		} else {
			viewToUse = convertView;
			holder = (ViewHolder) viewToUse.getTag();
		}
		holder.checkBox.setText(item.getMedicineName());
		holder.checkBox.setOnClickListener(listener);
		holder.checkBox.setChecked(item.isChecked());
		return viewToUse;
		
	}
	
}
