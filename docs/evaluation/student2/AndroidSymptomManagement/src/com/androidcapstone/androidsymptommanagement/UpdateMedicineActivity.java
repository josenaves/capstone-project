package com.androidcapstone.androidsymptommanagement;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import retrofit.client.ApacheClient;
import retrofit.client.Client;
import retrofit.client.Response;

import com.androidcapstone.androidsymptommanagement.client.Config;
import com.androidcapstone.androidsymptommanagement.client.EasyHttpClient;
import com.androidcapstone.androidsymptommanagement.client.OAuthHandler;
import com.androidcapstone.androidsymptommanagement.client.SecuredRestBuilder;
import com.androidcapstone.androidsymptommanagement.doctor.DoctorApplication;
import com.androidcapstone.androidsymptommanagement.doctor.MedicineUpdateListAdapter;
import com.androidcapstone.androidsymptommanagement.doctor.MedicineUpdateListItem;
import com.androidcapstone.symptommanagement.api.LoginService;
import com.androidcapstone.symptommanagement.api.PatientService;
import com.androidcapstone.symptommanagement.model.Medicine;
import com.androidcapstone.symptommanagement.model.Patient;

import butterknife.ButterKnife;
import butterknife.InjectView;
import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class UpdateMedicineActivity extends Activity implements OnClickListener {
	
	

	public static final String PATIENT_ARG = "PATIENT_ARG";

	public static final String PATIENT_RESULT = "PATIENT_RESULT";

	@InjectView(R.id.textView1)
	TextView prescribedFor;
	
	@InjectView(R.id.medListView)
	ListView listView;
	
	@InjectView(R.id.button1)
	Button submitButton;
	
	@InjectView(R.id.button2)
	Button cancelButton;
	
	private Patient mPatient;
	
	private List mMedicineCheckinList;
	
	private ListAdapter mAdapter;
	
	private AsyncTask<Void, Void, Patient> mTask;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_update_medicine);
		ButterKnife.inject(this);
		submitButton.setOnClickListener(this);
		cancelButton.setOnClickListener(this);
		Intent intent = getIntent();
		
		Bundle extras = intent.getExtras();
		
		mPatient = (Patient) extras.getSerializable(PATIENT_ARG);
		prescribedFor.setText(prescribedFor.getText() + " " + mPatient.getFirstName() + " " + mPatient.getLastName());
		setListAdapter();
	}

	private void setListAdapter() {
		// TODO Auto-generated method stub

		mMedicineCheckinList = new ArrayList();
		DoctorApplication drapp = DoctorApplication.getInstance();

		Collection<Medicine> pmeds = mPatient.getMedicines();
		List<Medicine> meds = drapp.getMedicines();
		for (Medicine medicine : meds) {
			MedicineUpdateListItem item = new MedicineUpdateListItem(medicine);
			mMedicineCheckinList.add(item);
			if(pmeds.contains(medicine)) {
				item.setChecked(true);
			}
		}

		Log.i(Config.TAG, "UpdateMedicineActivity num meds " + mMedicineCheckinList.size());
		mAdapter = new MedicineUpdateListAdapter(this, mMedicineCheckinList, this);
		listView.setAdapter(mAdapter);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.update_medicine, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onClick(View v) {
		if(v == cancelButton) {
			setResult(RESULT_CANCELED, new Intent());
			finish();
		} else if (v == submitButton) {
			mTask = new UpdateMedicineTask(mPatient);
			mTask.execute((Void)null);
		} else if (v instanceof CheckBox) {
			CheckBox box = (CheckBox) v;
			Log.i(Config.TAG, "CheckBox clicked " + box.getText());
			MedicineUpdateListItem item = findItemByName(box.getText().toString());
			if(item != null) {
				item.setChecked(!item.isChecked());
			} else {
				Log.i(Config.TAG, "item is null in onClick");
			}
		}
		
	}
	
	private MedicineUpdateListItem findItemByName(String name) {
		for (Object object : mMedicineCheckinList) {
			MedicineUpdateListItem item = (MedicineUpdateListItem) object;
			if(name.equalsIgnoreCase(item.getMedicineName())) {
				return item;
			}
		}
		return null;
	}

	public class UpdateMedicineTask extends AsyncTask<Void, Void, Patient> {

		private PatientService patients;
		Patient p;
		public UpdateMedicineTask(Patient patient) {
			if(null == patient) {
				Log.e(Config.TAG, "Pateint is NULL in UpdateMedicineTask ctor");
			}
			p = patient;
			DoctorApplication app = DoctorApplication.getInstance();
			Client c = new ApacheClient(new EasyHttpClient());
			OAuthHandler hdlr = new OAuthHandler(c, Config.LOGIN_ENDPOINT, app.getUserName(), app.getPassword(), Config.CLIENT_ID, "", app.getAuthToken());
			patients= new SecuredRestBuilder()
			.setClient(c)
			.setUsername(app.getUserName()).setPassword(app.getPassword())
			.setOAuthHandler(hdlr)
			.build().create(PatientService.class);
			
		}
		
		@Override
		protected Patient doInBackground(Void... params) {
			List<Medicine> medicines = p.getMedicines();
			for (Object obj : mMedicineCheckinList) {
				if (obj instanceof MedicineUpdateListItem) {
					MedicineUpdateListItem item = (MedicineUpdateListItem) obj;
					Medicine m = item.getMedicine();
					if(item.isChecked()) {
						Log.i(Config.TAG,"item " + m.getName() + " is checked");
						if(!medicines.contains(m)) {
							Log.i(Config.TAG, "Adding medicine " + m.getName() + " for patient " + p.getLastName());
							Response resp = patients.updateMedicine(p.getId(), m.getId(), "ADD");
							medicines.add(m);
						} else {
							Log.i(Config.TAG, "Can't ADD medicines  contains " + m.getName());
						}
					} else {
						Log.i(Config.TAG,"item " + m.getName() + " is NOT checked");
						if(medicines.contains(m)) {
							Log.i(Config.TAG, "Deleting medicine " + m.getName() + " for patient " + p.getLastName());
							Response resp = patients.updateMedicine(p.getId(), m.getId(), "DELETE");
							medicines.remove(medicines.indexOf(m));
						} else {
							Log.i(Config.TAG, "Can't DELETE medicines  does not contain " + m.getName());
						}
					}
				} else {
					Log.i(Config.TAG, "Hmm.. Not instance of MedicineCheckinListItem");
				}
			}
			
			// TODO Auto-generated method stub
			return p;
		}
		
		@Override
		protected void onPostExecute(Patient result) {
			if(result == null) {
				Log.e(Config.TAG, "Patient is NULL in onPostExecute");
			} else {
				Log.i(Config.TAG, "Patient is NOT NULL in onPostExecute");
			}
			Intent intent = new Intent();
			Bundle b = new Bundle();
			b.putSerializable(UpdateMedicineActivity.PATIENT_RESULT, result);
			intent.putExtras(b);
			UpdateMedicineActivity.this.setResult(RESULT_OK, intent);
			UpdateMedicineActivity.this.finish();
		}
		
	}
}
