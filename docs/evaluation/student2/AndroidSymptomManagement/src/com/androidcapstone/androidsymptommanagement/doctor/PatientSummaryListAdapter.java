package com.androidcapstone.androidsymptommanagement.doctor;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

import com.androidcapstone.androidsymptommanagement.PatientSummaryListActivity.PatientSummary;
import com.androidcapstone.androidsymptommanagement.R;
import com.androidcapstone.symptommanagement.model.Alert;

import android.app.Activity;
import android.content.Context;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;



public class PatientSummaryListAdapter extends ArrayAdapter<PatientSummaryListItem> {

	
	private Context context;
	private boolean useList = true;
	private OnClickListener listener;
	
	public PatientSummaryListAdapter(Context context, List<PatientSummaryListItem> items, OnClickListener listener) {
		super(context, android.R.layout.simple_list_item_1, items);
		this.context = context;
		this.listener = listener;
	}
	
	/** * Holder for the list items. */ 
	class ViewHolder{
		@InjectView(R.id.name)
		TextView name;
		@InjectView(R.id.dob)
		TextView dob;
		@InjectView(R.id.lastCheckIn)
		TextView lastCheckIn;
		@InjectView(R.id.painLevel)
		TextView painLevel;
		@InjectView(R.id.eatingProblem)
		TextView eatingProblem;
		@InjectView(R.id.alertLevel)
		TextView alertLevel;
		
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		PatientSummaryListItem item =  getItem(position);
		View viewToUse = null;
		
		LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
		
		if (convertView == null) 
		{ 
			if(useList){ 
				viewToUse = mInflater.inflate(R.layout.patient_summary_item, null); 
			} else { 
				viewToUse = mInflater.inflate(R.layout.alert_list_item, null); 
			} 
			holder = new ViewHolder();
			ButterKnife.inject(holder, viewToUse);
			viewToUse.setTag(holder); 
		} else {
			viewToUse = convertView; holder = (ViewHolder) viewToUse.getTag(); 
		} 
		PatientSummary sum = item.getSummary();
		holder.name.setText("Name: " + sum.name);
		holder.dob.setText("DOB: " + DateFormat.format("yyyy-MM-dd", sum.dob));
		holder.lastCheckIn.setText("Last CheckIn: " + (null == sum.lastCheckIn?"N/A":DateFormat.format("yyyy-MM-dd kk:mm",sum.lastCheckIn)));
		holder.painLevel.setText("Pain Level: " + (null == sum.painLevel?"N/A":sum.painLevel.toString()));
		holder.eatingProblem.setText("Problems Eating: " + (null == sum.eatingProblems? "N/A":sum.eatingProblems.toString()));
		holder.alertLevel.setText("Alert Level: " + (null == sum.alertType? "N/A":sum.alertType.toString()));
		return viewToUse;
		
	}
	
}
