package com.androidcapstone.androidsymptommanagement;

public class MedicineListItem {

	private String medicineName;

	public String getMedicineName() {
		return medicineName;
	}

	public void setMedicineName(String medicineName) {
		this.medicineName = medicineName;
	}

	public MedicineListItem(String medicineName) {
		super();
		this.medicineName = medicineName;
	}
	
	
}
