package com.androidcapstone.androidsymptommanagement;

import java.util.Calendar;

import com.androidcapstone.androidsymptommanagement.client.Config;
import com.androidcapstone.androidsymptommanagement.service.NotificationService;
import com.androidcapstone.symptommanagement.model.Reminder;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

public class AlarmReceiver extends BroadcastReceiver {

	public static final String HOUR = "com.androidcapstone.symptommanagement.HOUR";
	public static final String MINUTE = "com.androidcapstone.symptommanagement.MINUTE";

	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub
		Log.i(Config.TAG, "Alarm recieved");
		Bundle extras = intent.getExtras();
		String user = extras.getString(Config.USER);
		String pass = extras.getString(Config.PASS);
		NotificationService.startActionReminderNotification(context, user, pass);
	}

	public static void launchAlarm(Context context,
			Reminder reminder, String user, String pass) {

		// TODO Auto-generated method stub
		String time = reminder.getTime();
		String[] tmp = time.split(":");
		int hours = Integer.parseInt(tmp[0]);
		int minutes = Integer.parseInt(tmp[1]);
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.HOUR_OF_DAY, hours);
		cal.set(Calendar.MINUTE, minutes);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		Intent intent = new Intent(context, AlarmReceiver.class);
		intent.setData(Uri.parse("custom://" + reminder.getId()));
	    intent.setAction(String.valueOf(reminder.getId()));
	    Bundle extras = new Bundle();
	    extras.putString(Config.USER, user);
	    extras.putString(Config.PASS, pass);
	    extras.putInt(HOUR, hours);
	    extras.putInt(MINUTE, minutes);
	    intent.putExtras(extras);
	    Calendar now = Calendar.getInstance();
	    if(now.after(cal)) {
	    	cal.add(Calendar.DATE, 1);
	    }
	    Log.i(Config.TAG, "Alarm set for " + (cal.getTimeInMillis() - now.getTimeInMillis()) + " secs");
	    PendingIntent displayIntent = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
	    AlarmManager alarmManager = (AlarmManager) context.getSystemService(context.ALARM_SERVICE);
	    alarmManager.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), displayIntent);

		
	}

}
