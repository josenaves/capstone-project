package com.androidcapstone.androidsymptommanagement;

import java.util.ArrayList;
import java.util.Collection;

import retrofit.client.ApacheClient;
import retrofit.client.Client;

import com.androidcapstone.androidsymptommanagement.client.Config;
import com.androidcapstone.androidsymptommanagement.client.EasyHttpClient;
import com.androidcapstone.androidsymptommanagement.client.OAuthHandler;
import com.androidcapstone.androidsymptommanagement.client.SecuredRestBuilder;
import com.androidcapstone.androidsymptommanagement.doctor.DoctorActivity;
import com.androidcapstone.androidsymptommanagement.doctor.DoctorApplication;
import com.androidcapstone.androidsymptommanagement.service.NotificationService;
import com.androidcapstone.symptommanagement.api.AlertService;
import com.androidcapstone.symptommanagement.api.CheckInService;
import com.androidcapstone.symptommanagement.api.MedicineService;
import com.androidcapstone.symptommanagement.api.PatientService;
import com.androidcapstone.symptommanagement.model.Alert;
import com.androidcapstone.symptommanagement.model.CheckIn;
import com.androidcapstone.symptommanagement.model.Medicine;
import com.androidcapstone.symptommanagement.model.Patient;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;

public class DoctorUpdateReceiver extends BroadcastReceiver {
	
	private static final long ALARM_TIME = 2 * 10 * 1000L;
	
	private static final int NOTIFICATION_ID = Config.DOCTOR_UPDATE_NOTIFICATION;
	
	public DoctorUpdateReceiver() {
	}

	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO: This method is called when the BroadcastReceiver is receiving
		// an Intent broadcast.
		Bundle extras = intent.getExtras();
		String user = extras.getString(Config.USER);
		String pass = extras.getString(Config.PASS);
		NotificationService.startActionDoctorUpdate(context, user, pass);
	}
	
	public void onUpdateComplete(Context context, boolean result, String user, String pass) {
		if(result) {
			sendNotification(context);
		}
		
		
	}
	
	private void sendNotification(Context context) {
		// TODO Auto-generated method stub
		NotificationCompat.Builder mBuilder =
		        new NotificationCompat.Builder(context)
		        .setSmallIcon(android.R.drawable.stat_sys_warning)
		        .setContentTitle("My notification")
		        .setContentText("Hello World!");
		// Creates an explicit intent for an Activity in your app
		Intent resultIntent = new Intent(context, DoctorActivity.class);

		// The stack builder object will contain an artificial back stack for the
		// started Activity.
		// This ensures that navigating backward from the Activity leads out of
		// your application to the Home screen.
		TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
		// Adds the back stack for the Intent (but not the Intent itself)
		stackBuilder.addParentStack(DoctorActivity.class);
		// Adds the Intent that starts the Activity to the top of the stack
		stackBuilder.addNextIntent(resultIntent);
		PendingIntent resultPendingIntent =
		        stackBuilder.getPendingIntent(
		            0,
		            PendingIntent.FLAG_UPDATE_CURRENT
		        );
		mBuilder.setContentIntent(resultPendingIntent);
		NotificationManager mNotificationManager =
		    (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		// mId allows you to update the notification later on.
		mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
	}

	public static void launchNewUpdateAlarm(Context context, String user, String pass) {
			Intent receiver = new Intent(context, DoctorUpdateReceiver.class);
			Bundle extras = new Bundle ();
			extras.putString(Config.USER, user);
			extras.putString(Config.PASS, pass);
			receiver.putExtras(extras);
			PendingIntent recurring = PendingIntent.getBroadcast(context, 0, receiver, PendingIntent.FLAG_CANCEL_CURRENT);
			AlarmManager alarmManager = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
			alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + ALARM_TIME, recurring);
	}
	
	
	public class DoctorUpdateTask  extends AsyncTask<Void, Void, Boolean>{

		AlertService alerts;
		PatientService patients;
		CheckInService checkins;
		MedicineService medicines;
		Context context;
		String user;
		String pass;
		
		public DoctorUpdateTask(Context context, String user, String pass) {
			// TODO Auto-generated constructor stub
			this.context = context;
			this.user = user;
			this.pass = pass;
		}
		
		
		
		@Override
		protected void onPreExecute() {
			DoctorApplication dapp = DoctorApplication.getInstance();
			Client c = new ApacheClient(new EasyHttpClient());
			OAuthHandler handler = new OAuthHandler(c, Config.LOGIN_ENDPOINT, user, pass, Config.CLIENT_ID, "", null);
			
			alerts = new SecuredRestBuilder()
			.setClient(c)
			.setUsername(user).setPassword(pass)
			.setOAuthHandler(handler).build().create(AlertService.class);

			patients = new SecuredRestBuilder()
			.setClient(c)
			.setUsername(user).setPassword(pass)
			.setOAuthHandler(handler).build().create(PatientService.class);
			
			checkins = new SecuredRestBuilder()
			.setClient(c)
			.setUsername(user)
			.setPassword(pass)
			.setOAuthHandler(handler).build()
			.create(CheckInService.class);
			
			
			
		}

		@Override
		protected Boolean doInBackground(Void... params) {

			boolean isNew = false;
			DoctorApplication app = DoctorApplication.getInstance();
			Collection<Patient> ps = patients.getMyPatients();
			for (Patient patient : ps) {
				if(app.updatePatient(patient)) {
					isNew = true;
				}
				
				Collection<Alert> as = alerts.listByPatient(patient.getId());
				if(app.updateAlerts(patient, as)) {
					isNew = true;
				}
				
				Collection<CheckIn> cs = checkins.getCheckinsByPatientLastName(patient.getLastName());
				if(app.updateCheckins(patient, cs)) {
					isNew = true;
				}
				
			}
			
			return isNew;
		}
		

		@Override
		protected void onPostExecute(Boolean result) {
			onUpdateComplete(context, result, user, pass);
		}
		
		
	}
	
	
}
