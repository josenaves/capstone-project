package com.androidcapstone.androidsymptommanagement;

import com.androidcapstone.symptommanagement.model.CheckIn;

public class CheckInListItem {

	private CheckIn checkIn;

	public CheckInListItem(CheckIn checkIn) {
		super();
		this.checkIn = checkIn;
	}

	public CheckIn getCheckIn() {
		return checkIn;
	}

	public void setCheckIn(CheckIn checkIn) {
		this.checkIn = checkIn;
	}

	
	
}
