package com.androidcapstone.androidsymptommanagement;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.androidcapstone.androidsymptommanagement.doctor.DoctorApplication;
import com.androidcapstone.androidsymptommanagement.doctor.DoctorApplication.PatientData;
import com.androidcapstone.androidsymptommanagement.doctor.PatientSummaryListAdapter;
import com.androidcapstone.androidsymptommanagement.doctor.PatientSummaryListItem;
import com.androidcapstone.symptommanagement.model.Alert;
import com.androidcapstone.symptommanagement.model.AlertType;
import com.androidcapstone.symptommanagement.model.CheckIn;
import com.androidcapstone.symptommanagement.model.EatingInterferenceType;
import com.androidcapstone.symptommanagement.model.PainLevelType;
import com.androidcapstone.symptommanagement.model.Patient;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

public class PatientSummaryListActivity extends ListActivity {

	
	private PatientSummaryListAdapter mAdapter;
	
	private List<PatientSummaryListItem> mListItems;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_patient_summary_list);
		DoctorApplication dr = DoctorApplication.getInstance();
		mListItems = toListItems(toSummary(dr.getPatients()));
		mAdapter = new PatientSummaryListAdapter(this, mListItems, null);
		setListAdapter(mAdapter);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.patient_summary_list, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public static class PatientSummary {
		public String name;
		public Date dob;
		public PainLevelType painLevel;
		public EatingInterferenceType eatingProblems;
		public AlertType alertType;
		public Date lastCheckIn;
		public Patient p;
	}
	
	public List<PatientSummaryListItem> toListItems(List<PatientSummary> summaries) {
		List<PatientSummaryListItem> items = new ArrayList<PatientSummaryListItem>(summaries.size());
		for (PatientSummary summary : summaries) {
			items.add(new PatientSummaryListItem(summary));
		}
		return items;
	}
	
	
	public List<PatientSummary> toSummary(List<Patient> plist) {
		DoctorApplication dr = DoctorApplication.getInstance();
		List<PatientSummary> s = new ArrayList<PatientSummaryListActivity.PatientSummary>(plist.size());
		for (Patient p : plist) {
			CheckIn checkIn = dr.getLastCheckInFor(p);
			Alert alert = dr.getLastAlertFor(p);
			PatientSummary summary = new PatientSummary();
			summary.name = p.getFirstName() + " " + p.getLastName();
			summary.dob = p.getDateOfBirth();
			summary.lastCheckIn = checkIn.getCheckInTimestamp();
			summary.painLevel = checkIn.getPainLevel();
			summary.eatingProblems = checkIn.getEatingInterferenceLevel();
			summary.alertType = alert.getAlertType();
			summary.p = p;
			s.add(summary);
		}
		
		return s;
	}
	
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		PatientSummaryListItem item = mListItems.get(position);
		Intent intent = new Intent(this, PatientDetailsActivity.class);
		Bundle extras = new Bundle();
		extras.putSerializable(PatientDetailsActivity.PATIENT_ARG, item.getSummary().p);
		intent.putExtras(extras);
		startActivity(intent);
	}
}
