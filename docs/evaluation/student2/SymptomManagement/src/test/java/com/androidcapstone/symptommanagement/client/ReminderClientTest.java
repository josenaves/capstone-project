package com.androidcapstone.symptommanagement.client;

import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.junit.Assert.assertThat;

import java.util.Collection;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import retrofit.client.ApacheClient;

import com.androidcapstone.symptommanagement.api.API;
import com.androidcapstone.symptommanagement.api.ReminderService;
import com.androidcapstone.symptommanagement.model.Patient;
import com.androidcapstone.symptommanagement.model.Reminder;

public class ReminderClientTest {
	
	private static Logger LOG = LoggerFactory.getLogger(ReminderClientTest.class);
	
	private final String TEST_URL = "https://localhost:8443";

	private final String USERNAME1 = "doctor1";
	private final String USERNAME2 = "patient1";
	private final String PASSWORD = "smgmt";
	private final String CLIENT_ID = "mobile";
	
	ReminderService patient1 = new SecuredRestBuilder()
	.setClient(new ApacheClient(UnsafeHttpsClient.createUnsafeClient()))
	.setEndpoint(TEST_URL)
	.setLoginEndpoint(TEST_URL + API.TOKEN_PATH)
	// .setLogLevel(LogLevel.FULL)
	.setUsername(USERNAME2).setPassword(PASSWORD).setClientId(CLIENT_ID)
	.build().create(ReminderService.class);
	
	
	@Test
	public void testListReminders() {
		Collection<Reminder> reminders = patient1.getReminders();
		assertThat(reminders.size(), greaterThanOrEqualTo(1));
	}
	
	@Test
	public void testSave() {
		Reminder r = new Reminder();
		r.setPatient(new Patient(1));
		r.setTime("12:00");
		Reminder res = patient1.save(r);
		assertThat(res.getId(), greaterThanOrEqualTo(1L));
	}
	
	

}
