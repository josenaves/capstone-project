package com.androidcapstone.symptommanagement.repository;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.androidcapstone.symptommanagement.context.PersistenceContext;
import com.androidcapstone.symptommanagement.model.Doctor;
import com.androidcapstone.symptommanagement.model.Medicine;
import com.androidcapstone.symptommanagement.model.Patient;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {PersistenceContext.class/*, TestContext.class*/})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
    DirtiesContextTestExecutionListener.class,
    TransactionalTestExecutionListener.class,
    DbUnitTestExecutionListener.class})
//@DatabaseSetup("SymptomManagementData.xml")
public class ITPatientRepositoryTest {
	
	@Autowired
	private PatientRepository patientRepository;
	
	@Autowired 
	private MedicineRepository medicines;

	@Test
	public void testBootstrap() {
		assertNotNull(patientRepository);
	}
	
	@Test
	@Transactional
	public void testFetchById() {
		Patient p = patientRepository.findOne((long) 1);
		assertNotNull(p);
		assertThat(p.getFirstName(), is("John"));
		assertThat(p.getRoles().size(), is(1));
		assertThat(p.getMedicines().size(), is(3));
		assertThat(p.getReminders().size(), is(1));
		assertThat(p.getCheckIns().size(), is(1));
		assertThat(p.getCheckIns().get(0).getResponse().size(), is(1));
		assertNotNull(p.getDoctor());
		assertThat(p.getDoctor().getFirstName(), is("Jack"));
		Doctor d = p.getDoctor();
		assertThat(d.getRoles().size(), is(1));
		assertThat(d.getPatients().size(), is(1));
		assertThat(d.getPatients().get(0), is(p));
		
		
	}
	
	@Test
	@Transactional
	public void testUpdateMedcine() {
		Patient p = patientRepository.findOne((long) 1);
		int numMeds = p.getMedicines().size();
		Medicine m = medicines.findByName("Aspirin");
		
		p.getMedicines().add(m);
		
		patientRepository.save(p);
		
		Patient p1 = patientRepository.findOne((long) 1);
		
		assertThat(p1.getMedicines().size(), is(numMeds + 1));
	}
}
