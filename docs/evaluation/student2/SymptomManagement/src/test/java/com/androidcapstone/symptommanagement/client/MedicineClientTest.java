package com.androidcapstone.symptommanagement.client;

import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;

import java.util.Collection;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import retrofit.client.ApacheClient;

import com.androidcapstone.symptommanagement.api.API;
import com.androidcapstone.symptommanagement.api.MedicineService;
import com.androidcapstone.symptommanagement.model.Medicine;

public class MedicineClientTest {
	
	private static Logger LOG = LoggerFactory.getLogger(MedicineClientTest.class);
	
	private final String TEST_URL = "https://localhost:8443";

	private final String USERNAME1 = "doctor1";
	private final String USERNAME2 = "patient1";
	private final String PASSWORD = "smgmt";
	private final String CLIENT_ID = "mobile";
	
	MedicineService patient1 = new SecuredRestBuilder()
	.setClient(new ApacheClient(UnsafeHttpsClient.createUnsafeClient()))
	.setEndpoint(TEST_URL)
	.setLoginEndpoint(TEST_URL + API.TOKEN_PATH)
	// .setLogLevel(LogLevel.FULL)
	.setUsername(USERNAME2).setPassword(PASSWORD).setClientId(CLIENT_ID)
	.build().create(MedicineService.class);
	
	

	@Test
	public void testListMedicines() {
		Collection<Medicine> meds = patient1.getMedicines();
		assertThat(meds.size(), greaterThanOrEqualTo(3));
	}
	
	@Test
	public void testSearchMedicines() {
		Collection<Medicine> meds = patient1.searchMedicines("Aspirin");
		assertThat(meds.size(), is(1));
	}
	
	@Test
	public void testMatchMedicines() {
	
		Collection<Medicine> meds = patient1.matchMedicines("A");
		assertThat(meds.size(), greaterThanOrEqualTo(2));
		
	}
	
	@Test
	public void testSearchMedicinesFail() {
		Collection<Medicine> meds = patient1.searchMedicines("Chocolate");
		assertThat(meds.size(), is(1));
		assertNull(meds.iterator().next());
	}
}
