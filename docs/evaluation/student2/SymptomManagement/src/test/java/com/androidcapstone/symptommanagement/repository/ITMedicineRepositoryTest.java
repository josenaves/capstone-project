package com.androidcapstone.symptommanagement.repository;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import java.util.Collection;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.androidcapstone.symptommanagement.context.PersistenceContext;
import com.androidcapstone.symptommanagement.model.Medicine;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.google.common.collect.Lists;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {PersistenceContext.class/*, TestContext.class*/})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
    DirtiesContextTestExecutionListener.class,
    TransactionalTestExecutionListener.class,
    DbUnitTestExecutionListener.class})
public class ITMedicineRepositoryTest {

	@Autowired
	MedicineRepository medicines;
	
	@Test
	public void testFetchByName() {
		final String name = "Acetaminophen";
		
		Medicine m = medicines.findByName(name);
		
		Assert.assertNotNull(m);
	}
	
	@Test
	public void testFindByNameLike() {
		Collection<Medicine> m = medicines.findByNameLike("A%");
		
		assertThat(m.size(), is(2));
	}
	
	@Test
	public void testFindAll() {
		Collection<Medicine> m = Lists.newArrayList(medicines.findAll());
		assertThat(m.size(), is(4));
	}
}
