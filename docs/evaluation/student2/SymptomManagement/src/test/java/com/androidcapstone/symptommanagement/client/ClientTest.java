package com.androidcapstone.symptommanagement.client;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Collection;

import org.junit.Test;

import retrofit.ErrorHandler;
import retrofit.RetrofitError;
import retrofit.client.ApacheClient;
import retrofit.client.Response;

import com.androidcapstone.symptommanagement.api.API;
import com.androidcapstone.symptommanagement.api.PatientService;
import com.androidcapstone.symptommanagement.model.Patient;

/**
 * Test for REST
 * 
 * @author mitchell
 */
public class ClientTest {

	private class ErrorRecorder implements ErrorHandler {

		private RetrofitError error;

		@Override
		public Throwable handleError(RetrofitError cause) {
			error = cause;
			return error.getCause();
		}

		public RetrofitError getError() {
			return error;
		}
	}

	private final String TEST_URL = "https://localhost:8443";

	private final String USERNAME1 = "doctor1";
	private final String USERNAME2 = "patient1";
	private final String PASSWORD = "smgmt";
	private final String CLIENT_ID = "mobile";

	private PatientService doctor1 = new SecuredRestBuilder()
			.setClient(new ApacheClient(UnsafeHttpsClient.createUnsafeClient()))
			.setEndpoint(TEST_URL)
			.setLoginEndpoint(TEST_URL + API.TOKEN_PATH)
			// .setLogLevel(LogLevel.FULL)
			.setUsername(USERNAME1).setPassword(PASSWORD).setClientId(CLIENT_ID)
			.build().create(PatientService.class);

	private PatientService patient1 = new SecuredRestBuilder()
	.setClient(new ApacheClient(UnsafeHttpsClient.createUnsafeClient()))
	.setEndpoint(TEST_URL)
	.setLoginEndpoint(TEST_URL + API.TOKEN_PATH)
	// .setLogLevel(LogLevel.FULL)
	.setUsername(USERNAME2).setPassword(PASSWORD).setClientId(CLIENT_ID)
	.build().create(PatientService.class);



	@Test
	public void testGetPatients() throws Exception {
		Collection<Patient> received = doctor1.getMyPatients();
		assertTrue(received.size() == 1);
	}

	@Test
	public void testGetPatientsFail() throws Exception {
		
		try {
			Collection<Patient> received = patient1.getMyPatients();
			fail("This should have failed on PreAuthorize");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			assertTrue(true);
		}
	}
	
	@Test
	public void testGetPatient() {
		Patient p = patient1.getPatient();
		assertNotNull(p);
	}
	
	@Test
	public void testUpdateMedicine() {
		Response r = doctor1.updateMedicine(1L, 4L, "ADD");
		assertThat(r.getStatus(), is(200));
	}

//	@Test
//	public void testDenyVideoAddWithoutOAuth() throws Exception {
//		ErrorRecorder error = new ErrorRecorder();
//
//		// Create an insecure version of our Rest Adapter that doesn't know how
//		// to use OAuth.
//		VideoSvcApi insecurevideoService = new RestAdapter.Builder()
//				.setClient(
//						new ApacheClient(UnsafeHttpsClient.createUnsafeClient()))
//				.setEndpoint(TEST_URL).setLogLevel(LogLevel.FULL)
//				.setErrorHandler(error).build().create(VideoSvcApi.class);
//		try {
//			// This should fail because we haven't logged in!
//			insecurevideoService.addVideo(video);
//
//			fail("Yikes, the security setup is horribly broken and didn't require the user to authenticate!!");
//
//		} catch (Exception e) {
//			// Ok, our security may have worked, ensure that
//			// we got a 401
//			assertEquals(HttpStatus.SC_UNAUTHORIZED, error.getError()
//					.getResponse().getStatus());
//		}
//
//		// We should NOT get back the video that we added above!
//		Collection<Video> videos = readWriteVideoSvcUser1.getVideoList();
//		assertFalse(videos.contains(video));
//	}


}
