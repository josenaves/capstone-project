package com.androidcapstone.symptommanagement.client;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import retrofit.client.ApacheClient;

import com.androidcapstone.symptommanagement.api.API;
import com.androidcapstone.symptommanagement.api.CheckInService;
import com.androidcapstone.symptommanagement.api.PatientService;
import com.androidcapstone.symptommanagement.model.CheckIn;
import com.androidcapstone.symptommanagement.model.EatingInterferenceType;
import com.androidcapstone.symptommanagement.model.MedicationCheckIn;
import com.androidcapstone.symptommanagement.model.Medicine;
import com.androidcapstone.symptommanagement.model.MedicineType;
import com.androidcapstone.symptommanagement.model.PainLevelType;
import com.androidcapstone.symptommanagement.model.Patient;
import com.androidcapstone.symptommanagement.model.ResponseType;
import com.google.common.collect.Lists;

public class CheckInClientTest {

	private static Logger LOG = LoggerFactory.getLogger(CheckInClientTest.class);
	
	private final String TEST_URL = "https://localhost:8443";

	private final String USERNAME1 = "doctor1";
	private final String USERNAME2 = "patient1";
	private final String PASSWORD = "smgmt";
	private final String CLIENT_ID = "mobile";

	CheckInService doctor1 = new SecuredRestBuilder()
	.setClient(new ApacheClient(UnsafeHttpsClient.createUnsafeClient()))
	.setEndpoint(TEST_URL)
	.setLoginEndpoint(TEST_URL + API.TOKEN_PATH)
	// .setLogLevel(LogLevel.FULL)
	.setUsername(USERNAME1).setPassword(PASSWORD).setClientId(CLIENT_ID)
	.build().create(CheckInService.class);
	
	private CheckInService patient1 = new SecuredRestBuilder()
	.setClient(new ApacheClient(UnsafeHttpsClient.createUnsafeClient()))
	.setEndpoint(TEST_URL)
	.setLoginEndpoint(TEST_URL + API.TOKEN_PATH)
	// .setLogLevel(LogLevel.FULL)
	.setUsername(USERNAME2).setPassword(PASSWORD).setClientId(CLIENT_ID)
	.build().create(CheckInService.class);
	
	@Test
	public void testGetCheckinsByPatientLastName() {
		Collection<CheckIn> clist = doctor1.getCheckinsByPatientLastName("Doe");
		assertThat(clist.size(), is(1));
		for (CheckIn checkIn : clist) {
			LOG.info("CheckIn: {}", checkIn);
		}
	}
	
	@Test
	public void testGetCheckinsByPatientLastNameFail() {
		try {
			Collection<CheckIn> clist = doctor1.getCheckinsByPatientLastName("Diffenbach");
			assertThat(clist.size(), is(1));
			fail("This should not have authorized");
		} catch (Exception e) {
			assertTrue(true);
		}
	}
	
	@Test
	public void testGetCheckinsForDoctor() {
		Collection<CheckIn> clist = doctor1.getCheckinsForDoctor();
		assertThat(clist.size(), is(1));
	}
	
	@Test
	public void testSave() {
		CheckIn checkin = new CheckIn();
		checkin.setCheckInTimestamp(new Date());
		checkin.setEatingInterferenceLevel(EatingInterferenceType.No);
		checkin.setPainLevel(PainLevelType.WellControlled);
		Patient p = new Patient();
		p.setId(1);
		checkin.setPatient(p);
		MedicationCheckIn mcheck = new MedicationCheckIn();
		mcheck.setCheckIn(checkin);
		mcheck.setMedicineTaken(new Date());
		mcheck.setResponse(ResponseType.Yes);
		Medicine m = new Medicine();
		m.setId(1);
		m.setName("Acetaminophen");
		m.setType(MedicineType.Pain);
		mcheck.setMedicine(m);
		List<MedicationCheckIn> mlist = Lists.newArrayList(mcheck);
		checkin.setResponse(mlist);
		
		CheckIn c2 = patient1.save(checkin);
		
		MedicationCheckIn m2 = c2.getResponse().get(0);
		assertThat(m2.getCheckIn().getId(), is(c2.getId()));
		
	}
	
}
