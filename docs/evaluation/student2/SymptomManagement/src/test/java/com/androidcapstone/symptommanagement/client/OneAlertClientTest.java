package com.androidcapstone.symptommanagement.client;

import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Collection;
import java.util.Date;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import retrofit.client.ApacheClient;
import retrofit.client.Response;

import com.androidcapstone.symptommanagement.api.API;
import com.androidcapstone.symptommanagement.api.AlertService;
import com.androidcapstone.symptommanagement.model.Alert;
import com.androidcapstone.symptommanagement.model.AlertType;
import com.androidcapstone.symptommanagement.model.Doctor;
import com.androidcapstone.symptommanagement.model.Patient;

public class OneAlertClientTest {
	
	private static Logger LOG = LoggerFactory.getLogger(OneAlertClientTest.class);
	
	private final String TEST_URL = "https://localhost:8443";

	private final String USERNAME1 = "doctor1";
	private final String USERNAME2 = "patient1";
	private final String PASSWORD = "smgmt";
	private final String CLIENT_ID = "mobile";
	
	AlertService doctor1 = new SecuredRestBuilder()
	.setClient(new ApacheClient(UnsafeHttpsClient.createUnsafeClient()))
	.setEndpoint(TEST_URL)
	.setLoginEndpoint(TEST_URL + API.TOKEN_PATH)
	// .setLogLevel(LogLevel.FULL)
	.setUsername(USERNAME1).setPassword(PASSWORD).setClientId(CLIENT_ID)
	.build().create(AlertService.class);
	
	AlertService doctor2 = new SecuredRestBuilder()
	.setClient(new ApacheClient(UnsafeHttpsClient.createUnsafeClient()))
	.setEndpoint(TEST_URL)
	.setLoginEndpoint(TEST_URL + API.TOKEN_PATH)
	// .setLogLevel(LogLevel.FULL)
	.setUsername("doctor2").setPassword(PASSWORD).setClientId(CLIENT_ID)
	.build().create(AlertService.class);
	
	private AlertService patient1 = new SecuredRestBuilder()
	.setClient(new ApacheClient(UnsafeHttpsClient.createUnsafeClient()))
	.setEndpoint(TEST_URL)
	.setLoginEndpoint(TEST_URL + API.TOKEN_PATH)
	// .setLogLevel(LogLevel.FULL)
	.setUsername(USERNAME2).setPassword(PASSWORD).setClientId(CLIENT_ID)
	.build().create(AlertService.class);
	
	
	@Test
	public void testSave() {
		
		Alert a = new Alert();
		
		a.setAckCount(0);
		a.setAcknowledged(null);
		a.setAlertCount(1);
		a.setAlertType(AlertType.TwelveHoursNoEat);
		a.setCreated(new Date());
		a.setResolved(null);
		a.setUpdated(null);
		Patient p = new Patient(1);
		a.setPatient(p);
		Doctor d = new Doctor(2);
		a.setDoctor(d);
		
		Alert resp = patient1.save(a);
		
		assertTrue(resp.getId() > 0);
	}
	
	
}
