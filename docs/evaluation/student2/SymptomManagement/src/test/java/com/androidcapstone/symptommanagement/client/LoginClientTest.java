package com.androidcapstone.symptommanagement.client;

import java.util.Collection;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import retrofit.client.ApacheClient;

import com.androidcapstone.symptommanagement.api.API;
import com.androidcapstone.symptommanagement.api.LoginService;
import com.androidcapstone.symptommanagement.model.Role;

public class LoginClientTest {

	
private static Logger LOG = LoggerFactory.getLogger(LoginClientTest.class);
	
	private final String TEST_URL = "https://10.230.230.182:8443";

	private final String USERNAME1 = "doctor1";
	private final String USERNAME2 = "patient1";
	private final String PASSWORD = "smgmt";
	private final String CLIENT_ID = "mobile";
	
	LoginService doctor1 = new SecuredRestBuilder()
	.setClient(new ApacheClient(UnsafeHttpsClient.createUnsafeClient()))
	.setEndpoint(TEST_URL)
	.setLoginEndpoint(TEST_URL + API.TOKEN_PATH)
	// .setLogLevel(LogLevel.FULL)
	.setUsername(USERNAME1).setPassword(PASSWORD).setClientId(CLIENT_ID)
	.build().create(LoginService.class);
	
	
	@Test
	public void testLoginDoctor() {
		Collection<Role> r = doctor1.getLogin();
		LOG.info("Roles: {}", r);
		//assertTrue(u instanceof Doctor);
	}
}
