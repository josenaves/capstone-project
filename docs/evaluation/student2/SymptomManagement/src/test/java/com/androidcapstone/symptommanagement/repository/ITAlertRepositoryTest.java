package com.androidcapstone.symptommanagement.repository;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import com.androidcapstone.symptommanagement.context.PersistenceContext;
import com.androidcapstone.symptommanagement.model.Alert;
import com.androidcapstone.symptommanagement.model.Doctor;
import com.androidcapstone.symptommanagement.model.Patient;
import com.github.springtestdbunit.DbUnitTestExecutionListener;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {PersistenceContext.class/*, TestContext.class*/})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
    DirtiesContextTestExecutionListener.class,
    TransactionalTestExecutionListener.class,
    DbUnitTestExecutionListener.class})
public class ITAlertRepositoryTest {

	@Autowired
	private AlertRepository alerts;
	
	@Test
	public void testListByDoctor () {
		Doctor doctor = new Doctor (2);
		Collection<Alert> alertList = alerts.findByDoctor(doctor);
		assertThat(alertList.size(), is(1));
	}
	
	@Test
	public void testListByPatient() {
		Patient p = new Patient();
		p.setId(1);
		Collection<Alert> alertList = alerts.findByPatient(p);
		assertThat(alertList.size(), is(1));
	}
}
