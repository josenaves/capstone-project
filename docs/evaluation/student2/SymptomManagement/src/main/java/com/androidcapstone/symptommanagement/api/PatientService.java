package com.androidcapstone.symptommanagement.api;

import java.util.Collection;

import retrofit.client.Response;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;

import com.androidcapstone.symptommanagement.model.Patient;

public interface PatientService {
	

	@GET(API.PATIENT_GET)
	public Patient getPatient();
	
	@GET(API.PATIENT_LIST_FOR_DOCTOR)
	public Collection<Patient> getMyPatients();
	
	@FormUrlEncoded
	@POST(API.PATIENT_UPDATE_MEDICINE)
	public Response updateMedicine(@Path(API.PATIENT_ID_VAR)Long pId, @Field(API.MEDICINE_ID_VAR) Long mId, @Field(API.ACTION_VAR) String action);

}
