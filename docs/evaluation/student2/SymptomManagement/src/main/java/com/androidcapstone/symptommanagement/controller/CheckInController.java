package com.androidcapstone.symptommanagement.controller;

import java.security.Principal;
import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PostFilter;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.web.bind.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.androidcapstone.symptommanagement.api.API;
import com.androidcapstone.symptommanagement.auth.AuthTranslator;
import com.androidcapstone.symptommanagement.auth.AuthUser;
import com.androidcapstone.symptommanagement.model.CheckIn;
import com.androidcapstone.symptommanagement.model.Doctor;
import com.androidcapstone.symptommanagement.repository.CheckInRepository;

@Controller
public class CheckInController {

	private static final Logger LOG = LoggerFactory.getLogger(CheckInController.class);
	
	@Autowired
	private CheckInRepository checkIns;
	
	@Autowired
	private AuthTranslator authTranslator;
	
	@RequestMapping(value = API.CHECKIN_SAVE, method = RequestMethod.POST)
	@PreAuthorize("principal.userDbId == #checkIn.patient.id")
	public @ResponseBody CheckIn save(@RequestBody CheckIn checkIn) {
		LOG.info("CheckInController save recv: {}", checkIn);
		checkIns.saveAndFlush(checkIn);
		return checkIn;
	}
	
	@RequestMapping(value = API.CHECKINS_GET_FOR_DOCTOR, method = RequestMethod.GET)
	@PreAuthorize("hasRole('Doctor')")
	public @ResponseBody Collection<CheckIn> listCheckinsForDoctor(Principal p) {
		AuthUser user = authTranslator.toAuthUser(p);
		Doctor d = new Doctor(user.getUserDbId());
		return checkIns.findByDoctor(d);
	}
	
	@RequestMapping(value = API.CHECKIN_GET_BY_PATIENT, method = RequestMethod.GET)
	@PreAuthorize("@decider.isDoctorForPatientName(principal,#name)")
	@PostFilter("filterObject.patient.doctor.id == principal.userDbId")
	public @ResponseBody Collection<CheckIn> listCheckinsByPatientNameForDoctor(@RequestParam(API.NAME_VAR) String name) {
		
		return checkIns.findByPatientLastName(name);
	}
	
	
}
