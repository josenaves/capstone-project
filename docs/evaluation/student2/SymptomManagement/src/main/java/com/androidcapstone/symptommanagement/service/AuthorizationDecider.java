package com.androidcapstone.symptommanagement.service;

import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.androidcapstone.symptommanagement.auth.AuthUser;
import com.androidcapstone.symptommanagement.model.Alert;
import com.androidcapstone.symptommanagement.model.Doctor;
import com.androidcapstone.symptommanagement.model.Patient;
import com.androidcapstone.symptommanagement.repository.AlertRepository;
import com.androidcapstone.symptommanagement.repository.PatientRepository;

@Service(value="decider")
public class AuthorizationDecider {

	private static Logger LOG = LoggerFactory.getLogger(AuthorizationDecider.class);
	
	@Autowired
	private PatientRepository patients;
	
	@Autowired
	private AlertRepository alerts;

	
	public boolean isDoctorForPatient(AuthUser user, Long pId) {
		Patient p = patients.findOne(pId);
		if(null == p) return false;
		Long docId = p.getDoctor().getId();
		return user.getUserDbId() == docId;
	}

	
	public boolean isDoctorForAlert(AuthUser user, Long aId) {
		Alert a = alerts.findOne(aId);
		if(a == null)  return false;
		
		Doctor d  = a.getDoctor();
		return d.getId() == user.getUserDbId();
	}
	
	public boolean isDoctorForPatientName(AuthUser user, String name) {
		Doctor d = new Doctor(user.getUserDbId());
		Collection<Patient> plist = patients.findByDoctorAndLastName(d, name);
		LOG.info("isDoctorForPatient called doctor {} patient {} size {}", user.getUserDbId(), name, plist.size());
		return plist.size() > 0;
	}
}
