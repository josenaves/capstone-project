package com.androidcapstone.symptommanagement.api;

import java.util.Collection;

import retrofit.client.Response;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;

import com.androidcapstone.symptommanagement.model.Reminder;

public interface ReminderService {

	@GET(API.REMINDER_GET_FOR_PATIENT)
	public Collection<Reminder> getReminders();
	
	@POST(API.REMINDER_SAVE)
	public Reminder save(@Body Reminder reminder);
	
	@POST(API.REMINDER_DELETE)
	public Response delete(@Body Reminder reminder);
}
