package com.androidcapstone.symptommanagement.auth;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.util.StringUtils;

import com.androidcapstone.symptommanagement.model.Role;
import com.androidcapstone.symptommanagement.model.User;
import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.ImmutableSet.Builder;

public class AuthUser implements UserDetails {

	 private Collection<GrantedAuthority> authorities_;
     private  String password_;
     private  String username_;
     private  boolean enabled_;
     private  String fullName;
     private  long userDbId;
     private  boolean locked;
     private Locale locale;
     
     
     
     
     public AuthUser() {
		super();
		// TODO Auto-generated constructor stub
	}



	public static UserDetails create(User user) {
    	 return new AuthUser(user);
     }
     

	
	public AuthUser(User user) {
		// TODO Auto-generated constructor stub
		this.password_ = user.getPasswordHash();
		this.userDbId = user.getId();
		this.username_ = user.getUserName();
		this.enabled_ = user.isEnabled();
		this.fullName = Joiner.on(" ").skipNulls().join(user.getFirstName(), user.getLastName());
		if(StringUtils.hasText(user.getLocale())) {
			this.locale = new Locale(user.getLocale());
		} else {
			this.locale = Locale.ENGLISH;
		}
		
		if(null != user.getLockedOut()) {
			Date lockedOut = user.getLockedOut();
			if(lockedOut.after(new Date())) {
				locked = true;
			} else {
				locked = false;
			}
			
		} else {
			locked = false;
		}
		
		Builder<GrantedAuthority> builder = ImmutableSet.builder();
		
		for (Role role : user.getRoles()) {
			builder.add(new SimpleGrantedAuthority(role.getName()));
		}
		authorities_ = builder.build();
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		// TODO Auto-generated method stub
		return authorities_;
	}

	@Override
	public String getPassword() {
		// TODO Auto-generated method stub
		return password_;
	}

	@Override
	public String getUsername() {
		// TODO Auto-generated method stub
		return username_;
	}

	public long getUserDbId() {
		return userDbId;
	}


	@Override
	public boolean isAccountNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		// TODO Auto-generated method stub
		return !locked;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return enabled_;
	}

}
