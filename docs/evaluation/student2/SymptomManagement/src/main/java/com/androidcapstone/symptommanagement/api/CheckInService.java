package com.androidcapstone.symptommanagement.api;

import java.util.Collection;

import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;
import retrofit.http.Query;

import com.androidcapstone.symptommanagement.model.CheckIn;

public interface CheckInService {

	@GET(API.CHECKIN_GET_BY_PATIENT)
	public Collection<CheckIn> getCheckinsByPatientLastName(@Query(API.NAME_VAR) String lastName);
	
	@GET(API.CHECKINS_GET_FOR_DOCTOR)
	public Collection<CheckIn> getCheckinsForDoctor();
	
	@POST(API.CHECKIN_SAVE)
	public CheckIn save(@Body CheckIn checkIn);
	
	// CheckInSummaryReport
	// Missed CheckIns
	
}
