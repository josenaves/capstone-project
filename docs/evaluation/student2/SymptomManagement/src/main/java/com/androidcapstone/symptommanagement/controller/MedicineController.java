package com.androidcapstone.symptommanagement.controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.androidcapstone.symptommanagement.api.API;
import com.androidcapstone.symptommanagement.model.Medicine;
import com.androidcapstone.symptommanagement.repository.MedicineRepository;
import com.google.common.collect.Lists;

@Controller
public class MedicineController {

	@Autowired 
	MedicineRepository medicines;
	
	@RequestMapping(value=API.MEDICINE_PATH, method = RequestMethod.GET)
	public @ResponseBody Collection<Medicine> getMedicines() {
		return Lists.newArrayList(medicines.findAll());
	}
	
	@RequestMapping(value=API.MEDICINE_SEARCH, method = RequestMethod.GET)
	public @ResponseBody Collection<Medicine> searchMedicines(@RequestParam(value=API.NAME_VAR) String name) {
		return Lists.newArrayList(medicines.findByName(name));
	}
	
	@RequestMapping(value=API.MEDICINE_MATCH, method = RequestMethod.GET)
	public @ResponseBody Collection<Medicine> matchMedicines(String name) {
		return medicines.findByNameLike(name + "%");
	}
	
}
