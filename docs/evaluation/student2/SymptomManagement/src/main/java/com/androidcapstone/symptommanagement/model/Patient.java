package com.androidcapstone.symptommanagement.model;

import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.jsonFormatVisitors.JsonFormatTypes;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Patient extends User{
	

	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ss")
	private Date dateOfBirth;
	
	@OneToMany(mappedBy = "patient", fetch = FetchType.LAZY)
	private List<Reminder> reminders;
	
	@OneToMany(mappedBy = "patient", fetch = FetchType.LAZY)
	private List<CheckIn> checkIns;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="doctor")
	private Doctor doctor;
	
	@ManyToMany(fetch = FetchType.EAGER)
	private List<Medicine> medicines;


	public Patient() {
		super();
	}
	
	public Patient(long id) {
		// TODO Auto-generated constructor stub
		super.setId(id);
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	@JsonIgnore
	public List<Reminder> getReminders() {
		return reminders;
	}

	public void setReminders(List<Reminder> reminders) {
		this.reminders = reminders;
	}

	@JsonIgnore
	public List<CheckIn> getCheckIns() {
		return checkIns;
	}

	public void setCheckIns(List<CheckIn> checkIns) {
		this.checkIns = checkIns;
	}

	public Doctor getDoctor() {
		return doctor;
	}

	public void setDoctor(Doctor doctor) {
		this.doctor = doctor;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Patient [dateOfBirth=");
		builder.append(dateOfBirth);
		builder.append(", reminders=");
		builder.append(reminders);
		builder.append(", checkIns=");
		builder.append(checkIns);
		builder.append(", doctor=");
		builder.append(doctor);
		builder.append(", medicines=");
		builder.append(medicines);
		builder.append("]");
		return builder.toString();
	}

	public List<Medicine> getMedicines() {
		return medicines;
	}

	public void setMedicines(List<Medicine> medicines) {
		this.medicines = medicines;
	}
	
	
	
}
