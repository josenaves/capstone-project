package com.androidcapstone.symptommanagement.model;

import java.io.Serializable;

public enum MedicineType implements Serializable {
	Pain, Other;
}
