package com.androidcapstone.symptommanagement.auth;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.event.AuthenticationFailureBadCredentialsEvent;

import com.androidcapstone.symptommanagement.model.User;
import com.androidcapstone.symptommanagement.repository.UserRepository;

public class UserAuthenticationErrorHandler implements
		ApplicationListener<AuthenticationFailureBadCredentialsEvent> {
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	@Override
	public void onApplicationEvent(
			AuthenticationFailureBadCredentialsEvent event) {

		Object principal = event.getAuthentication().getPrincipal();
		
		if(principal instanceof String) {
			String username = (String) principal;
			User u = userRepository.findByUserName(username);
			
			if(u.getFailedLogins() == null) {
				u.setFailedLogins(1);
			} else {
				u.setFailedLogins(u.getFailedLogins() + 1);
			}
			
			if(u.getFailedLogins() > 10) {
				u.setLockedOut(DateTime.now().plusMinutes(10).toDate());
			}
			
		}

	}

}
