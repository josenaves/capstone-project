package com.androidcapstone.symptommanagement.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.androidcapstone.symptommanagement.model.User;

@org.springframework.stereotype.Repository
public interface UserRepository extends JpaRepository<User, Long> {
	
	public User findByUserName(String userName);

	
}
