package com.androidcapstone.symptommanagement.controller;

import java.security.Principal;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.androidcapstone.symptommanagement.api.API;
import com.androidcapstone.symptommanagement.auth.AuthTranslator;
import com.androidcapstone.symptommanagement.auth.AuthUser;
import com.androidcapstone.symptommanagement.model.Patient;
import com.androidcapstone.symptommanagement.model.Reminder;
import com.androidcapstone.symptommanagement.repository.ReminderRepository;

@Controller
public class ReminderController {

	@Autowired
	private ReminderRepository reminders;
	
	@Autowired
	private AuthTranslator authTranslator;
	
	@RequestMapping(value=API.REMINDER_SAVE, method = RequestMethod.POST)
	@PreAuthorize("principal.userDbId == #reminder.patient.id")
	public @ResponseBody Reminder save(@RequestBody Reminder reminder) {
		reminders.save(reminder);
		reminders.flush();
		return reminder;
	}
	
	@RequestMapping(value=API.REMINDER_GET_FOR_PATIENT, method = RequestMethod.GET)
	public @ResponseBody Collection<Reminder> getRemindersForPatient(Principal p) {
		AuthUser user = authTranslator.toAuthUser(p);
		Patient pat = new Patient(user.getUserDbId());
		return reminders.findByPatient(pat);
	}
	
	@RequestMapping(value=API.REMINDER_DELETE, method = RequestMethod.POST)
	@PreAuthorize("principal.userDbId == #reminder.patient.id")
	public ResponseEntity delete(@RequestBody Reminder reminder) {
		reminders.delete(reminder);
		return new ResponseEntity(HttpStatus.OK);
	}
	
}
