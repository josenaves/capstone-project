package com.androidcapstone.symptommanagement.auth;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.event.AuthenticationSuccessEvent;
import org.springframework.stereotype.Component;

import com.androidcapstone.symptommanagement.model.User;
import com.androidcapstone.symptommanagement.repository.UserRepository;

@Component
public class UserAuthenticationSuccessfulHandler implements
		ApplicationListener<AuthenticationSuccessEvent> {

	@Autowired
	private UserRepository userRepository;
	
	@Override
	@Transactional
	public void onApplicationEvent(AuthenticationSuccessEvent event) {
		
		Object principal = event.getAuthentication().getPrincipal();
		
		if (principal instanceof AuthUser) {
			AuthUser au = (AuthUser) principal;
				User u = userRepository.findByUserName(au.getUsername());
				u.setFailedLogins(null);
				u.setLockedOut(null);
				userRepository.save(u);			
		}
		
	}

}
