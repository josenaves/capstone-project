package com.androidcapstone.symptommanagement.controller;

import java.security.Principal;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.androidcapstone.symptommanagement.api.API;
import com.androidcapstone.symptommanagement.auth.AuthTranslator;
import com.androidcapstone.symptommanagement.auth.AuthUser;
import com.androidcapstone.symptommanagement.model.Role;
import com.androidcapstone.symptommanagement.model.User;
import com.androidcapstone.symptommanagement.repository.UserRepository;

@Controller
public class LoginController {

	
	@Autowired
	private UserRepository users;
	
	@Autowired
	private AuthTranslator authTranslator;
	
	@RequestMapping(value = API.LOGIN_PATH, method = RequestMethod.GET)
	public @ResponseBody Collection<Role> getLogin(Principal p) {
		AuthUser user = authTranslator.toAuthUser(p);
		User u = users.findOne(user.getUserDbId());
		return u.getRoles();
	}
}
