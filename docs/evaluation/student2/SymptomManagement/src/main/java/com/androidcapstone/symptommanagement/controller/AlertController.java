package com.androidcapstone.symptommanagement.controller;

import java.security.Principal;
import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.androidcapstone.symptommanagement.api.API;
import com.androidcapstone.symptommanagement.auth.AuthTranslator;
import com.androidcapstone.symptommanagement.auth.AuthUser;
import com.androidcapstone.symptommanagement.model.Alert;
import com.androidcapstone.symptommanagement.model.Doctor;
import com.androidcapstone.symptommanagement.model.Patient;
import com.androidcapstone.symptommanagement.repository.AlertRepository;
import com.androidcapstone.symptommanagement.repository.DoctorRepository;

@Controller
public class AlertController {

	@Autowired
	private AlertRepository alerts;
	
	@Autowired
	private AuthTranslator authTranslator;
	
	
	@RequestMapping(value = API.ALERT_SAVE, method = RequestMethod.POST)
	@PreAuthorize("principal.userDbId == #alert.patient.id")
	public @ResponseBody Alert save(@RequestBody Alert alert) {
		alerts.saveAndFlush(alert);
		return alert;
		
	}
	
	@RequestMapping(value = API.ALERT_LIST_FOR_DOCTOR, method = RequestMethod.GET)
	@PreAuthorize("hasRole('Doctor')")
	public @ResponseBody Collection<Alert> listByDoctor(Principal p) {
		AuthUser user = authTranslator.toAuthUser(p);
		Doctor doctor = new Doctor(user.getUserDbId());
		return alerts.findByDoctor(doctor);
	}
	
	@RequestMapping(value = API.ALERT_LIST_BY_PATIENT_FOR_DOCTOR, method = RequestMethod.GET)
	@PreAuthorize("@decider.isDoctorForPatient(principal, #pId)")
	public @ResponseBody Collection<Alert> listByPatient(@PathVariable(API.PATIENT_ID_VAR) Long pId) {
		Patient p = new Patient();
		p.setId(pId);
		return alerts.findByPatient(p);
	}
	
	@RequestMapping(value = API.ALERT_ACKNOWLEDGE, method = RequestMethod.POST)
	@PreAuthorize("@decider.isDoctorForAlert(principal, #aId)")
	public ResponseEntity acknowledge(@PathVariable(API.ALERT_ID_VAR) Long aId) {
		Alert a = alerts.findOne(aId);
		
		if (null != a) {
			a.setAcknowledged(new Date ());
			a.setAckCount(a.getAckCount() + 1);
			alerts.saveAndFlush(a);
		}
		return new ResponseEntity(HttpStatus.OK);
	}
	
}
