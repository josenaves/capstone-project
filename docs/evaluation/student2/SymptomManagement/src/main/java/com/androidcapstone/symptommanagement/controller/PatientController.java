package com.androidcapstone.symptommanagement.controller;

import java.security.Principal;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.androidcapstone.symptommanagement.api.API;
import com.androidcapstone.symptommanagement.api.PatientService;
import com.androidcapstone.symptommanagement.auth.AuthTranslator;
import com.androidcapstone.symptommanagement.auth.AuthUser;
import com.androidcapstone.symptommanagement.model.Doctor;
import com.androidcapstone.symptommanagement.model.Medicine;
import com.androidcapstone.symptommanagement.model.Patient;
import com.androidcapstone.symptommanagement.repository.MedicineRepository;
import com.androidcapstone.symptommanagement.repository.PatientRepository;
import com.androidcapstone.symptommanagement.repository.UserRepository;
import com.google.common.collect.Lists;

@Controller
public class PatientController {

	@Autowired
	private PatientRepository patients;
	
	@Autowired
	private MedicineRepository medicines;
	
	@Autowired
	private AuthTranslator authTranslator;
	
	
	@RequestMapping(value=API.PATIENT_LIST_FOR_DOCTOR, method=RequestMethod.GET)
	@PreAuthorize("hasRole('Doctor')")
	public @ResponseBody Collection<Patient> getPatientsByDoctor(Principal p) {
		AuthUser user = authTranslator.toAuthUser(p);
		return patients.findByDoctor(new Doctor(user.getUserDbId()));
	}
	
	@RequestMapping(value = API.PATIENT_GET, method = RequestMethod.GET)
	@PreAuthorize("hasRole('Patient')")
	public @ResponseBody Patient getPatient(Principal p) {
		AuthUser user = authTranslator.toAuthUser(p);
		return patients.findOne(user.getUserDbId());
	}
	
	@RequestMapping(value = API.PATIENT_UPDATE_MEDICINE, method = RequestMethod.POST)
	@PreAuthorize("@decider.isDoctorForPatient(principal, #pId)")
	public ResponseEntity updateMedicine(@PathVariable(API.PATIENT_ID_VAR) Long pId, @RequestParam(API.MEDICINE_ID_VAR) Long mId, @RequestParam(API.ACTION_VAR) String action) {
		Patient p = patients.findOne(pId);
		Collection<Medicine> meds = p.getMedicines();
		Medicine reqMed = medicines.findOne(mId);
		
		if(null == p || null == reqMed) {
			throw new IllegalArgumentException("Must supply valid patientId and medicineId");
		}
		
		if(meds.contains(reqMed)) {
			if(action.equalsIgnoreCase("DELETE")) {
				meds.remove(reqMed);
				patients.saveAndFlush(p);
			} else {
				//throw new IllegalArgumentException("Patient is already prescribed that medicine");
			}
		} else {
			if(action.equalsIgnoreCase("ADD")) {
				meds.add(reqMed);
				patients.saveAndFlush(p);
			} else {
				//throw new IllegalArgumentException("Patient has not been prescribed that medicine");
			}
		}
		
		return new ResponseEntity(HttpStatus.OK);
	}
}
