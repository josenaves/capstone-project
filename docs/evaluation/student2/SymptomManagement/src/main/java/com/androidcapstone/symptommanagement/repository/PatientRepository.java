package com.androidcapstone.symptommanagement.repository;

import java.util.Collection;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.androidcapstone.symptommanagement.model.Doctor;
import com.androidcapstone.symptommanagement.model.Patient;

@Repository
public interface PatientRepository extends JpaRepository<Patient, Long> {

	public Collection<Patient> findByDoctor(Doctor doctor);
	
	public Collection<Patient> findByDoctorAndLastName(Doctor doctor, String lastName);
}
