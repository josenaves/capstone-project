package com.androidcapstone.symptommanagement.api;

import java.util.Collection;

import retrofit.http.GET;

import com.androidcapstone.symptommanagement.model.Role;

public interface LoginService {

	@GET(API.LOGIN_PATH)
	public Collection<Role> getLogin();
}
