package com.androidcapstone.symptommanagement.api;

import java.util.Collection;

import com.androidcapstone.symptommanagement.model.Medicine;

import retrofit.http.GET;
import retrofit.http.Query;

public interface MedicineService {
	
	@GET(API.MEDICINE_PATH)
	public Collection<Medicine> getMedicines();
	
	@GET(API.MEDICINE_SEARCH)
	public Collection<Medicine> searchMedicines(@Query("name")String name);
	
	@GET(API.MEDICINE_MATCH)
	public Collection<Medicine> matchMedicines(@Query("name")String name);
	
	

}
