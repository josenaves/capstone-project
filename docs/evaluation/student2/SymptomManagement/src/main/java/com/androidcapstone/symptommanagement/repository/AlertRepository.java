package com.androidcapstone.symptommanagement.repository;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;

import com.androidcapstone.symptommanagement.model.Alert;
import com.androidcapstone.symptommanagement.model.Doctor;
import com.androidcapstone.symptommanagement.model.Patient;

public interface AlertRepository extends JpaRepository<Alert, Long> {

	//save
	
	// findByDoctor
	public Collection<Alert> findByDoctor(Doctor doctor);
	
	// findByPatient
	public Collection<Alert> findByPatient(Patient patient);
}
