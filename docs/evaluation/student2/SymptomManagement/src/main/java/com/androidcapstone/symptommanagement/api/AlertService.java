package com.androidcapstone.symptommanagement.api;

import java.util.Collection;

import retrofit.client.Response;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;

import com.androidcapstone.symptommanagement.model.Alert;

public interface AlertService {

	@POST(API.ALERT_SAVE)
	public Alert save(@Body Alert alert);
	
	@GET(API.ALERT_LIST_FOR_DOCTOR)
	public Collection<Alert> list();

	@GET(API.ALERT_LIST_BY_PATIENT_FOR_DOCTOR)
	public Collection<Alert> listByPatient(@Path(API.PATIENT_ID_VAR) Long pId);
	
	@POST(API.ALERT_ACKNOWLEDGE)
	public Response acknowledge(@Path(API.ALERT_ID_VAR) Long aId);
	
	
	
}
