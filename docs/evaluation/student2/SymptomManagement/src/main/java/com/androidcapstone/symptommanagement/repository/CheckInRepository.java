package com.androidcapstone.symptommanagement.repository;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.androidcapstone.symptommanagement.model.CheckIn;
import com.androidcapstone.symptommanagement.model.Doctor;
import com.androidcapstone.symptommanagement.model.Patient;

@Repository
public interface CheckInRepository extends JpaRepository<CheckIn, Long>{
	
	@Query("FROM CheckIn AS ch WHERE ch.patient.lastName = ?1")
	public Collection<CheckIn> findByPatientLastName(String lastName);
	
	@Query("FROM CheckIn as ch WHERE ch.patient.doctor = ?1")
	public Collection<CheckIn> findByDoctor(Doctor doctor);
}
