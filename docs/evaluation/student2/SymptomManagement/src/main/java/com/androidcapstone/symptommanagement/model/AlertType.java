package com.androidcapstone.symptommanagement.model;

import java.io.Serializable;

public enum AlertType implements Serializable {
	TwelveHoursSeverePain,SixteenHoursModerateToSeverePain,TwelveHoursNoEat
}
