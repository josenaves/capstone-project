package com.androidcapstone.symptommanagement.model;

import java.io.Serializable;

public enum PainLevelType implements Serializable {
	Severe,
	Moderate,
	WellControlled
}
