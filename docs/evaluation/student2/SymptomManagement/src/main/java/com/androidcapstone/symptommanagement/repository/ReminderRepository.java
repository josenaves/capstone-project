package com.androidcapstone.symptommanagement.repository;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.androidcapstone.symptommanagement.model.Patient;
import com.androidcapstone.symptommanagement.model.Reminder;

@Repository
public interface ReminderRepository extends JpaRepository<Reminder, Long> {

	Collection<Reminder> findByPatient(Patient p);
	
}
