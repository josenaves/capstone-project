package com.androidcapstone.symptommanagement.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Doctor extends User {

	public Doctor() {
		super();
	}
	
	public Doctor (long id) {
		this.setId(id);
	}
	
	@OneToMany(mappedBy = "doctor", fetch = FetchType.EAGER)
	private List<Patient> patients;



	@JsonIgnore
	public List<Patient> getPatients() {
		return patients;
	}

	public void setPatients(List<Patient> patients) {
		this.patients = patients;
	}
	
	
	
}
