package com.androidcapstone.symptommanagement.model;

import java.io.Serializable;

public enum ResponseType implements Serializable {
	Yes, No;
}
