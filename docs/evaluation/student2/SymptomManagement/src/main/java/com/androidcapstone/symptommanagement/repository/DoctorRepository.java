package com.androidcapstone.symptommanagement.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.androidcapstone.symptommanagement.model.Doctor;

@Repository
public interface DoctorRepository extends
		PagingAndSortingRepository<Doctor, Long> {
	
	

}
