package com.androidcapstone.symptommanagement.api;

public class API {

	public static final String DOCTOR_ACCESS_PATH = "/doctor";
	
	public static final String PATIENT_ACCESS_PATH = "/patient";
	
	public static final String TOKEN_PATH = "/oauth/token";
	
	public static final String CHECKIN_PATH = "/checkin";
	
	public static final String LOGIN_PATH = "/login";
	
	// Vars and Fields
	public static final String PATIENT_ID_VAR = "pId";
	
	public static final String MEDICINE_ID_VAR = "mId";
	
	public static final String ACTION_VAR = "action";
	
	public static final String NAME_VAR = "name";
	
	
	public static final String PATIENT_ID_PATH_VAR = "{"+ PATIENT_ID_VAR +"}";
	
	private static final String SAVE_PATH = "/save";
	
	private static final String DELETE_PATH = "/delete";
	

	
	
	
	public static final String PATIENT_LIST_FOR_DOCTOR = DOCTOR_ACCESS_PATH + PATIENT_ACCESS_PATH;
	
	public static final String PATIENT_GET_FOR_DOCTOR = DOCTOR_ACCESS_PATH + PATIENT_ACCESS_PATH + "/" + PATIENT_ID_PATH_VAR;
	
	public static final String PATIENT_GET = PATIENT_ACCESS_PATH;
	
	public static final String PATIENT_UPDATE_MEDICINE = DOCTOR_ACCESS_PATH + PATIENT_ACCESS_PATH + "/" + PATIENT_ID_PATH_VAR;
	
	public static final String CHECKIN_GET_BY_PATIENT = CHECKIN_PATH + DOCTOR_ACCESS_PATH + PATIENT_ACCESS_PATH;
	
	public static final String CHECKINS_GET_FOR_DOCTOR = CHECKIN_PATH + DOCTOR_ACCESS_PATH;
	
	public static final String CHECKIN_SAVE = CHECKIN_PATH + PATIENT_ACCESS_PATH + SAVE_PATH;
	
	public static final String MEDICINE_PATH = "/medicine";
	
	public static final String SEARCH_PATH = "/search";
	
	public static final String MATCH_PATH = "/match";
	
	public static final String MEDICINE_SEARCH = MEDICINE_PATH + SEARCH_PATH;
	
	public static final String MEDICINE_MATCH = MEDICINE_PATH + MATCH_PATH;
	
	public static final String REMINDER_PATH = "/reminder";
	
	public static final String REMINDER_GET_FOR_PATIENT = REMINDER_PATH + PATIENT_ACCESS_PATH ;

	public static final String REMINDER_SAVE = REMINDER_PATH + SAVE_PATH;
	
	public static final String REMINDER_DELETE = REMINDER_PATH + DELETE_PATH;
	
	public static final String ALERT_ID_VAR = "aId";
	
	public static final String ALERT_ID_PATH_VAR = "/{" + ALERT_ID_VAR + "}";
	
	public static final String ALERT_PATH = "/alert";
	
	public static final String ALERT_LIST_FOR_DOCTOR = DOCTOR_ACCESS_PATH + ALERT_PATH;
	
	public static final String ALERT_SAVE = ALERT_PATH + SAVE_PATH;
	
	public static final String ALERT_LIST_BY_PATIENT_FOR_DOCTOR = DOCTOR_ACCESS_PATH + ALERT_PATH + PATIENT_ACCESS_PATH + "/" + PATIENT_ID_PATH_VAR;
	
	public static final String ALERT_ACKNOWLEDGE = DOCTOR_ACCESS_PATH + ALERT_PATH + "/ack/" + ALERT_ID_PATH_VAR;
	
	
	

	
	
}
