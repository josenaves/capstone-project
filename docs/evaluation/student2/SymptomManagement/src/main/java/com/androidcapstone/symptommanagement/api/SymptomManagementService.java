package com.androidcapstone.symptommanagement.api;

public interface SymptomManagementService {

	public static final String DOCTOR_ACCESS_PATH = "/doctor";
	
	public static final String PATIENT_ACCESS_PATH = "/patient";
	
	public static final String TOKEN_PATH = "/oauth/token";

}
