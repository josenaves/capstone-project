package com.androidcapstone.symptommanagement.auth;

import java.security.Principal;

import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.stereotype.Service;

@Service
public class AuthTranslator {

	
	public AuthUser toAuthUser(Principal p) {
		
		if (p instanceof OAuth2Authentication) {
			OAuth2Authentication oauth = (OAuth2Authentication) p;
			return (AuthUser) oauth.getUserAuthentication().getPrincipal();
			
		}
		throw new RuntimeException("Unable to translate Principal to AuthUser");
	}
}
