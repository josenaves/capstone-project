package com.androidcapstone.symptommanagement.model;

import java.io.Serializable;

public enum EatingInterferenceType implements Serializable {
	No,Some,CanNotEat
}
