package com.androidcapstone.symptommanagement.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.hibernate.annotations.Cascade;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
public class CheckIn implements Serializable{


	private long id;
	
	
	private Date checkInTimestamp;
	

	private Patient patient;
	
	private PainLevelType painLevel;
	
	
	private EatingInterferenceType eatingInterferenceLevel;
	
	
	private List<MedicationCheckIn> response;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ss")
	public Date getCheckInTimestamp() {
		return checkInTimestamp;
	}

	public void setCheckInTimestamp(Date checkInTimestamp) {
		this.checkInTimestamp = checkInTimestamp;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "PATIENT_ID")
	public Patient getPatient() {
		return patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}

	@OneToMany(mappedBy = "checkIn", cascade = CascadeType.ALL)
	@JsonManagedReference
	public List<MedicationCheckIn> getResponse() {
		return response;
	}

	public void setResponse(List<MedicationCheckIn> response) {
		this.response = response;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof CheckIn))
			return false;
		CheckIn other = (CheckIn) obj;
		if (id != other.id)
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CheckIn [id=");
		builder.append(id);
		builder.append(", checkInTimestamp=");
		builder.append(checkInTimestamp);
		builder.append(", patient=");
		builder.append(patient);
		builder.append(", response=");
		builder.append(response);
		builder.append("]");
		return builder.toString();
	}

	@Enumerated(EnumType.STRING)
	public PainLevelType getPainLevel() {
		return painLevel;
	}

	public void setPainLevel(PainLevelType painLevel) {
		this.painLevel = painLevel;
	}

	@Enumerated(EnumType.STRING)
	public EatingInterferenceType getEatingInterferenceLevel() {
		return eatingInterferenceLevel;
	}

	public void setEatingInterferenceLevel(
			EatingInterferenceType eatingInterferenceLevel) {
		this.eatingInterferenceLevel = eatingInterferenceLevel;
	}
	
	
}
