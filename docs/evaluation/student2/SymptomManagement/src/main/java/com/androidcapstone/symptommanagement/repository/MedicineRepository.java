package com.androidcapstone.symptommanagement.repository;

import java.util.Collection;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.androidcapstone.symptommanagement.model.Medicine;

@Repository
public interface MedicineRepository extends PagingAndSortingRepository<Medicine, Long>{

	Medicine findByName(String name);
	
	Collection<Medicine> findByNameLike(String name);
}
