package com.androidcapstone.symptommanagement.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class MedicationCheckIn implements Serializable {


	private long id;
	
	
	private CheckIn checkIn;
	
	
	
	private ResponseType response;
	

	private Date medicineTaken;
	

	private Medicine medicine;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "CHECKIN_ID")
	@JsonBackReference
	public CheckIn getCheckIn() {
		return checkIn;
	}

	public void setCheckIn(CheckIn checkIn) {
		this.checkIn = checkIn;
	}


	@Enumerated(EnumType.STRING)
	public ResponseType getResponse() {
		return response;
	}

	public void setResponse(ResponseType response) {
		this.response = response;
	}

	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ss")
	public Date getMedicineTaken() {
		return medicineTaken;
	}

	public void setMedicineTaken(Date medicineTaken) {
		this.medicineTaken = medicineTaken;
	}
	
	@ManyToOne
	public Medicine getMedicine() {
		return medicine;
	}

	public void setMedicine(Medicine medicine) {
		this.medicine = medicine;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof MedicationCheckIn))
			return false;
		MedicationCheckIn other = (MedicationCheckIn) obj;
		if (id != other.id)
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CheckInResponse [id=");
		builder.append(id);
		builder.append(", response=");
		builder.append(response);
		builder.append(", medicineTaken=");
		builder.append(medicineTaken);
		builder.append(", medicine=");
		builder.append(medicine);
		builder.append("]");
		return builder.toString();
	}
	
	
}
