package com.androidcapstone.symptommanagement.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.config.annotation.builders.InMemoryClientDetailsServiceBuilder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.provider.ClientDetailsService;

@Configuration
@EnableGlobalMethodSecurity(prePostEnabled=true)
public class OAuth2SecurityConfiguration {

	@Autowired
	private UserDetailsService userDetailsService;
	
	@Configuration
	@EnableWebSecurity
	protected static class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {
		
		@Autowired
		private UserDetailsService userDetailsService;
		
		@Override
		@Bean(name="authenticationManagerBean")
		public AuthenticationManager authenticationManagerBean()
				throws Exception {
			return super.authenticationManagerBean();
		}
		
		@Autowired
		protected void registerAuthentication(final AuthenticationManagerBuilder auth) throws Exception {
			auth.userDetailsService(userDetailsService).passwordEncoder(encoder());
		}
		
		public PasswordEncoder encoder() {
			return new BCryptPasswordEncoder();
		}
	}
	
	/**
	 * configure access to app resources
	 */
	@Configuration
	@EnableResourceServer
	protected static class ResourceServer extends ResourceServerConfigurerAdapter {
		
		@Override
		public void configure(HttpSecurity http) throws Exception {
			
			http.csrf().disable();
			
			http.authorizeRequests().antMatchers("/oauth/token").anonymous();
			
//			http.authorizeRequests()
//				.antMatchers("/doctor/**")
//				.hasAuthority("Doctor");
			
			http.authorizeRequests()
				.antMatchers(HttpMethod.GET, "/**")
				.access("#oauth2.hasScope('read')");
			
			http.authorizeRequests()
				.antMatchers("/**")
				.access("#oauth2.hasScope('write')");
			
			
		}
	}
	
	/**
	 * Configure how authorization server ("/oauth/token" endpoint) validates client credentials
	 * @author jim
	 *
	 */
	@Configuration
	@EnableAuthorizationServer
	@Order(Ordered.LOWEST_PRECEDENCE - 100)
	protected static class OAuth2Config extends AuthorizationServerConfigurerAdapter {
		
		// Delegate Authentication requests to Framework
		@Autowired
		@Qualifier("authenticationManagerBean")
		private AuthenticationManager authenticationManager;
		
		@Autowired
		private UserDetailsService userDetailsService;
		
		private ClientDetailsService clientDetailsService;
		
		public OAuth2Config() throws Exception {
			// service with client credentials
			// TODO figure out how to configure this beast
			clientDetailsService = new InMemoryClientDetailsServiceBuilder()
				.withClient("mobile").authorizedGrantTypes("password")
				.authorities("ROLE_CLIENT", "ROLE_TRUSTED_CLIENT")
				.scopes("read","write").resourceIds("doctor", "patient")
				.and()
				.withClient("mobileReader").authorizedGrantTypes("password")
				.authorities("ROLE_CLIENT")
				.scopes("read").resourceIds("patient")
				.accessTokenValiditySeconds(3600).and().build();
			
				
		}
		
		@Bean
		public ClientDetailsService clientDetailsService() {
			return clientDetailsService;
		}
		
		public UserDetailsService userDetailsService() {
			return userDetailsService;
		}
		
		@Override
		public void configure(AuthorizationServerEndpointsConfigurer endpoints)
				throws Exception {
			endpoints.authenticationManager(authenticationManager);
		}
		
		@Override
		public void configure(ClientDetailsServiceConfigurer clients)
				throws Exception {
			clients.withClientDetails(clientDetailsService);
		}
	}
	
}
