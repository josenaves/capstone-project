package com.josenaves.capstone.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.josenaves.capstone.client.DoctorServiceApi;
import com.josenaves.capstone.model.Doctor;

/**
 * An interface for a repository that can store Doctors objects.
 * @author josenaves
 */
// This @RepositoryRestResource annotation tells Spring Data Rest to
// expose the DoctorRepository through a controller and map it to the 
// "/doctor" path. This automatically enables you to do the following:
//
// 1. List all doctors by sending a GET request to /doctor 
// 2. Add a video by sending a POST request to /doctor with the JSON for a doctor
// 3. Get a specific doctor by sending a GET request to /doctor/{doctorId}
//    (e.g., /doctor/1 would return the JSON for the doctor with id=1)
// 4. Send search requests to our findByXYZ methods to /doctor/search/findByXYZ
//    (e.g., /doctor/search/findByName?title=Foo)
//
// See: http://docs.spring.io/spring-data/jpa/docs/1.3.0.RELEASE/reference/html/jpa.repositories.html
//      for more examples of writing query methods
//
@RepositoryRestResource(path = DoctorServiceApi.DOCTOR_PATH)
public interface DoctorRepository extends PagingAndSortingRepository<Doctor, Long> {
	public Doctor findByUserNameAndPassword(String username, String password);
}
