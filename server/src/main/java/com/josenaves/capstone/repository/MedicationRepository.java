package com.josenaves.capstone.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.josenaves.capstone.client.MedicationServiceApi;
import com.josenaves.capstone.model.Medication;

/**
 * An interface for a repository that can store Medication objects.
 * @author josenaves
 */
@RepositoryRestResource(path = MedicationServiceApi.MEDICATION_PATH)
public interface MedicationRepository extends PagingAndSortingRepository<Medication, Long> {	
}
