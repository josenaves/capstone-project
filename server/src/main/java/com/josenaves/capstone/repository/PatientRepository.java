// This @RepositoryRestResource annotation tells Spring Data Rest to
// expose the PatientRepository through a controller and map it to the 
// "/patient" path. This automatically enables you to do the following:
//
// 1. List all patients by sending a GET request to /patient 
// 2. Add a patient by sending a POST request to /patient with the JSON for a patient
// 3. Get a specific video by sending a GET request to /patient/{patientId}
//    (e.g., /patient/1 would return the JSON for the patient with id=1)
// 4. Send search requests to our findByXYZ methods to /patient/search/findByXYZ
//    (e.g., /patient/search/findByName?title=Foo)
//
// See: http://docs.spring.io/spring-data/jpa/docs/1.3.0.RELEASE/reference/html/jpa.repositories.html
//      for more examples of writing query methods
//
package com.josenaves.capstone.repository;

import java.util.Collection;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.josenaves.capstone.client.PatientServiceApi;
import com.josenaves.capstone.model.Patient;

/**
 * An interface for a repository that can store Patient objects and allow them to be searched by firstName.
 * @author josenaves
 */
@RepositoryRestResource(path = PatientServiceApi.PATIENT_PATH)
public interface PatientRepository extends PagingAndSortingRepository<Patient, Long> {

	// Find all patients with a matching name (e.g., Patient.firstName)
	public Collection<Patient> findByFirstName(
			// The @Param annotation tells Spring Data Rest which HTTP request
			// parameter it should use to fill in the "firstName" variable used to search for Patients
			@Param(PatientServiceApi.FIRST_NAME_PARAMETER) String firstName);
	
	public Patient findByUserNameAndPassword(String userName, String password);
}
