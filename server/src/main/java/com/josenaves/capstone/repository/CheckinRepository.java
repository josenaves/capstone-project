package com.josenaves.capstone.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.josenaves.capstone.client.CheckinServiceApi;
import com.josenaves.capstone.model.CheckIn;

/**
 * An interface for a repository that can store Checkins objects.
 * @author josenaves
 */
// This @RepositoryRestResource annotation tells Spring Data Rest to
// expose the CheckinRepository through a controller and map it to the 
// "/checkin" path. This automatically enables you to do the following:
//
// 1. List all checkins by sending a GET request to /checkin 
// 2. Add a checkin by sending a POST request to /checkin with the JSON for a checkin
// 3. Get a specific checkin by sending a GET request to /checkin/{chedkinId}
//    (e.g., /checkin/1 would return the JSON for the checkin with id=1)
//
// See: http://docs.spring.io/spring-data/jpa/docs/1.3.0.RELEASE/reference/html/jpa.repositories.html
//      for more examples of writing query methods
//
@RepositoryRestResource(path = CheckinServiceApi.CHECKIN_PATH)
public interface CheckinRepository extends PagingAndSortingRepository<CheckIn, Long> {
}
