package com.josenaves.capstone.controller;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.josenaves.capstone.client.PatientServiceApi;
import com.josenaves.capstone.model.CheckIn;
import com.josenaves.capstone.model.Medication;
import com.josenaves.capstone.model.Patient;
import com.josenaves.capstone.repository.PatientRepository;

/**
 * This simple MedicationController allows clients to send HTTP DELETE 
 * requests to delete medication from users that are stored in a database. 
 * 
 * @author josenaves
 */
// Tell Spring that this class is a Controller that should 
// handle certain HTTP requests for the DispatcherServlet
@RestController
public class PatientController  {
	// The PatientRepository that we are going to store our patients in. 
	
	// We don't explicitly construct a PatientRepository, but
	// instead mark this object as a dependency that needs to be
	// injected by Spring. Our Application class has a method
	// annotated with @Bean that determines what object will end
	// up being injected into this member variable.

	// Also notice that we don't even need a setter for Spring to
	// do the injection.
	@Autowired
	private PatientRepository repo;
	
	@RequestMapping(value=PatientServiceApi.PATIENT_PATH + "/{id}/medication", method=RequestMethod.GET)
	public @ResponseBody List<Medication> getMedication(@PathVariable(value="id") Long patientId){
		Patient patient = repo.findOne(patientId);
		return patient.getMedication();
	}

	@RequestMapping(value=PatientServiceApi.PATIENT_PATH + "/{id}/medication", method=RequestMethod.DELETE)
	public void deleteMedication(@PathVariable(value="id") Long patientId){
		Patient patient = repo.findOne(patientId);
		patient.getMedication().clear();
		repo.save(patient);
	}
	
	@RequestMapping(value=PatientServiceApi.PATIENT_PATH + "/{id}/medication", method=RequestMethod.PUT)
	public void updateMedication(@PathVariable(value="id") Long patientId, @RequestBody List<Medication> medication){
		Patient patient = repo.findOne(patientId);
		patient.getMedication().clear();
		patient.getMedication().addAll(medication);
		repo.save(patient);
	}

	@RequestMapping(value=PatientServiceApi.PATIENT_PATH + "/{id}/checkin", method=RequestMethod.GET)
	public @ResponseBody Set<CheckIn> getCheckins(@PathVariable(value="id") Long patientId){
		Patient patient = repo.findOne(patientId);
		return patient.getCheckin();
	}

	@RequestMapping(value=PatientServiceApi.PATIENT_PATH + "/{id}/checkin", method=RequestMethod.POST)
	public @ResponseBody Patient addCheckin(@PathVariable(value="id") Long patientId, @RequestBody CheckIn checkin){
		Patient patient = repo.findOne(patientId);
		patient.getCheckin().add(checkin);
		return repo.save(patient);
	}
}