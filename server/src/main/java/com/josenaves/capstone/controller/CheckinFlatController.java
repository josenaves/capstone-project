package com.josenaves.capstone.controller;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.josenaves.capstone.client.CheckinServiceApi;
import com.josenaves.capstone.model.CheckIn;
import com.josenaves.capstone.model.CheckinFlat;
import com.josenaves.capstone.model.Medication;
import com.josenaves.capstone.model.Patient;
import com.josenaves.capstone.repository.CheckinRepository;
import com.josenaves.capstone.repository.MedicationRepository;
import com.josenaves.capstone.repository.PatientRepository;

/**
 * This simple CheckinFlatController allows clients to send HTTP POST 
 * requests to create check-ins for users that are stored in a database. 
 * 
 * @author josenaves
 */
// Tell Spring that this class is a Controller that should 
// handle certain HTTP requests for the DispatcherServlet
@RestController
public class CheckinFlatController  {
	// The CheckinRepository that we are going to store our patients in. 
	
	// We don't explicitly construct a PatientRepository, but
	// instead mark this object as a dependency that needs to be
	// injected by Spring. Our Application class has a method
	// annotated with @Bean that determines what object will end
	// up being injected into this member variable.

	// Also notice that we don't even need a setter for Spring to
	// do the injection.
	@Autowired
	private CheckinRepository repoCheckin;
	
	@Autowired
	private PatientRepository repoPatient;
	
	@Autowired
	private MedicationRepository repoMedication;
	
	@RequestMapping(value=CheckinServiceApi.CHECKIN_PATH, method=RequestMethod.POST)
	public @ResponseBody CheckIn addCheckin(@RequestBody CheckinFlat checkin){
		
		Patient patient = repoPatient.findOne(checkin.getPatientId());
		Medication medication = repoMedication.findOne(checkin.getMedicationId());
		
		CheckIn realCheckin = new CheckIn();
		realCheckin.setPatient(patient);
		realCheckin.setMedication(medication);
		realCheckin.setMouthPain(checkin.getMouthPain());
		realCheckin.setStopEating(checkin.getStopEating());
		realCheckin.setTaken(checkin.isTaken());
		
		String checkinDatetime = convertDate(checkin.getDatetime());
		realCheckin.setDatetime(checkinDatetime);
		
		//update patient's last checkin date
		patient.setLastCheckinDate(checkinDatetime);
		
		return repoCheckin.save(realCheckin);
	}
	
	private String convertDate(String timePart) {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
		String datePart = df.format(Calendar.getInstance().getTime());
		return datePart + " " + timePart;
	}
}