package com.josenaves.capstone.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.josenaves.capstone.client.LoginServiceApi;
import com.josenaves.capstone.model.Doctor;
import com.josenaves.capstone.model.Login;
import com.josenaves.capstone.model.Patient;
import com.josenaves.capstone.repository.DoctorRepository;
import com.josenaves.capstone.repository.PatientRepository;

/**
 * This simple LoginController allows clients to send HTTP POST requests with
 * username and a password that are stored in a database. 
 * 
 * @author josenaves
 */
// Tell Spring that this class is a Controller that should 
// handle certain HTTP requests for the DispatcherServlet
@Controller
public class LoginController implements LoginServiceApi {
	// The PatientRepository that we are going to store our patients in. 
	
	// We don't explicitly construct a PatientRepository, but
	// instead mark this object as a dependency that needs to be
	// injected by Spring. Our Application class has a method
	// annotated with @Bean that determines what object will end
	// up being injected into this member variable.

	// Also notice that we don't even need a setter for Spring to
	// do the injection.
	@Autowired
	private PatientRepository patients;
	
	// The DoctorRepository that we are going to store our doctors in. 
	@Autowired
	private DoctorRepository doctors;

	// Receives POST requests to /login and converts the HTTP
	// request body, which should contain json, into a Login
	// object before adding it to the list. The @RequestBody
	// annotation on the Login parameter is what tells Spring
	// to interpret the HTTP request body as JSON and convert
	// it into a Login object to pass into the method. The
	// @ResponseBody annotation tells Spring to convert the
	// return value from the method back into JSON and put
	// it into the body of the HTTP response to the client.
	//
	// The LOGIN_PATH is set to "/login" in the LoginApi
	// interface. We use this constant to ensure that the 
	// client and service paths for the LoginApi are always
	// in synch.
	@RequestMapping(value=LoginServiceApi.LOGIN_PATH, method=RequestMethod.POST)
	public @ResponseBody Login authenticate(@RequestBody Login login){
		Patient patient;
		Doctor doctor;
		
		Login authenticatedUser = null;
		
		// try get a patient with that username and password
		patient = patients.findByUserNameAndPassword(login.getUsername(), login.getPassword());
		
		if (patient == null) {
			// try get a doctor with that username and password
			doctor = doctors.findByUserNameAndPassword(login.getUsername(), login.getPassword());
			
			if (doctor != null) {
				authenticatedUser = new Login(doctor.getId(), doctor.getUserName(), doctor.getPassword(), Login.ROLE_DOCTOR); 
			}
		}
		else {
			authenticatedUser = new Login(patient.getId(), patient.getUserName(), patient.getPassword(), Login.ROLE_PATIENT);
		}
		return authenticatedUser;
	}
}
