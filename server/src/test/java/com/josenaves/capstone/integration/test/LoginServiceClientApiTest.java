package com.josenaves.capstone.integration.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import retrofit.RestAdapter.LogLevel;
import retrofit.client.ApacheClient;

import com.josenaves.capstone.client.DoctorServiceApi;
import com.josenaves.capstone.client.LoginServiceApi;
import com.josenaves.capstone.client.PatientServiceApi;
import com.josenaves.capstone.client.SecuredRestBuilder;
import com.josenaves.capstone.client.UnsafeHttpsClient;
import com.josenaves.capstone.model.Doctor;
import com.josenaves.capstone.model.Login;
import com.josenaves.capstone.model.Patient;

/**
 * This integration test sends HTTP calls to the server using the Retrofit library.
 * The server must be running before you launch this test. 
 * @author josenaves
 */
public class LoginServiceClientApiTest {

	private final String TEST_URL = "https://localhost:8443";
	
	//private final String USERNAME1 = "admin";
	private final String USERNAME2 = "user0";
	private final String PASSWORD = "pass";
	private final String CLIENT_ID = "mobile";

	private PatientServiceApi patientService = new SecuredRestBuilder()
			.setClient(new ApacheClient(UnsafeHttpsClient.createUnsafeClient()))
			.setEndpoint(TEST_URL).setLogLevel(LogLevel.FULL)
			.setLoginEndpoint(TEST_URL + PatientServiceApi.TOKEN_PATH)
			.setUsername(USERNAME2).setPassword(PASSWORD).setClientId(CLIENT_ID)
			.build()
			.create(PatientServiceApi.class);
	
	private DoctorServiceApi doctorService = new SecuredRestBuilder()
		.setClient(new ApacheClient(UnsafeHttpsClient.createUnsafeClient()))
		.setEndpoint(TEST_URL).setLogLevel(LogLevel.FULL)
		.setLoginEndpoint(TEST_URL + DoctorServiceApi.TOKEN_PATH)
		.setUsername(USERNAME2).setPassword(PASSWORD).setClientId(CLIENT_ID)
		.build()
		.create(DoctorServiceApi.class);
	
	private LoginServiceApi loginService = new SecuredRestBuilder()
			.setClient(new ApacheClient(UnsafeHttpsClient.createUnsafeClient()))
			.setEndpoint(TEST_URL).setLogLevel(LogLevel.FULL)
			.setLoginEndpoint(TEST_URL + PatientServiceApi.TOKEN_PATH)
			.setUsername(USERNAME2).setPassword(PASSWORD).setClientId(CLIENT_ID)
			.build()
			.create(LoginServiceApi.class);

	private final Patient patient = new Patient("José", "Naves", "19721006", 1, "josenaves", "123456");
	private final Doctor doctor = new Doctor("José", "Henrique", 1234, "drrique", "123456");
	
	private final Login loginPatient = new Login("josenaves", "123456", Login.ROLE_PATIENT);
	private final Login loginDoctor = new Login("drrique", "123456", Login.ROLE_DOCTOR);
	
	@Before
	public void setUp() {
		patientService.addPatient(patient);
		doctorService.addDoctor(doctor);
	}
	
	@Test
	public void testAuthenticatePatient() throws Exception {
		// Add the patient
		Login found = loginService.authenticate(loginPatient);
		assertTrue(found != null);
		assertEquals(found.getRole(), Login.ROLE_PATIENT);
	}

	@Test
	public void testAuthenticateDoctor() throws Exception {
		// Add the patient
		Login found = loginService.authenticate(loginDoctor);
		assertTrue(found != null);
		assertEquals(found.getRole(), Login.ROLE_DOCTOR);
	}
	
	@After
	public void shutDown() {
		//patientService.deletePatient(id);
	}
	
}
