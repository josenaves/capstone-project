package com.josenaves.capstone.integration.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Collection;

import org.junit.Test;

import retrofit.RestAdapter.LogLevel;
import retrofit.client.ApacheClient;

import com.josenaves.capstone.client.PatientServiceApi;
import com.josenaves.capstone.client.SecuredRestBuilder;
import com.josenaves.capstone.client.UnsafeHttpsClient;
import com.josenaves.capstone.model.Patient;
import com.josenaves.capstone.utils.TestUtils;

/**
 * This integration test sends HTTP calls to the server using the Retrofit library.
 * The server must be running before you launch this test. 
 * 
 * @author josenaves
 */
public class PatientServiceClientApiTest {

	private final String TEST_URL = "https://localhost:8443";
	
	//private final String USERNAME1 = "admin";
	private final String USERNAME2 = "user0";
	private final String PASSWORD = "pass";
	private final String CLIENT_ID = "mobile";

	private PatientServiceApi patientService = new SecuredRestBuilder()
			.setClient(new ApacheClient(UnsafeHttpsClient.createUnsafeClient()))
			.setEndpoint(TEST_URL).setLogLevel(LogLevel.FULL)
			.setLoginEndpoint(TEST_URL + PatientServiceApi.TOKEN_PATH)
			.setUsername(USERNAME2).setPassword(PASSWORD).setClientId(CLIENT_ID)
			.build()
			.create(PatientServiceApi.class);

	private final String user = TestUtils.randomPersonName();
	
	private final Patient patient = new Patient(user, TestUtils.randomPersonName(), "19721006", 6, user, user + "!!!");
	
	/**
	 * This test creates a Patient, adds the Patient to the PatientService, and then checks that 
	 * the Patient is included in the list when getPatientList() is called.
	 * 
	 * @throws Exception
	 */
	@Test
	public void testPatientCrud() throws Exception {
		
		// Add the patient
		patientService.addPatient(patient);
		
		// We should get back the video that we added above
		Collection<Patient> patients = patientService.getPatientList();
		assertTrue(patients.size() > 0);
		assertTrue(patients.contains(patient));
		
		for (Patient v : patients){
			patientService.deletePatient(v.getId());
		}
		
		patients = patientService.getPatientList();
		assertEquals(0, patients.size());
	}

	
	@Test
	public void testPatientFindByFirstName() throws Exception {
		
		// Add the patient
		patientService.addPatient(patient);
		
		Collection<Patient> patients = patientService.findByFirstName(patient.getFirstName());
		assertTrue(patients.size() > 0);
		for(Patient v : patients){
			assertEquals(patient.getFirstName(), v.getFirstName());
		}
		
		for(Patient v : patients){
			patientService.deletePatient(v.getId());
		}
	
		assertTrue(patientService.findByFirstName(patient.getFirstName()).size() == 0);
	}
	
	@Test
	public void testPatientFindById() throws Exception {
		
		// Add the patient
		patientService.addPatient(patient);
		
		Collection<Patient> patients = patientService.findByFirstName(patient.getFirstName());
		assertTrue(patients.size() > 0);
		for(Patient v : patients){
			assertEquals(patient.getFirstName(), v.getFirstName());
		}
						
		for(Patient v : patients){
			// find by id
			Patient p = patientService.getPatient(v.getId());
			assertTrue(p != null);

			patientService.deletePatient(v.getId());
		}
	
		assertTrue(patientService.findByFirstName(patient.getFirstName()).size() == 0);
	}
}
