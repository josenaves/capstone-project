package com.josenaves.capstone.integration.test;

import static org.junit.Assert.assertTrue;

import java.util.Collection;

import org.junit.Test;

import retrofit.RestAdapter.LogLevel;
import retrofit.client.ApacheClient;

import com.josenaves.capstone.client.MedicationServiceApi;
import com.josenaves.capstone.client.SecuredRestBuilder;
import com.josenaves.capstone.client.UnsafeHttpsClient;
import com.josenaves.capstone.model.Medication;

/**
 * This integration test sends HTTP calls to the server using the Retrofit library.
 * The server must be running before you launch this test. 
 * 
 * @author josenaves
 */
public class MedicationServiceClientApiTest {

	private final String TEST_URL = "https://localhost:8443";
	
	//private final String USERNAME1 = "admin";
	private final String USERNAME2 = "user0";
	private final String PASSWORD = "pass";
	private final String CLIENT_ID = "mobile";

	private MedicationServiceApi medicationService = new SecuredRestBuilder()
			.setClient(new ApacheClient(UnsafeHttpsClient.createUnsafeClient()))
			.setEndpoint(TEST_URL).setLogLevel(LogLevel.FULL)
			.setLoginEndpoint(TEST_URL + MedicationServiceApi.TOKEN_PATH)
			.setUsername(USERNAME2).setPassword(PASSWORD).setClientId(CLIENT_ID)
			.build()
			.create(MedicationServiceApi.class);

	/**
	 * This test only get a list of medication and a single medication
	 * 
	 * @throws Exception
	 */
	@Test
	public void testMedicationCrud() throws Exception {
		// We should get back all the medications
		Collection<Medication> medications = medicationService.getMedicationList();
		assertTrue(medications.size() > 0);
		
		Medication medication = medicationService.getMedication(1);
		assertTrue(medication != null);
		assertTrue(medication.getId() == 1);
	}

	/**
	 * This test create and delete a medication
	 * @throws Exception
	 */
	@Test
	public void testMedicationCreation() throws Exception {
		Medication medication = new Medication("Oxy");
		medicationService.addMedication(medication);

		Medication m = findMedication(medication.getName());
		assert(m != null);
		
		medicationService.deleteMedication(m.getId());
	}
	
	/**
	 * This test get a single medication
	 * @throws Exception
	 */
	@Test
	public void testFindMedicationById() throws Exception {
		// We should get back all the medications
		Collection<Medication> medications = medicationService.getMedicationList();
		assertTrue(medications.size() > 0);
		
		for (Medication v : medications){
			Medication med = medicationService.getMedication(v.getId());
			assertTrue(med != null);
			assertTrue(med.getId() == v.getId());
		}
	}
	
	private Medication findMedication(String name) {
		Collection<Medication> medications = medicationService.getMedicationList();
		for (Medication medication : medications){
			if (medication.getName().equals(name)) {
				return medication;
			}
		}
		return null;
	}
}
