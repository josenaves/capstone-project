package com.josenaves.capstone.integration.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Collection;

import org.junit.Test;

import retrofit.RestAdapter.LogLevel;
import retrofit.client.ApacheClient;

import com.josenaves.capstone.client.DoctorServiceApi;
import com.josenaves.capstone.client.SecuredRestBuilder;
import com.josenaves.capstone.client.UnsafeHttpsClient;
import com.josenaves.capstone.model.Doctor;
import com.josenaves.capstone.utils.TestUtils;

/**
 * This integration test sends HTTP calls to the server using the Retrofit library.
 * The server must be running before you launch this test. 
 * 
 * @author josenaves
 */
public class DoctorServiceClientApiTest {

	//private final String TEST_URL = "http://localhost:8080";
	private final String TEST_URL = "https://localhost:8443";
	
	//private final String USERNAME1 = "admin";
	private final String USERNAME2 = "user0";
	private final String PASSWORD = "pass";
	private final String CLIENT_ID = "mobile";

	/*
	private DoctorServiceApi doctorService = new RestAdapter.Builder()
			.setEndpoint(TEST_URL).setLogLevel(LogLevel.FULL).build()
			.create(DoctorServiceApi.class);
	*/		
	private DoctorServiceApi doctorService = new SecuredRestBuilder()
			.setClient(new ApacheClient(UnsafeHttpsClient.createUnsafeClient()))
			.setEndpoint(TEST_URL).setLogLevel(LogLevel.FULL)
			.setLoginEndpoint(TEST_URL + DoctorServiceApi.TOKEN_PATH)
			.setUsername(USERNAME2).setPassword(PASSWORD).setClientId(CLIENT_ID)
			.build()
			.create(DoctorServiceApi.class);

	private final String username = TestUtils.randomPersonName();
	
	private final Doctor doctor = new Doctor(username, TestUtils.randomPersonName(), 6, username, username + "!!!");
	
	/**
	 * This test creates a Doctor, adds the Doctor to the DoctorService, and then checks that 
	 * the Doctor is included in the list when getDoctorList() is called.
	 * 
	 * @throws Exception
	 */
	@Test
	public void testDoctorCrud() throws Exception {
		
		// Add the doctor
		doctorService.addDoctor(doctor);
		
		// We should get back the video that we added above
		Collection<Doctor> doctors = doctorService.getDoctorList();
		assertTrue(doctors.size() > 0);
		assertTrue(doctors.contains(doctor));
		
		for (Doctor v : doctors){
			doctorService.deleteDoctor(v.getId());
		}
		
		doctors = doctorService.getDoctorList();
		assertEquals(0, doctors.size());
	}
	
	/**
	 * Test find by id
	 * @throws Exception
	 */
	@Test
	public void testFindDoctorById() throws Exception {
		
		// Add the doctor
		doctorService.addDoctor(doctor);
		
		// We should get back the video that we added above
		Collection<Doctor> doctors = doctorService.getDoctorList();
		assertTrue(doctors.size() > 0);
		assertTrue(doctors.contains(doctor));
		
		for (Doctor v : doctors){
			Doctor doc = doctorService.getDoctor(v.getId());
			assertTrue(doc != null);
			assertTrue(doc.getId() == v.getId());
			doctorService.deleteDoctor(v.getId());
		}
		
		doctors = doctorService.getDoctorList();
		assertEquals(0, doctors.size());
	}
	
	
}
