package com.josenaves.capstone.app;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.Callable;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.josenaves.capstone.app.adapter.PatientAdapter;
import com.josenaves.capstone.app.async.CallableTask;
import com.josenaves.capstone.app.async.TaskCallback;
import com.josenaves.capstone.app.client.DoctorService;
import com.josenaves.capstone.app.client.DoctorServiceApi;
import com.josenaves.capstone.app.model.Doctor;
import com.josenaves.capstone.app.model.Patient;
import com.josenaves.capstone.app.util.ApplicationRepository;

public class DoctorActivity extends Activity implements AdapterView.OnItemClickListener {

	private static final String TAG = DoctorActivity.class.getSimpleName();
	
	private static final String DOCTOR_TITLE = "Doctor ";
	private static final String DOCTOR_PATIENTS = "Patients attended: ";

	private long doctorId = -1;
	
	private ApplicationRepository repo;

	private TextView textDoctor;
	private TextView textPatientsAttended;
	
	private ListView listView;
	
	private static final long REFRESH_TIME = 60000;
	private Handler handler;

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_doctor);
		Log.d(TAG, "onCreate _________________");

		repo = new ApplicationRepository(this);
		
		listView = (ListView) findViewById(R.id.list_doctor_patients);
		listView.setOnItemClickListener(this);
		
		setAutoRefresh();
		
		getDoctorInTheCloud();
	}
	
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        final Patient item = (Patient) parent.getItemAtPosition(position);
        Log.d(TAG, "clicked on " + item.toString());
        
        // put patient details on SharedProperties
        repo.savePatientData(item);
        
        // open up patient details activity
        Intent intent = new Intent(getApplicationContext(), PatientDetailsActivity.class);
        intent.putExtra("PATIENT_ID", item.getId());
        startActivity(intent);
	}	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		Log.d(TAG, "onCreateOptionsMenu _________________");

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.doctor, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_refresh) {
			Toast.makeText(getApplicationContext(), "Refreshing...", Toast.LENGTH_SHORT).show();
			getDoctorInTheCloud();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	private void getDoctorInTheCloud() {
		
		doctorId = repo.getUserId();
		Log.d(TAG, "doctorId = " + doctorId);

		final DoctorServiceApi api = DoctorService.init(getString(R.string.server_url));
		CallableTask.invoke(
			new Callable<Doctor>() {
				@Override
				public Doctor call() throws Exception {
					return api.getDoctor(doctorId);
				}
			},
			new TaskCallback<Doctor>() {
				@Override
				public void success(Doctor doctor) {
					if (doctor == null) {
						Log.e(TAG, "No doctor returned for id " + doctorId);
					}
					else {
						// fill the fields
						Log.d(TAG, doctor.toString());
						
						textDoctor = (TextView)findViewById(R.id.text_doctor);
						textDoctor.setText(DOCTOR_TITLE + doctor.getFirstName() + " " + doctor.getLastName());
					}
					
					// get patients
					getPatientsInTheCloud();
				}

				@Override
				public void error(Exception e) {
					Log.e(TAG, "Error: could not get patient data", e);
					Toast.makeText(getApplicationContext(), "Error gettin patient from the cloud!", Toast.LENGTH_LONG).show();
				}
			}
		);
	}
	
	
	private void getPatientsInTheCloud() {
		
		doctorId = repo.getUserId();
		Log.d(TAG, "doctorId = " + doctorId);

		final DoctorServiceApi api = DoctorService.init(getString(R.string.server_url));
		CallableTask.invoke(
			new Callable<List<Patient>>() {
				@Override
				public List<Patient> call() throws Exception {
					return api.getDoctorPatients(doctorId);
				}
			},
			new TaskCallback<List<Patient>>() {
				@Override
				public void success(List<Patient> patients) {
					if (patients == null) {
						Log.e(TAG, "No doctor returned for id " + doctorId);
					}
					else {
						// order the patients by checkins (desc)
						Collections.sort(patients, new Comparator<Patient>() {
							@Override
							public int compare(Patient lhs, Patient rhs) {
								return rhs.getLastCheckinDate().compareTo(lhs.getLastCheckinDate());
							}
						});
						
						// create the adapter to convert the list to views
						PatientAdapter adapter = new PatientAdapter(DoctorActivity.this, patients);
						
						// attach the adapter to a listview
						DoctorActivity.this.listView.setAdapter(adapter);
						
						// fill the fields
						Log.d(TAG, patients.toString());
						
						textPatientsAttended = (TextView)findViewById(R.id.text_doctor_patients_total);
						textPatientsAttended.setText(DOCTOR_PATIENTS + patients.size());
					}
				}

				@Override
				public void error(Exception e) {
					Log.e(TAG, "Error: could not get patient data", e);
					Toast.makeText(getApplicationContext(), "Error gettin patient from the cloud!", Toast.LENGTH_LONG).show();
				}
			}
		);
	}	
	
	private final Runnable refreshRunnable = new Runnable() {
		@Override
		public void run() {
			Toast.makeText(getApplicationContext(), "Refreshing...", Toast.LENGTH_SHORT).show();
			getDoctorInTheCloud();
	        DoctorActivity.this.handler.postDelayed(refreshRunnable, REFRESH_TIME);            
		}
	};

	private void setAutoRefresh() {
		this.handler = new Handler();
		handler.postDelayed(refreshRunnable, REFRESH_TIME);
	}
	
	@Override
	protected void onStop() {
		super.onStop();
		Log.d(TAG, "onStop _________________");
	    handler.removeCallbacks(refreshRunnable);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		Log.d(TAG, "onDestroy _________________");
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		Log.d(TAG, "onPause _________________");
	}
	
	@Override
	protected void onRestart() {
		super.onRestart();
		Log.d(TAG, "onRestart _________________");
	}
	
	@Override
	protected void onStart() {
		super.onRestart();
		Log.d(TAG, "onStart _________________");
		
		repo.dumpProperties();
	}

}
