package com.josenaves.capstone.app;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.josenaves.capstone.app.adapter.MedicationSpinnerAdapter;
import com.josenaves.capstone.app.async.CallableTask;
import com.josenaves.capstone.app.async.TaskCallback;
import com.josenaves.capstone.app.client.CheckinService;
import com.josenaves.capstone.app.client.CheckinServiceApi;
import com.josenaves.capstone.app.client.PatientService;
import com.josenaves.capstone.app.client.PatientServiceApi;
import com.josenaves.capstone.app.model.CheckIn;
import com.josenaves.capstone.app.model.CheckinFlat;
import com.josenaves.capstone.app.model.Medication;
import com.josenaves.capstone.app.util.ApplicationRepository;
import com.josenaves.capstone.app.util.CapstoneUtils;

public class CheckinActivity extends Activity {
	
	private static final String TAG = CheckinActivity.class.getSimpleName();

	private ToggleButton toggleWellControlled;
	private ToggleButton toggleModerated;
	private ToggleButton toggleSevere;
	
	private ToggleButton toggleNo;
	private ToggleButton toggleSome;
	private ToggleButton toggleCantEat;
	
	private Switch switchTaken;
	private EditText editTime; 
	private Spinner spinnerMedication;
	private Button buttonSave;
	
	private CheckinFlat checkin;
	
	private List<Medication> medication = new ArrayList<Medication>();
	
	private String patientName;
	private Long patientId;
	
	private ApplicationRepository repo;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_checkin);
		Log.d(TAG, "onCreate _________________");
		
		repo = new ApplicationRepository(this);
		patientId = CapstoneUtils.getPatientId(this);
		patientName = repo.getPatientName();
		
		findViews();
		
		getPatientMedicationInTheCloud();
	}
	
	private void findViews() {
		TextView textPatient = (TextView) findViewById(R.id.text_checkin_patient_name);
		textPatient.setText("Hello, " + patientName);
		
		toggleWellControlled = (ToggleButton) findViewById(R.id.toggle_well_controlled);
		toggleModerated = (ToggleButton) findViewById(R.id.toggle_moderated);
		toggleSevere = (ToggleButton) findViewById(R.id.toggle_severe);
		
		HowBadClickListener listener = new HowBadClickListener();
		toggleWellControlled.setOnCheckedChangeListener(listener);
		toggleModerated.setOnCheckedChangeListener(listener);
		toggleSevere.setOnCheckedChangeListener(listener);
		
		toggleNo = (ToggleButton) findViewById(R.id.toggle_no);
		toggleSome = (ToggleButton) findViewById(R.id.toggle_some);
		toggleCantEat = (ToggleButton) findViewById(R.id.toggle_cant_eat);
		
		PainClickListener painListener = new PainClickListener();
		toggleNo.setOnCheckedChangeListener(painListener);
		toggleSome.setOnCheckedChangeListener(painListener);
		toggleCantEat.setOnCheckedChangeListener(painListener);
		
		spinnerMedication = (Spinner)findViewById(R.id.spinner_medication);
		
		switchTaken = (Switch)findViewById(R.id.switch_take_medication);
		
		editTime = (EditText)findViewById(R.id.edit_time);
		editTime.setText(CapstoneUtils.getSystemTime());
		
		buttonSave = (Button)findViewById(R.id.button_save_checkin);
		buttonSave.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				makeCheckinObject();
				saveCheckinInTheCloud();
			}
		});
	}
	
	private CheckinFlat makeCheckinObject() {
		checkin = new CheckinFlat();
		
		int mouthPain = -1;
    	if (toggleWellControlled.isChecked()) {
    		mouthPain = CheckIn.CONTROLLED;
    	}
    	else if (toggleModerated.isChecked()) {
    		mouthPain = CheckIn.MODERATED;
    	}
    	else {
    		mouthPain = CheckIn.SEVERE;
    	}
		checkin.setMouthPain(mouthPain);
		
		int stopEating = -1;
    	if (toggleNo.isChecked()) {
    		stopEating = CheckIn.CONTROLLED;
    	}
    	else if (toggleSome.isChecked()) {
    		stopEating = CheckIn.SOME;
    	}
    	else {
    		stopEating = CheckIn.CANT_EAT;
    	}
		checkin.setStopEating(stopEating);
		
		checkin.setTaken(switchTaken.isChecked());
		checkin.setDatetime(editTime.getText().toString());
		
		long medicationId = ((Medication)spinnerMedication.getSelectedItem()).getId();
		checkin.setMedicationId(medicationId);
		checkin.setPatientId(patientId);
		
		return checkin;
	}
	
	
	private void setSpinner() {
		// Create an ArrayAdapter using the medication list
		ArrayAdapter<Medication> adapter = new MedicationSpinnerAdapter(this, medication);
		// Apply the adapter to the spinner
		spinnerMedication.setAdapter(adapter);
	}
	
	private class HowBadClickListener implements OnCheckedChangeListener {
	    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
	        if (isChecked) {
	        	if (toggleWellControlled == buttonView) {
	        		toggleModerated.setChecked(false);
	        		toggleSevere.setChecked(false);
	        	}
	        	else if (toggleModerated == buttonView) {
	        		toggleWellControlled.setChecked(false);
	        		toggleSevere.setChecked(false);
	        	}
	        	else {
	        		toggleWellControlled.setChecked(false);
	        		toggleModerated.setChecked(false);
	        	}
	        } 
	    }
	}
	
	private class PainClickListener implements OnCheckedChangeListener {
	    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
	        if (isChecked) {
	        	if (toggleNo == buttonView) {
	        		toggleSome.setChecked(false);
	        		toggleCantEat.setChecked(false);
	        	}
	        	else if (toggleSome == buttonView) {
	        		toggleNo.setChecked(false);
	        		toggleCantEat.setChecked(false);
	        	}
	        	else {
	        		toggleNo.setChecked(false);
	        		toggleSome.setChecked(false);
	        	}
	        } 
	    }
	}
	
	private void getPatientMedicationInTheCloud() {
		final PatientServiceApi api = PatientService.init(getString(R.string.server_url));
		
		CallableTask.invoke(
			new Callable<List<Medication>>() {
				@Override
				public List<Medication> call() throws Exception {
					return api.getPatientMedication(patientId);
				}
			}, 
			new TaskCallback<List<Medication>>() {
				@Override
				public void success(List<Medication> result) {
					medication = result;
					setSpinner();
				}

				@Override
				public void error(Exception e) {
					Log.e(TAG, "Error logging in via OAuth or getting data.", e);
					Toast.makeText(getApplicationContext(), "Error connecting with the cloud!", Toast.LENGTH_LONG).show();
				}
			}
		);
	}
	
	private void saveCheckinInTheCloud() {
		final CheckinServiceApi api = CheckinService.init(getString(R.string.server_url));
		
		CallableTask.invoke(
			new Callable<CheckIn>() {
				@Override
				public CheckIn call() throws Exception {
					return api.addCheckin(checkin);
				}
			}, 
			new TaskCallback<CheckIn>() {
				@Override
				public void success(CheckIn result) {
					Log.d(TAG, "saved: " + result);
					Toast.makeText(getApplicationContext(), "Check-in successfully saved!", Toast.LENGTH_LONG).show();
				}

				@Override
				public void error(Exception e) {
					Log.e(TAG, "Error logging in via OAuth or getting data.", e);
					Toast.makeText(getApplicationContext(), "Error connecting with the cloud!", Toast.LENGTH_LONG).show();
				}
			}
		);
	}
	@Override
	protected void onStop() {
		super.onStop();
		Log.d(TAG, "onStop _________________");
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		Log.d(TAG, "onDestroy _________________");
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		Log.d(TAG, "onPause _________________");
	}
	
	@Override
	protected void onRestart() {
		super.onRestart();
		Log.d(TAG, "onRestart _________________");
		Log.d(TAG, "patientId=" + patientName );
	}
	
	@Override
	protected void onStart() {
		super.onRestart();
		Log.d(TAG, "onStart _________________");
		Log.d(TAG, "patientName=" + patientName );
		repo.dumpProperties();
	}
	
}
