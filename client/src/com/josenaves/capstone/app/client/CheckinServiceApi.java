package com.josenaves.capstone.app.client;

import java.util.Collection;

import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;

import com.josenaves.capstone.app.model.CheckIn;
import com.josenaves.capstone.app.model.CheckinFlat;
import com.josenaves.capstone.app.model.Medication;

public interface CheckinServiceApi {
	// The path where we expect the CheckinService to live
	public static final String CHECKIN_PATH = "/checkin";
	
	public static final String TOKEN_PATH = "/oauth/token";

	public Collection<CheckIn> getCheckinList();
	
	@GET(CHECKIN_PATH + "/{id}")
	public CheckIn getCheckin(@Path("id") long id);
	
	@GET(CHECKIN_PATH + "/{id}/medication")
	public Medication getCheckinMedication(@Path("id") long id);
	
	@POST(CHECKIN_PATH)
	public CheckIn addCheckin(@Body CheckinFlat flatCheckin);
}
