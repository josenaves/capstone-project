package com.josenaves.capstone.app.client;

import java.util.List;

import retrofit.http.GET;
import retrofit.http.Path;

import com.josenaves.capstone.app.model.Medication;

/**
 * This interface defines an API for a MedicationService.  The interface is used to provide a 
 * contract for client/server interactions. 
 * 
 * The interface is annotated with Retrofit annotations so that clients can automatically 
 * convert the object.
 * 
 * @author josenaves
 */
public interface MedicationServiceApi {
	
	// The path where we expect the MedicationService to live
	public static final String MEDICATION_PATH = "/medication";
	
	public static final String TOKEN_PATH = "/oauth/token";

	@GET(MEDICATION_PATH)
	public List<Medication> getMedicationList();
	
	@GET(MEDICATION_PATH + "/{id}")
	public Medication getMedication(@Path("id") long id);
	
}
