package com.josenaves.capstone.app.client;

import com.josenaves.capstone.app.async.EasyHttpClient;
import com.josenaves.capstone.app.async.SecuredRestBuilder;

import retrofit.RestAdapter.LogLevel;
import retrofit.client.ApacheClient;

public final class DoctorService {

	private static final String USERNAME2 = "user0";
	private static final String PASSWORD = "pass";
	private static final String CLIENT_ID = "mobile";

	private static DoctorServiceApi doctorService;

	public static synchronized DoctorServiceApi init(String server) {
		
		doctorService = new SecuredRestBuilder()
			.setClient(new ApacheClient(new EasyHttpClient()))
			.setLoginEndpoint(server + DoctorServiceApi.TOKEN_PATH)
			.setEndpoint(server).setLogLevel(LogLevel.FULL)
			.setUsername(USERNAME2).setPassword(PASSWORD).setClientId(CLIENT_ID)
			.build()
			.create(DoctorServiceApi.class);

		return doctorService;
	}
}
