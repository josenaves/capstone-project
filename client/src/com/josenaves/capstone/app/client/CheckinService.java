package com.josenaves.capstone.app.client;

import retrofit.RestAdapter.LogLevel;
import retrofit.client.ApacheClient;

import com.josenaves.capstone.app.async.EasyHttpClient;
import com.josenaves.capstone.app.async.SecuredRestBuilder;

public final class CheckinService {

	private static final String USERNAME2 = "user0";
	private static final String PASSWORD = "pass";
	private static final String CLIENT_ID = "mobile";

	private static CheckinServiceApi checkinService;

	public static synchronized CheckinServiceApi init(String server) {
		
		checkinService = new SecuredRestBuilder()
			.setClient(new ApacheClient(new EasyHttpClient()))
			.setLoginEndpoint(server + CheckinServiceApi.TOKEN_PATH)
			.setEndpoint(server).setLogLevel(LogLevel.FULL)
			.setUsername(USERNAME2).setPassword(PASSWORD).setClientId(CLIENT_ID)
			.build()
			.create(CheckinServiceApi.class);

		return checkinService;
	}
}
