package com.josenaves.capstone.app.client;

import java.util.Collection;
import java.util.List;

import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;
import retrofit.http.Query;

import com.josenaves.capstone.app.model.CheckIn;
import com.josenaves.capstone.app.model.Doctor;
import com.josenaves.capstone.app.model.Medication;
import com.josenaves.capstone.app.model.Patient;

public interface PatientServiceApi {
	
	// The path where we expect the PatientService to live
	public static final String PATIENT_PATH = "/patient";
	
	public static final String TOKEN_PATH = "/oauth/token";

	// The path to search patient by firstName
	public static final String PATIENT_FIRST_NAME_SEARCH_PATH = PATIENT_PATH + "/search/findByFirstName";
	
	public static final String FIRST_NAME_PARAMETER = "firstName";

	@GET(PATIENT_PATH)
	public Collection<Patient> getPatientList();
	
	@GET(PATIENT_PATH + "/{id}")
	public Patient getPatient(@Path("id") long id);

	@GET(PATIENT_PATH + "/{patientId}/doctor")
	public Doctor getPatientDoctor(@Path("patientId") long patientId);
	
	@GET(PATIENT_PATH + "/{patientId}/checkin")
	public List<CheckIn> getPatientCheckins(@Path("patientId") long patientId);
	
	@POST(PATIENT_PATH + "/{id}/checkin")
	public Patient addCheckin(@Path("id") long patientId, @Body CheckIn checkin);

	@GET(PATIENT_PATH + "/{id}/medication")
	public List<Medication> getPatientMedication(@Path("id") long patientId);
	
	@DELETE(PATIENT_PATH + "/{id}/medication")
	public Void deleteMedication(@Path("id") long patientId);
	
	@PUT(PATIENT_PATH + "/{id}/medication")
	public Void updateMedication(@Path("id") long patientId, @Body List<Medication> medication);

	@GET(PATIENT_FIRST_NAME_SEARCH_PATH)
	public List<Patient> findByFirstName(@Query(FIRST_NAME_PARAMETER) String firstName);
	
}