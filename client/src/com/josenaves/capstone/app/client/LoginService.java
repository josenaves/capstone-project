package com.josenaves.capstone.app.client;

import com.josenaves.capstone.app.async.EasyHttpClient;
import com.josenaves.capstone.app.async.SecuredRestBuilder;

import retrofit.RestAdapter.LogLevel;
import retrofit.client.ApacheClient;

public final class LoginService {

	private static final String USERNAME2 = "user0";
	private static final String PASSWORD = "pass";
	private static final String CLIENT_ID = "mobile";

	private static LoginServiceApi loginService;

	public static synchronized LoginServiceApi init(String server) {
		
		loginService = new SecuredRestBuilder()
			.setClient(new ApacheClient(new EasyHttpClient()))
			.setLoginEndpoint(server + LoginServiceApi.TOKEN_PATH)
			.setEndpoint(server).setLogLevel(LogLevel.FULL)
			.setUsername(USERNAME2).setPassword(PASSWORD).setClientId(CLIENT_ID)
			.build()
			.create(LoginServiceApi.class);

		return loginService;
	}
}
