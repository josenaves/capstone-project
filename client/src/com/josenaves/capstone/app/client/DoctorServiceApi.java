package com.josenaves.capstone.app.client;

import java.util.Collection;
import java.util.List;

import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;

import com.josenaves.capstone.app.model.Doctor;
import com.josenaves.capstone.app.model.Patient;

/**
 * This interface defines an API for a DoctorService.  The interface is used to provide a 
 * contract for client/server interactions. 
 * 
 * The interface is annotated with Retrofit annotations so that clients can automatically 
 * convert the object.
 * 
 * @author josenaves
 */
public interface DoctorServiceApi {
	
	// The path where we expect the DoctorService to live
	public static final String DOCTOR_PATH = "/doctor";
	
	public static final String TOKEN_PATH = "/oauth/token";

	@GET(DOCTOR_PATH)
	public Collection<Doctor> getDoctorList();
	
	@GET(DOCTOR_PATH + "/{id}")
	public Doctor getDoctor(@Path("id") long id);
	
	@GET(DOCTOR_PATH + "/{id}/patients")
	public List<Patient> getDoctorPatients(@Path("id") long id);
		
	@POST(DOCTOR_PATH)
	public Void addDoctor(@Body Doctor doctor);
	
	@POST(DOCTOR_PATH + "{doctorId}/patient/{patientId}")
	public Void addPatient(@Path("doctorId") long doctorId, @Path("patientId") long patientId);
	
	@DELETE(DOCTOR_PATH + "/{id}")
	public Void deleteDoctor(@Path("id") long id);
}
