package com.josenaves.capstone.app.client;

import com.josenaves.capstone.app.async.EasyHttpClient;
import com.josenaves.capstone.app.async.SecuredRestBuilder;

import retrofit.RestAdapter.LogLevel;
import retrofit.client.ApacheClient;

public final class PatientService {

	private static final String USERNAME2 = "user0";
	private static final String PASSWORD = "pass";
	private static final String CLIENT_ID = "mobile";

	private static PatientServiceApi patientService;

	public static synchronized PatientServiceApi init(String server) {
		
		patientService = new SecuredRestBuilder()
			.setClient(new ApacheClient(new EasyHttpClient()))
			.setLoginEndpoint(server + PatientServiceApi.TOKEN_PATH)
			.setEndpoint(server).setLogLevel(LogLevel.FULL)
			.setUsername(USERNAME2).setPassword(PASSWORD).setClientId(CLIENT_ID)
			.build()
			.create(PatientServiceApi.class);

		return patientService;
	}
}
