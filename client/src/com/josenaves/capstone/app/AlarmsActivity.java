package com.josenaves.capstone.app;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TimePicker;
import android.widget.Toast;

import com.josenaves.capstone.app.model.Alarms;
import com.josenaves.capstone.app.util.ApplicationRepository;

public class AlarmsActivity extends Activity implements OnClickListener, OnTimeSetListener {

	private static final String TAG = AlarmsActivity.class.getSimpleName();

	private EditText editTextAlarm1;
	private EditText editTextAlarm2;
	private EditText editTextAlarm3;
	private EditText editTextAlarm4;

	private static int editTextSelected;
	
	private RadioGroup radioGroupAlarmStyle;
	
	private Button buttonSave;
	
	private ApplicationRepository repo;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Log.d(TAG, "onCreate");
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_alarms);
		
		editTextAlarm1 = (EditText) findViewById(R.id.edit_time_alarm_1);
		editTextAlarm1.setOnClickListener(this);
		
		editTextAlarm2 = (EditText) findViewById(R.id.edit_time_alarm_2);
		editTextAlarm2.setOnClickListener(this);

		editTextAlarm3 = (EditText) findViewById(R.id.edit_time_alarm_3);
		editTextAlarm3.setOnClickListener(this);

		editTextAlarm4 = (EditText) findViewById(R.id.edit_time_alarm_4);
		editTextAlarm4.setOnClickListener(this);
		
		radioGroupAlarmStyle = (RadioGroup) findViewById(R.id.radio_group_alarm);
		
		buttonSave = (Button) findViewById(R.id.button_alarms_save);
		buttonSave.setOnClickListener(this);
		
		// show the back button
		getActionBar().setHomeButtonEnabled(true);
		
		repo = new ApplicationRepository(this);
		
		restoreAlarms();
	}

	private void showTimePickerDialog(View view) {
		editTextSelected = view.getId();
	    DialogFragment newFragment = new TimePickerFragment(this);
	    newFragment.show(getFragmentManager(), "timepicker" );
	}
	
	@Override
	public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
		String time = hourOfDay + ":" + (minute < 10 ? "0" + minute : minute);
		
		switch (editTextSelected) {
			case R.id.edit_time_alarm_1:
				editTextAlarm1.setText(time);
				break;
			
			case R.id.edit_time_alarm_2:
				editTextAlarm2.setText(time);
				break;
		
			case R.id.edit_time_alarm_3:
				editTextAlarm3.setText(time);
				break;
				
			case R.id.edit_time_alarm_4:
				editTextAlarm4.setText(time);
				break;
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == android.R.id.home) {
	        NavUtils.navigateUpFromSameTask(this);
	        return true;
		}
		
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onClick(View view) {
		if (view.getId() == R.id.button_alarms_save) {
			
			String[] alarms = new String[4];
			alarms[0] = editTextAlarm1.getText().toString();
			alarms[1] = editTextAlarm2.getText().toString();
			alarms[2] = editTextAlarm3.getText().toString();
			alarms[3] = editTextAlarm4.getText().toString();
			
			repo.saveAlarms(new Alarms(repo.getUserId(), alarms, radioGroupAlarmStyle.getCheckedRadioButtonId())); 
			
			Toast.makeText(this, "Alarms saved", Toast.LENGTH_SHORT).show();
			finish();
		}
		else {
			showTimePickerDialog(view);
		}
	}
	
	private void restoreAlarms() {
		Alarms alarms = repo.restoreAlarms(repo.getUserId());
		String[] savedAlarms = alarms.getAlarms();
		
		editTextAlarm1.setText(savedAlarms[0]);
		editTextAlarm2.setText(savedAlarms[1]);
		editTextAlarm3.setText(savedAlarms[2]);
		editTextAlarm4.setText(savedAlarms[3]);
		
		radioGroupAlarmStyle.check(alarms.getStyle());
	}	
}