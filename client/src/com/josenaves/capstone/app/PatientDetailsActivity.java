package com.josenaves.capstone.app;

import java.util.List;
import java.util.concurrent.Callable;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.josenaves.capstone.app.adapter.CheckinAdapter;
import com.josenaves.capstone.app.async.CallableTask;
import com.josenaves.capstone.app.async.TaskCallback;
import com.josenaves.capstone.app.client.CheckinService;
import com.josenaves.capstone.app.client.CheckinServiceApi;
import com.josenaves.capstone.app.client.PatientService;
import com.josenaves.capstone.app.client.PatientServiceApi;
import com.josenaves.capstone.app.model.CheckIn;
import com.josenaves.capstone.app.model.Medication;
import com.josenaves.capstone.app.util.ApplicationRepository;
import com.josenaves.capstone.app.util.CapstoneUtils;

public class PatientDetailsActivity extends Activity {

	private static final String TAG = PatientDetailsActivity.class.getSimpleName();
	
	private long patientId = -1;
	private String patientName;
	
	private TextView textPatient;
	private TextView textMedications;
	
	private ListView listView;
	
	private List<CheckIn> checkins;
	
	private ApplicationRepository repo;
	
	private static final long REFRESH_TIME = 60000;
	private Handler handler;

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_patient_details);
		Log.d(TAG, "onCreate _________________");
		
		repo = new ApplicationRepository(this);
		
		patientId = CapstoneUtils.getPatientId(this);
		patientName = repo.getPatientName();
		
		textPatient = (TextView)findViewById(R.id.text_patient_details_name);
		textPatient.setText(patientName);
		
		textMedications = (TextView)findViewById(R.id.text_patient_details_medications);
		
		repo = new ApplicationRepository(this);
		listView = (ListView) findViewById(R.id.list_patient_checkins);
		
		setAutoRefresh();

		getPatientMedicationsInTheCloud();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		Log.d(TAG, "onCreateOptionsMenu _________________");
		
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.patient_details, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_patient_medication) {
			startActivity(new Intent(this, PatientMedicationActivity.class));
			return true;
		}
		else if (id == R.id.action_refresh) {
			Toast.makeText(getApplicationContext(), "Refreshing...", Toast.LENGTH_SHORT).show();
			getCheckinsInTheCloud();
		}
		return super.onOptionsItemSelected(item);
	}
	
	private void getPatientMedicationsInTheCloud() {
		final PatientServiceApi api = PatientService.init(getString(R.string.server_url));

		CallableTask.invoke(
				
			new Callable<List<Medication>>() {
				@Override
				public List<Medication> call() throws Exception {
					return api.getPatientMedication(patientId);
				}
			}, 
			
			new TaskCallback<List<Medication>>() {
				@Override
				public void success(List<Medication> result) {
					Log.d(TAG, result.toString());
					textMedications.setText("Medications: " + getMedicationListString(result));
					
					getCheckinsInTheCloud();
				}

				@Override
				public void error(Exception e) {
					Log.e(TAG, "Error logging in via OAuth.", e);
					Toast.makeText(getApplicationContext(), "Error connecting with the cloud!", Toast.LENGTH_LONG).show();
					
					getCheckinsInTheCloud();
				}
			});
	}

	private void getCheckinsInTheCloud() {
		
		final PatientServiceApi api = PatientService.init(getString(R.string.server_url));
		CallableTask.invoke(
				
			new Callable<List<CheckIn>>() {
				@Override
				public List<CheckIn> call() throws Exception {
					return api.getPatientCheckins(patientId);
				}
			}, 
			
			new TaskCallback<List<CheckIn>>() {
				@Override
				public void success(List<CheckIn> result) {
					Log.d(TAG, result.toString());
					checkins = result;
					
					// create the adapter to convert the list to views
					CheckinAdapter adapter = new CheckinAdapter(PatientDetailsActivity.this, checkins);
					
					// attach the adapter to a listview
					PatientDetailsActivity.this.listView.setAdapter(adapter);
					
					getCheckinsDetailsInTheCloud();
				}

				@Override
				public void error(Exception e) {
					Log.e(TAG, "Error logging in via OAuth.", e);
					Toast.makeText(getApplicationContext(), "Error connecting with the cloud!", Toast.LENGTH_LONG).show();
					
					getCheckinsDetailsInTheCloud();
				}
			});
	}
	
	private void getCheckinsDetailsInTheCloud() {
		final CheckinServiceApi api = CheckinService.init(getString(R.string.server_url));

		for (final CheckIn checkin : checkins) {
			// get medication
			CallableTask.invoke(
					
				new Callable<Medication>() {
					@Override
					public Medication call() throws Exception {
						return api.getCheckinMedication(checkin.getId());
					}
				}, 
				
				new TaskCallback<Medication>() {
					@Override
					public void success(Medication result) {
						Log.d(TAG, result.toString());
						checkin.setMedication(result);
						Log.d(TAG, checkin.toString());
					}

					@Override
					public void error(Exception e) {
						Log.e(TAG, "Error logging in via OAuth.", e);
						Toast.makeText(getApplicationContext(), "Error connecting with the cloud!", Toast.LENGTH_LONG).show();
					}
				});
		}
	}

	private String getMedicationListString(List<Medication> medications) {
		String medicationList = "";
		if (!medications.isEmpty()) {
			for (Medication medication : medications) {
				medicationList += medication.getName() + ", ";
			}
			medicationList = medicationList.substring(0, medicationList.length()-2);
		}
		return medicationList.equals("") ? "none" : medicationList;
	}
	
	private final Runnable refreshRunnable = new Runnable() {
		@Override
		public void run() {
			Toast.makeText(getApplicationContext(), "Refreshing...", Toast.LENGTH_SHORT).show();
			getCheckinsInTheCloud();
			PatientDetailsActivity.this.handler.postDelayed(refreshRunnable, REFRESH_TIME);            
		}
	};

	private void setAutoRefresh() {
		this.handler = new Handler();
		handler.postDelayed(refreshRunnable, REFRESH_TIME);
	}
	@Override
	protected void onStop() {
		super.onStop();
		Log.d(TAG, "onStop _________________");
	    handler.removeCallbacks(refreshRunnable);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		Log.d(TAG, "onDestroy _________________");
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		Log.d(TAG, "onPause _________________");
	}
	
	@Override
	protected void onRestart() {
		super.onRestart();
		Log.d(TAG, "onRestart _________________");
	}
	
	@Override
	protected void onStart() {
		super.onRestart();
		Log.d(TAG, "onStart _________________");
		
		repo.dumpProperties();
	}

}
