package com.josenaves.capstone.app;

import java.util.concurrent.Callable;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.josenaves.capstone.app.async.CallableTask;
import com.josenaves.capstone.app.async.TaskCallback;
import com.josenaves.capstone.app.client.LoginService;
import com.josenaves.capstone.app.client.LoginServiceApi;
import com.josenaves.capstone.app.model.Login;
import com.josenaves.capstone.app.util.ApplicationRepository;

public class LoginActivity extends Activity implements OnClickListener {

	private static final String TAG = LoginActivity.class.getSimpleName();
	
	private Button buttonLogin;
	private EditText editUsername;
	private EditText editPassword;
	
	private CheckBox checkSaveCredentials;
	private MenuItem menuForgetCredentials;
	
	private Login savedLogin;
	
	private ApplicationRepository repo;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Log.d(TAG, "onCreate _________________");
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		
		this.checkSaveCredentials = (CheckBox) findViewById(R.id.check_remember);
		this.editUsername = (EditText) findViewById(R.id.edit_username);
		this.editPassword = (EditText) findViewById(R.id.edit_password);
		this.buttonLogin = (Button) findViewById(R.id.button_login);
		
		buttonLogin.setOnClickListener(this);
		
		repo = new ApplicationRepository(this);
		
		// try to get previously saved credentials
		savedLogin = repo.restoreCredentials();
	
		if (savedLogin != null) {
			Log.d(TAG, "Credentials restored...");

			this.checkSaveCredentials.setVisibility(View.INVISIBLE);
			this.editUsername.setText(savedLogin.getUsername());
			this.editPassword.setText(savedLogin.getPassword());
		}
		else {
			this.checkSaveCredentials.setVisibility(View.VISIBLE);
		}
	}
	
	@Override
	public void onClick(View v) {
		final String username = editUsername.getText().toString();
		final String password = editPassword.getText().toString();
		
		final Login credentials;
		if (savedLogin == null) {
			Log.d(TAG, "Credentials not saved, applying user entered ones to login...");
			credentials = new Login(username, password, 0);
		}
		else if (savedLogin.getUsername().equals(username) && savedLogin.getPassword().equals(password)){
			Log.d(TAG, "Applying saved credentials to login...");
			credentials = savedLogin;
		}
		else {
			Log.d(TAG, "Another user is trying to login...");
			credentials = new Login(username, password, 0);
		}
		
		authenticateUserInTheCloud(username, password, credentials);
	}

	private void authenticateUserInTheCloud(final String username, final String password, final Login credentials) {
		
		final LoginServiceApi api = LoginService.init(getString(R.string.server_url));
			
		CallableTask.invoke(
			
			new Callable<Login>() {
				@Override
				public Login call() throws Exception {
					return api.authenticate(credentials);
				}
			}, 
			new TaskCallback<Login>() {
				@Override
				public void success(Login result) {
					if (result == null) {
						Toast.makeText(getApplicationContext(), "Access denied!", Toast.LENGTH_LONG).show();
					}
					else {
						Intent intent;
						Log.d(TAG, result.toString());
						if (result.getRole() == Login.ROLE_DOCTOR) {
							Toast.makeText(getApplicationContext(), "Access granted, Doc !", Toast.LENGTH_LONG).show();
							intent = new Intent(getApplicationContext(), DoctorActivity.class);
						}
						else {
							Toast.makeText(getApplicationContext(), "Access granted, patient! ", Toast.LENGTH_LONG).show();
							intent = new Intent(getApplicationContext(), PatientActivity.class);
						}
						
						if (checkSaveCredentials.isShown() && isCheckedRememberCredentials()) {
							Log.d(TAG, "Saving credentials...");
							repo.saveCredentials(username, password);
						}
						
						// save the logged user (patient or doctor) in the sharedPreferences
						repo.saveUserId(result.getId());
						
						// start the correct activity
						startActivity(intent);
					}
				}

				@Override
				public void error(Exception e) {
					Log.e(TAG, "Error logging in via OAuth.", e);
					Toast.makeText(getApplicationContext(), "Error connecting with the cloud!", Toast.LENGTH_LONG).show();
				}
		});
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		Log.d(TAG, "onCreateOptionsMenu _________________");
		
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		
		// set visibility of Forget menu
		this.menuForgetCredentials = menu.findItem(R.id.action_forget);
		if (savedLogin != null) {
			this.menuForgetCredentials.setEnabled(true);
		}
		else {
			this.menuForgetCredentials.setEnabled(false);
		}
		
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_forget) {
			repo.clearCredentials();
			checkSaveCredentials.setVisibility(View.VISIBLE);
			checkSaveCredentials.setChecked(false);
			Toast.makeText(this, "Your credentials will not be saved anymore.", Toast.LENGTH_SHORT).show();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	private boolean isCheckedRememberCredentials() {
		return checkSaveCredentials.isChecked();
	}
	
	
	@Override
	protected void onStop() {
		super.onStop();
		Log.d(TAG, "onStop _________________");
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		Log.d(TAG, "onDestroy _________________");
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		Log.d(TAG, "onPause _________________");
	}
	
	@Override
	protected void onRestart() {
		super.onRestart();
		Log.d(TAG, "onRestart _________________");
	}
	
	@Override
	protected void onStart() {
		super.onRestart();
		Log.d(TAG, "onStart _________________");
	}
}
