package com.josenaves.capstone.app;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.josenaves.capstone.app.adapter.MedicationAdapter;
import com.josenaves.capstone.app.async.CallableTask;
import com.josenaves.capstone.app.async.TaskCallback;
import com.josenaves.capstone.app.client.MedicationService;
import com.josenaves.capstone.app.client.MedicationServiceApi;
import com.josenaves.capstone.app.client.PatientService;
import com.josenaves.capstone.app.client.PatientServiceApi;
import com.josenaves.capstone.app.model.Medication;
import com.josenaves.capstone.app.util.ApplicationRepository;
import com.josenaves.capstone.app.util.CapstoneUtils;

public class PatientMedicationActivity extends Activity {

	private static final String TAG = PatientMedicationActivity.class.getSimpleName();
	
	private ListView listView;
	private Button buttonUpdate;
	
	private long patientId;
	
	private List<Medication> patientMedication;
	private MedicationAdapter adapter;
	
	private ApplicationRepository repo;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		Log.d(TAG, "onCreate _________________");
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_patient_medication);
		
		repo = new ApplicationRepository(this);
		
		patientId = CapstoneUtils.getPatientId(this);
		
		listView = (ListView) findViewById(R.id.list_patient_medication);
		listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
		
		buttonUpdate = (Button) findViewById(R.id.button_update_medication);
		buttonUpdate.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				updatePatientMedicationInTheCloud();
			}
		});
		
		getPatientMedicationInTheCloud();
	}
	
	
	private void getMedicationInTheCloud() {
		final MedicationServiceApi api = MedicationService.init(getString(R.string.server_url));
		
		CallableTask.invoke(
			new Callable<List<Medication>>() {
				@Override
				public List<Medication> call() throws Exception {
					return api.getMedicationList();
				}
			}, 
			new TaskCallback<List<Medication>>() {
				@Override
				public void success(List<Medication> result) {
					// create the adapter to convert the list to views
					adapter = new MedicationAdapter(PatientMedicationActivity.this, result, patientMedication);
					
					// attach the adapter to a listview
					listView.setAdapter(adapter);
				}

				@Override
				public void error(Exception e) {
					Log.e(TAG, "Error logging in via OAuth or getting data.", e);
					Toast.makeText(getApplicationContext(), "Error connecting with the cloud!", Toast.LENGTH_LONG).show();
				}
			}
		);
	}
	
	private void getPatientMedicationInTheCloud() {
		final PatientServiceApi api = PatientService.init(getString(R.string.server_url));
		
		CallableTask.invoke(
			new Callable<List<Medication>>() {
				@Override
				public List<Medication> call() throws Exception {
					return api.getPatientMedication(patientId);
				}
			}, 
			new TaskCallback<List<Medication>>() {
				@Override
				public void success(List<Medication> result) {
					patientMedication = result;
					
					getMedicationInTheCloud();
				}

				@Override
				public void error(Exception e) {
					Log.e(TAG, "Error logging in via OAuth or getting data.", e);
					Toast.makeText(getApplicationContext(), "Error connecting with the cloud!", Toast.LENGTH_LONG).show();
				}
			}
		);
	}
	
	private void updatePatientMedicationInTheCloud() {
		final PatientServiceApi api = PatientService.init(getString(R.string.server_url));
		
		CallableTask.invoke(
			new Callable<Void>() {
				@Override
				public Void call() throws Exception {
					SparseBooleanArray checked = listView.getCheckedItemPositions();
					List<Medication> selectedItems = new ArrayList<Medication>();
					for (int i = 0; i < checked.size(); i++) {
						// Item position in adapter
						int position = checked.keyAt(i);
						if (checked.valueAt(i)) selectedItems.add(adapter.getItem(position));
					}
					
					// now call the api method for save the collection of medication
					api.updateMedication(patientId, selectedItems);
					return null;
				}
			}, 
			new TaskCallback<Void>() {
				@Override
				public void success(Void result) {
					Log.i(TAG, "Sucess! Medication udpated!");
					Toast.makeText(getApplicationContext(), "Patient's medication updated!", Toast.LENGTH_LONG).show();
				}

				@Override
				public void error(Exception e) {
					Log.e(TAG, "Error logging in via OAuth or getting data.", e);
					Toast.makeText(getApplicationContext(), "Error connecting with the cloud!", Toast.LENGTH_LONG).show();
				}
			}
		);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		Log.d(TAG, "onCreateOptionsMenu _________________");
		
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.patient_medication, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_alarms) {
			startActivity(new Intent(this, AlarmsActivity.class));
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	protected void onStop() {
		super.onStop();
		Log.d(TAG, "onStop _________________");
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		Log.d(TAG, "onDestroy _________________");
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		Log.d(TAG, "onPause _________________");
	}
	
	@Override
	protected void onRestart() {
		super.onRestart();
		Log.d(TAG, "onRestart _________________");
		Log.d(TAG, "patientId=" + patientId );
	}
	
	@Override
	protected void onStart() {
		super.onRestart();
		Log.d(TAG, "onStart _________________");
		Log.d(TAG, "patientId=" + patientId );
		repo.dumpProperties();
	}

}
