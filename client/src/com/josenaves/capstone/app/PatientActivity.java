package com.josenaves.capstone.app;

import java.util.List;
import java.util.concurrent.Callable;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.josenaves.capstone.app.async.CallableTask;
import com.josenaves.capstone.app.async.TaskCallback;
import com.josenaves.capstone.app.client.PatientService;
import com.josenaves.capstone.app.client.PatientServiceApi;
import com.josenaves.capstone.app.model.Doctor;
import com.josenaves.capstone.app.model.Medication;
import com.josenaves.capstone.app.model.Patient;
import com.josenaves.capstone.app.util.ApplicationRepository;

public class PatientActivity extends Activity implements OnClickListener {

	private static final String TAG = PatientActivity.class.getSimpleName();
	
	private static final String MEDICATION_TITLE = "Medication: ";
	private static final String DOCTOR_TITLE = "Doctor: ";
	
	private long patientId = -1;
	private Patient patient;
	
	private TextView textPatient;
	private TextView textMedication;
	private TextView textDoctor;
	
	private Button checkinButton;
	
	private ApplicationRepository repo;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_patient);
		
		Log.d(TAG, "onCreate _________________");
		
		checkinButton = (Button) findViewById(R.id.button_checkin);
		checkinButton.setOnClickListener(this);
		
		repo = new ApplicationRepository(this);
		
		getPatientInTheCloud();
		getPatientMedicationInTheCloud();
		getPatientDoctorInTheCloud();
	}


	@Override
	public void onClick(View view) {
		repo.savePatientData(patient);
		Intent intent = new Intent(this, CheckinActivity.class);
		startActivity(intent);
	}
	
	
	private void getPatientInTheCloud() {
		
		patientId = repo.getUserId();
		
		// id will be used to get patient details from the cloud
		Log.d(TAG, "patient id = " + patientId);

		final PatientServiceApi api = PatientService.init(getString(R.string.server_url));
		CallableTask.invoke(
			new Callable<Patient>() {
				@Override
				public Patient call() throws Exception {
					return api.getPatient(patientId);
				}
			},
			new TaskCallback<Patient>() {
				@Override
				public void success(Patient patient) {
					if (patient == null) {
						Log.e(TAG, "No patient returned for id " + patientId);
					}
					else {
						// fill the fields
						Log.d(TAG, patient.toString());
						
						textPatient = (TextView)findViewById(R.id.text_patient);
						textPatient.setText(patient.getFirstName() + " " + patient.getLastName());
						PatientActivity.this.patient = patient;
					}
				}

				@Override
				public void error(Exception e) {
					Log.e(TAG, "Error: could not get patient data", e);
					Toast.makeText(getApplicationContext(), "Error getting medication from the cloud!", Toast.LENGTH_LONG).show();
				}
			}
		);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		Log.d(TAG, "onCreateOptionsMenu _________________");
		
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.patient, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_alarms) {
			startActivity(new Intent(this, AlarmsActivity.class));
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private void getPatientMedicationInTheCloud() {
		
		patientId = repo.getUserId();
		
		// id will be used to get patient details from the cloud
		Log.d(TAG, "patient id = " + patientId);

		final PatientServiceApi api = PatientService.init(getString(R.string.server_url));
		CallableTask.invoke(
			new Callable<List<Medication>>() {
				@Override
				public List<Medication> call() throws Exception {
					return api.getPatientMedication(patientId);
				}
			},
			new TaskCallback<List<Medication>>() {
				@Override
				public void success(List<Medication> medications) {
					if (medications == null) {
						Log.e(TAG, "No medications returned for id " + patientId);
					}
					else {
						// fill the fields
						Log.d(TAG, medications.toString());
						
						String medicationList = "";
						if (!medications.isEmpty()) {
							for (Medication medication : medications) {
								medicationList += medication.getName() + ", ";
							}
							medicationList = medicationList.substring(0, medicationList.length()-2);
						}
						
						textMedication = (TextView)findViewById(R.id.text_medication);
						textMedication.setText(MEDICATION_TITLE + medicationList);
					}
				}

				@Override
				public void error(Exception e) {
					Log.e(TAG, "Error: could not get medication data", e);
					Toast.makeText(getApplicationContext(), "Error getting medication from the cloud!", Toast.LENGTH_LONG).show();
				}
			}
		);
	}
	
	private void getPatientDoctorInTheCloud() {
		
		patientId = repo.getUserId();

		final PatientServiceApi api = PatientService.init(getString(R.string.server_url));
		CallableTask.invoke(
			new Callable<Doctor>() {
				@Override
				public Doctor call() throws Exception {
					return api.getPatientDoctor(patientId);
				}
			},
			new TaskCallback<Doctor>() {
				@Override
				public void success(Doctor doctor) {
					if (doctor == null) {
						Log.e(TAG, "No doctor returned for id " + patientId);
					}
					else {
						// fill the fields
						Log.d(TAG, doctor.toString());
						
						textDoctor = (TextView)findViewById(R.id.text_patient_doctor);
						textDoctor.setText(DOCTOR_TITLE + doctor.getFirstName() + " " + doctor.getLastName());
					}
				}

				@Override
				public void error(Exception e) {
					Log.e(TAG, "Error: could not get doctor data", e);
					Toast.makeText(getApplicationContext(), "Error getting doctor from the cloud!", Toast.LENGTH_LONG).show();
				}
			}
		);
	}
	
	@Override
	protected void onStop() {
		super.onStop();
		Log.d(TAG, "onStop _________________");
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		Log.d(TAG, "onDestroy _________________");
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		Log.d(TAG, "onPause _________________");
	}
	
	@Override
	protected void onRestart() {
		super.onRestart();
		Log.d(TAG, "onRestart _________________");
	}
	
	@Override
	protected void onStart() {
		super.onRestart();
		Log.d(TAG, "onStart _________________");
		
		repo.dumpProperties();
	}

}
