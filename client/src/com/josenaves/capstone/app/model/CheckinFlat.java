package com.josenaves.capstone.app.model;

/**
 * A simple object to represent a FLAT checkin. This is a workaround to fix the
 * save checkin problem.
 * 
 * @author josenaves
 */
public class CheckinFlat {

	private String datetime;

	public static final int CONTROLLED = 0;
	public static final int MODERATED = 1;
	public static final int SEVERE = 2;
	private int mouthPain;

	private long patientId;

	private long medicationId;

	private boolean taken;

	public static final int NO = 0;
	public static final int SOME = 1;
	public static final int CANT_EAT = 2;
	private int stopEating;

	public String getDatetime() {
		return datetime;
	}

	public void setDatetime(String datetime) {
		this.datetime = datetime;
	}

	public int getMouthPain() {
		return mouthPain;
	}

	public void setMouthPain(int mouthPain) {
		this.mouthPain = mouthPain;
	}

	public long getPatientId() {
		return patientId;
	}

	public void setPatientId(long patientId) {
		this.patientId = patientId;
	}

	public long getMedicationId() {
		return medicationId;
	}

	public void setMedicationId(long medicationId) {
		this.medicationId = medicationId;
	}

	public boolean isTaken() {
		return taken;
	}

	public void setTaken(boolean taken) {
		this.taken = taken;
	}

	public int getStopEating() {
		return stopEating;
	}

	public void setStopEating(int stopEating) {
		this.stopEating = stopEating;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((datetime == null) ? 0 : datetime.hashCode());
		result = prime * result + (int) (medicationId ^ (medicationId >>> 32));
		result = prime * result + mouthPain;
		result = prime * result + (int) (patientId ^ (patientId >>> 32));
		result = prime * result + stopEating;
		result = prime * result + (taken ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CheckinFlat other = (CheckinFlat) obj;
		if (datetime == null) {
			if (other.datetime != null)
				return false;
		} else if (!datetime.equals(other.datetime))
			return false;
		if (medicationId != other.medicationId)
			return false;
		if (mouthPain != other.mouthPain)
			return false;
		if (patientId != other.patientId)
			return false;
		if (stopEating != other.stopEating)
			return false;
		if (taken != other.taken)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "CheckinFlat [datetime=" + datetime + ", mouthPain=" + mouthPain
				+ ", patientId=" + patientId + ", medicationId=" + medicationId
				+ ", taken=" + taken + ", stopEating=" + stopEating + "]";
	}

}
