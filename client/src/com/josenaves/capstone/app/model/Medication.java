package com.josenaves.capstone.app.model;

import com.google.common.base.Objects;

public class Medication {

	private long id = -1;
	private String name;
	
	public Medication(){}
	
	public Medication(String name) {
		super();
		this.name = name;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Medication [id=" + id + ", name=" + name + "]";
	}

	/**
	 * Two medications will generate the same hashcode if they have exactly the
	 * same values for their names.
	 */
	@Override
	public int hashCode() {
		// Google Guava provides great utilities for hashing
		return Objects.hashCode(name);
	}

	/**
	 * Two medications are considered equal if they have exactly the same values for
	 * their fields.
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Medication) {
			Medication other = (Medication) obj;
			// Google Guava provides great utilities for equals too!
			return Objects.equal(name, other.getName());
		} else {
			return false;
		}
	}
}
