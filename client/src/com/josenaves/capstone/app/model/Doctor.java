package com.josenaves.capstone.app.model;

import java.util.HashSet;
import java.util.Set;

import com.google.common.base.Objects;

/**
 * A simple object to represent a doctor.
 * @author josenaves
 */
public class Doctor {
	
	private long id = -1;

	private String firstName;
	private String lastName;
	private long doctorLicenseId;
	
	private String userName;
	private String password;
	
	private Set<Patient> patients = new HashSet<Patient>();
	
	public Doctor() {
	}

	public Doctor(String firstName, String lastName, long doctorId, String username, String password) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.doctorLicenseId = doctorId;
		this.userName = username;
		this.password = password;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	public long getDoctorLicenseId() {
		return doctorLicenseId;
	}

	public void setDoctorLicenseId(long doctorLicenseId) {
		this.doctorLicenseId = doctorLicenseId;
	}

	public Set<Patient> getPatients() {
		return patients;
	}

	public void setPatients(Set<Patient> patients) {
		this.patients = patients;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * Two Doctors will generate the same hashcode if they have exactly the same values for their name.
	 */
	@Override
	public int hashCode() {
		// Google Guava provides great utilities for hashing
		return Objects.hashCode(firstName, lastName, doctorLicenseId, patients);
	}

	/**
	 * Two Doctors are considered equal if they have exactly the same values for
	 * their fields.
	 * 
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Doctor) {
			Doctor other = (Doctor) obj;
			// Google Guava provides great utilities for equals too!
			return Objects.equal(firstName, other.firstName) 
					&& Objects.equal(lastName, other.lastName)
					&& Objects.equal(doctorLicenseId, other.doctorLicenseId)
					&& Objects.equal(patients, other.patients);
		} 
		else {
			return false;
		}
	}

	@Override
	public String toString() {
		return "Doctor [id=" + id + ", firstName=" + firstName + ", lastName="
				+ lastName + ", doctorLicenseId=" + doctorLicenseId
				+ ", userName=" + userName + ", password=" + password
				+ ", patients=" + patients + "]";
	}
}
