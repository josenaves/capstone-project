package com.josenaves.capstone.app.model;

import java.io.Serializable;

public final class Alarms implements Serializable {

	private static final long serialVersionUID = -1508234457387016352L;
	
	private long patientId;
	private String[] alarms;
	private int style;
	
	public Alarms(long patientId, String[] alarms, int style) {
		this.patientId = patientId;
		this.alarms = alarms;
		this.style = style;
	}

	public long getPatientId() {
		return patientId;
	}

	public String[] getAlarms() {
		return alarms;
	}

	public int getStyle() {
		return style;
	}
}
