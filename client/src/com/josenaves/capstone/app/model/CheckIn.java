package com.josenaves.capstone.app.model;

import com.google.common.base.Objects;

/**
 * A simple object to represent a checkin.
 * @author josenaves
 */
public class CheckIn {

	private long id = -1;

	private String datetime;

	public static final int CONTROLLED = 0;
	public static final int MODERATED = 1;
	public static final int SEVERE = 2;
	private int mouthPain;
	
	private Patient patient;

	private Medication medication; 
	
	private boolean taken;

	public static final int NO = 0;
	public static final int SOME = 1;
	public static final int CANT_EAT = 2;
	private int stopEating;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Patient getPatient() {
		return patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}

	public String getDatetime() {
		return datetime;
	}

	public void setDatetime(String datetime) {
		this.datetime = datetime;
	}

	public int getMouthPain() {
		return mouthPain;
	}

	public void setMouthPain(int mouthPain) {
		this.mouthPain = mouthPain;
	}

	public int getStopEating() {
		return stopEating;
	}

	public void setStopEating(int stopEating) {
		this.stopEating = stopEating;
	}

	public Medication getMedication() {
		return medication;
	}

	public void setMedication(Medication medication) {
		this.medication = medication;
	}

	public boolean isTaken() {
		return taken;
	}

	public void setTaken(boolean taken) {
		this.taken = taken;
	}

	/**
	 * Two checkins will generate the same hashcode if they have exactly the
	 * same values for their patient and timestamp.
	 */
	@Override
	public int hashCode() {
		// Google Guava provides great utilities for hashing
		return Objects.hashCode(patient, datetime);
	}

	/**
	 * Two Doctors are considered equal if they have exactly the same values for
	 * their fields.
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof CheckIn) {
			CheckIn other = (CheckIn) obj;
			// Google Guava provides great utilities for equals too!
			return Objects.equal(patient, other.getPatient())
					&& Objects.equal(datetime, other.datetime);
		} 
		else {
			return false;
		}
	}

	@Override
	public String toString() {
		return "CheckIn [id=" + id + ", datetime=" + datetime + ", mouthPain="
				+ mouthPain + ", patient=" + patient + ", medication="
				+ medication + ", taken=" + taken + ", stopEating="
				+ stopEating + "]";
	}


}
