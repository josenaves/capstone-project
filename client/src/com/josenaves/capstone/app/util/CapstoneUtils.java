package com.josenaves.capstone.app.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public final class CapstoneUtils {
	
	public static long getPatientId(Activity activity) {
		ApplicationRepository repo = new ApplicationRepository(activity);
		long patientId;
		Intent intent = activity.getIntent();
		Bundle bundle = intent.getExtras();
		if (bundle != null) {
			patientId = (Long) bundle.get("PATIENT_ID");
		}
		else {
			patientId = repo.getPatientId();
		}
		return patientId;
	}

	public static String getSystemTime() {
		SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss", Locale.US);
		return df.format(Calendar.getInstance().getTime());
	}
	
}
