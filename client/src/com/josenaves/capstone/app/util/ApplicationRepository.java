package com.josenaves.capstone.app.util;

import java.util.Map;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

import com.josenaves.capstone.app.model.Alarms;
import com.josenaves.capstone.app.model.Login;
import com.josenaves.capstone.app.model.Patient;

public final class ApplicationRepository {
	
	private static final String TAG = ApplicationRepository.class.getSimpleName();

	private static final String SHARED_PREFS = "symptoms";
	
	private static final String USERNAME = "USER";
	private static final String PASSWORD = "PASSWORD";
	
	private static final String ALARM_1 = "ALARM_1";
	private static final String ALARM_2 = "ALARM_2";
	private static final String ALARM_3 = "ALARM_3";
	private static final String ALARM_4 = "ALARM_4";
	private static final String ALARM_STYLE = "ALARM_STYLE";
	
	/** The user using the app (logged) */
	private static final String USER_ID = "USER_ID";
	
	private static final String PATIENT_NAME = "PATIENT_NAME";
	private static final String PATIENT_ID   = "PATIENT_ID";

	
	private SharedPreferences sharedPreferences;
	
	public ApplicationRepository(Context context) {
		this.sharedPreferences = context.getSharedPreferences(ApplicationRepository.SHARED_PREFS, Context.MODE_PRIVATE);
	}
	
	public Login restoreCredentials() {
		Login login = null;
		if (sharedPreferences.contains(USERNAME)) {
			String username = sharedPreferences.getString(USERNAME, "");
			String password = sharedPreferences.getString(PASSWORD, "");
			login = new Login(username, password, 0);
		}
		return login;
	}
	
	
	public void saveCredentials(String username, String password) {
		// We need an Editor object to make preference changes.
		// All objects are from android.context.Context
	    Editor editor = sharedPreferences.edit();
	    editor.putString(USERNAME, username);
	    editor.putString(PASSWORD, password);
		// Commit the edits!
		editor.commit();
	}
	
	
	public void savePatientData(Patient patient) {
		// We need an Editor object to make preference changes.
		// All objects are from android.context.Context
	    Editor editor = sharedPreferences.edit();
	    editor.putString(PATIENT_NAME, patient.getFullName());
	    editor.putLong(PATIENT_ID, patient.getId());
		// Commit the edits!
		editor.commit();
	}
	
	public String getPatientName() {
		return sharedPreferences.getString(PATIENT_NAME, "");
	}
	
	public long getPatientId() {
		return sharedPreferences.getLong(PATIENT_ID, -1);
	}

	
	public void clearCredentials() {
		// We need an Editor object to make preference changes.
		// All objects are from android.context.Context
		Editor editor = sharedPreferences.edit();
		editor.remove(USERNAME);
		editor.remove(PASSWORD);		
	    editor.commit();
	}

	public void saveUserId(long id) {
		Editor editor = sharedPreferences.edit();
		editor.putLong(USER_ID, id);
		editor.commit();
	}
	
	public long getUserId() {
		long id = -1;
		if (sharedPreferences.contains(USER_ID)) {
			id = sharedPreferences.getLong(USER_ID, -1);
		}
		return id;
	}
	
	public void saveAlarms(Alarms alarms) {
		// We need an Editor object to make preference changes.
		// All objects are from android.context.Context
	    Editor editor = sharedPreferences.edit();
	    
	    String[] savedAlarms = alarms.getAlarms();
	    long patientId = alarms.getPatientId();
	    
	    editor.putString(patientId + "_" + ALARM_1, savedAlarms[0]);
	    editor.putString(patientId + "_" + ALARM_2, savedAlarms[1]);
	    editor.putString(patientId + "_" + ALARM_3, savedAlarms[2]);
	    editor.putString(patientId + "_" + ALARM_4, savedAlarms[3]);
	    
	    editor.putInt(patientId + "_" + ALARM_STYLE, alarms.getStyle());

	    // Commit the edits!
		editor.commit();
		
	}
	
	public Alarms restoreAlarms(long patientId) {
		String[] alarms = new String[4];
		alarms[0] = sharedPreferences.getString(patientId + "_" + ALARM_1, "");
		alarms[1] = sharedPreferences.getString(patientId + "_" + ALARM_2, "");
		alarms[2] = sharedPreferences.getString(patientId + "_" + ALARM_3, "");
		alarms[3] = sharedPreferences.getString(patientId + "_" + ALARM_4, "");
		int alarmStyle = sharedPreferences.getInt(patientId + "_" + ALARM_STYLE, -1);
		
		return new Alarms(patientId, alarms, alarmStyle);
	}
	
	public void dumpProperties() {
		Log.d(TAG, "Shared Properties ____");
		@SuppressWarnings("unchecked")
		Map<String, Object> properties = (Map<String, Object>) sharedPreferences.getAll();
		for (String key : properties.keySet()) {
			Log.d(TAG, "  " + key + ": [" + properties.get(key) + "]"); 
		}
		Log.d(TAG, "____ Shared Properties");

	}
}

