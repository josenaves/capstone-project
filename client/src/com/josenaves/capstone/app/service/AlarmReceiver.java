package com.josenaves.capstone.app.service;

import java.util.Calendar;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.josenaves.capstone.app.model.Alarms;

public class AlarmReceiver extends BroadcastReceiver {
 
    @Override
    public void onReceive(Context context, Intent intent) {
        // For our recurring task, we'll just display a message
        Toast.makeText(context, "I'm running", Toast.LENGTH_SHORT).show();
    }

    @SuppressLint("NewApi")
    public static void setAlarm(Context context, Calendar calendar, PendingIntent pendinIntent) {
		AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
			alarmManager.setExact(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendinIntent);
		}
		else {
			alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendinIntent);
		}
    }
    
    public static void cancelAlarms(Context context) {
//		SharedPreferences settings = context.getSharedPreferences(Alarms.PREFS_ALARMS, 0);
//		
//		int alarmStyle = settings.getInt("ALARM_STYLE", -1);
//
//		Alarms alarm1 = new Alarms(1, settings.getString("ALARM_1", ""), alarmStyle);
//		cancelAlarm(context, alarm1);
//		
//		Alarms alarm2 = new Alarms(2, settings.getString("ALARM_2", ""), alarmStyle);
//		cancelAlarm(context, alarm2);
//		
//		Alarms alarm3 = new Alarms(3, settings.getString("ALARM_3", ""), alarmStyle);
//		cancelAlarm(context, alarm3);
//
//		Alarms alarm4 = new Alarms(4, settings.getString("ALARM_4", ""), alarmStyle);
//		cancelAlarm(context, alarm4);

    }
    
    private static void cancelAlarm(Context context, Alarms alarm) {
		PendingIntent pendingIntent = createPendingIntent(context, alarm);
		AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		alarmManager.cancel(pendingIntent);
    }
    
    private static PendingIntent createPendingIntent(Context context, Alarms alarm) {
    	Intent intent = new Intent(context, AlarmService.class);
//    	intent.putExtra("ID", alarm.getId());
//    	intent.putExtra("TIME", alarm.getTime());
//    	intent.putExtra("TYPE", alarm.getType());
    	
//    	return PendingIntent.getService(context, alarm.getId(), intent, PendingIntent.FLAG_UPDATE_CURRENT);
    	return null;
    }
}
