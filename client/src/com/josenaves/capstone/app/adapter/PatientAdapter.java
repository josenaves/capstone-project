package com.josenaves.capstone.app.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.josenaves.capstone.app.R;
import com.josenaves.capstone.app.model.Patient;

public class PatientAdapter extends ArrayAdapter<Patient> {
	
    public PatientAdapter(Context context, List<Patient> patients) {
        super(context, 0, patients);
     }
    
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
       // Get the data item for this position
       Patient patient = getItem(position);
       
       // Check if an existing view is being reused, otherwise inflate the view
       if (convertView == null) {
          convertView = LayoutInflater.from(getContext()).inflate(R.layout.generic_item, parent, false);
       }
       
       // Lookup view for data population
       TextView textPatientName = (TextView) convertView.findViewById(R.id.item_data);
       
       // Populate the data into the template view using the data object
       textPatientName.setText(patient.getFullName() + " - last check-in " + patient.getLastCheckinDate());

       // Return the completed view to render on screen
       return convertView;
   }
}
