package com.josenaves.capstone.app.adapter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.josenaves.capstone.app.R;
import com.josenaves.capstone.app.model.CheckIn;

@SuppressLint("SimpleDateFormat")
public class CheckinAdapter extends ArrayAdapter<CheckIn> {

	private static final String TAG = CheckinAdapter.class.getSimpleName();

	public CheckinAdapter(Context context, List<CheckIn> checkins) {
		super(context, 0, checkins);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// Get the data item for this position
		CheckIn checkin = getItem(position);

		// Check if an existing view is being reused, otherwise inflate the view
		if (convertView == null) {
			convertView = LayoutInflater.from(getContext()).inflate(
					R.layout.generic_item, parent, false);
		}

		// Lookup view for data population
		TextView textData = (TextView) convertView.findViewById(R.id.item_data);

		// Populate the data into the template view using the data object
		textData.setText(formatDate(checkin) + " - " + medicationTaken(checkin) + " - " + painIntensivity(checkin)
				+ " - " + eatingCondition(checkin));

		// Return the completed view to render on screen
		return convertView;
	}
	
	private String medicationTaken(CheckIn checkin) {
		return "medication" + (checkin.isTaken() ? "" : " NOT") + " taken";
	}

	private String formatDate(CheckIn checkin) {
		String ret = "";
		
		SimpleDateFormat sdfOut = new SimpleDateFormat("EEE, MMM d - HH:mm:ss");
		SimpleDateFormat sdfIn = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SimpleDateFormat sdfIn2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

		try {
			ret = sdfOut.format(sdfIn.parse(checkin.getDatetime()));
		} 
		catch (ParseException e) {
			Log.d(TAG, e.toString());
			try {
				ret = sdfOut.format(sdfIn2.parse(checkin.getDatetime()));
			} 
			catch (ParseException e1) {
				Log.d(TAG, e.toString());
			}
		}
		return ret;
	}

	private String painIntensivity(CheckIn checkin) {
		String intensivity = "";
		switch (checkin.getMouthPain()) {
		case CheckIn.CONTROLLED:
			intensivity = "controlled";
			break;
		case CheckIn.MODERATED:
			intensivity = "moderated";
			break;
		case CheckIn.SEVERE:
			intensivity = "severe";
			break;
		}
		return intensivity + " pain";
	}

	private String eatingCondition(CheckIn checkin) {
		String eating = "";
		switch (checkin.getStopEating()) {
		case CheckIn.NO:
			eating = "eating";
			break;
		case CheckIn.SOME:
			eating = "sometimes eating";
			break;
		case CheckIn.CANT_EAT:
			eating = "not eating";
			break;
		}
		return eating;
	}

}
