package com.josenaves.capstone.app.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.josenaves.capstone.app.model.Medication;

public class MedicationSpinnerAdapter extends ArrayAdapter<Medication> {

	private static final String TAG = MedicationSpinnerAdapter.class.getSimpleName();
	
	public MedicationSpinnerAdapter(Context context, List<Medication> medication) {
		super(context, 0, medication);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// Get the data item for this position
		Medication medication = getItem(position);

		// Check if an existing view is being reused, otherwise inflate the view
		if (convertView == null) {
			convertView = LayoutInflater.from(getContext()).inflate(
					android.R.layout.simple_spinner_item, parent, false);
		}

		// Lookup view for data population
		TextView item = (TextView) convertView.findViewById(android.R.id.text1);

		// Populate the data into the template view using the data object
		item.setText(medication.getName());
		
		// Return the completed view to render on screen
		return convertView;
	}
	
	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent) {
		// Get the data item for this position
		Medication medication = getItem(position);

		// Check if an existing view is being reused, otherwise inflate the view
		if (convertView == null) {
			convertView = LayoutInflater.from(getContext()).inflate(
					android.R.layout.simple_spinner_dropdown_item, parent, false);
		}

		// Lookup view for data population
		TextView item = (TextView) convertView.findViewById(android.R.id.text1);

		// Populate the data into the template view using the data object
		item.setText(medication.getName());
		
		// Return the completed view to render on screen
		return convertView;
	}

}
