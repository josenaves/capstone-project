package com.josenaves.capstone.app.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckedTextView;
import android.widget.ListView;

import com.josenaves.capstone.app.model.Medication;

public class MedicationAdapter extends ArrayAdapter<Medication> {

	private static final String TAG = MedicationAdapter.class.getSimpleName();
	
	private List<Medication> patientMedication;
	
	public MedicationAdapter(Context context, List<Medication> medication, List<Medication> patientMedication) {
		super(context, 0, medication);
		this.patientMedication = patientMedication;
	}
	

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// Get the data item for this position
		Medication medication = getItem(position);

		// Check if an existing view is being reused, otherwise inflate the view
		if (convertView == null) {
			convertView = LayoutInflater.from(getContext()).inflate(
					android.R.layout.simple_list_item_multiple_choice, parent, false);
		}

		// Lookup view for data population
		CheckedTextView medicationItem = (CheckedTextView) convertView.findViewById(android.R.id.text1);

		// Populate the data into the template view using the data object
		medicationItem.setText(medication.getName());
		medicationItem.setChecked(patientMedication.contains(medication));
		
		((ListView)parent).setItemChecked(position, patientMedication.contains(medication));
		
		// Return the completed view to render on screen
		return convertView;
	}

}
